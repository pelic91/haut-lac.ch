<?php
//if ($_SERVER['REQUEST_URI'] != '/gestion/ajax_pager') {exit('403');}

foreach (glob('../class/*.php') as $file) {include_once($file);}
foreach (glob('../function/*.php') as $file) {include_once($file);}

$thead = '';
$tbody = '';
$tfooter = '';
$limit = 10;
$i = 0;

	if (intval($_POST['page'])==0) {$_POST['page']=1;}
	$pager = ((intval($_POST['page']) - 1) * $limit);
	$request_limit = " LIMIT ".$pager.", ".$limit.";";


	switch ($_POST['subject']) {
		case 'utilisateur':
			// Utilisateur
			$thead = '
								<thead>
									<tr>
									   <th width="200">Identifiant</th>
									   <th>Nombre de dossiers en cours - en traitement</th>
									   <th width="120">Action</th>
									</tr>
								</thead>';
								
			$request = "SELECT * FROM `hautlac_user` WHERE `Valide` = '1'";
			if ($_POST['filter_id']) {$request .= " AND `email` LIKE '%".$_POST['filter_id']."%'";}
			$request .= " ORDER BY `email` ASC";
			
			$nb_result = $db->query($request);
			$sql = $db->query($request.$request_limit);
			
			$tbody .= '
										<tbody>';
					while($res = $sql->fetch_object()) {
					$request = "SELECT * FROM `hautlac_dossier` WHERE `UserID`='".$res->userID."' AND `State`<2;";
					$nb_folder = $db->query($request);
					
						$class = ($i&1) ? '' : ' class="alt-row"';
						$tbody .='
											<tr'.$class.'>
												<td><a href="mailto:'.$res->email.'">'.$res->email.'</a></td>
												<td>'.$nb_folder->num_rows.'</td>
												<td>												
													<a href="javascript:void(0);" onclick="del_user('.$res->userID.');" title="Supprimer la fiche de '.$res->email.'" id="del_user_'.$res->userID.'">
														<img src="media/image/icons/06.png" alt="Supprimer" />
													</a>
												</td>
											</tr>';
						$i++;
					}
			$tbody .= '
										</tbody>';
			// ! Utilisateur
			break;


			case 'gestionnaire':
			// Gestionnaire
			$thead = '
								<thead>
									<tr>
									   <th width="200">Identifiant</th>
									   <th>Informations</th>
									   <th width="120">Action</th>
									</tr>
								</thead>';
								
			$request = "SELECT * FROM `hautlac_admin` WHERE `Level`='1' AND `Valide` = '1'";
			if ($_POST['filter_id']) {$request .= " AND `email` LIKE '%".$_POST['filter_id']."%'";}
			if ($_POST['filter_info']) {$request .= " AND `Information` LIKE '%".$_POST['filter_info']."%'";}
			$request .= " ORDER BY `email` ASC";
			
			$nb_result = $db->query($request);
			$sql = $db->query($request.$request_limit);
			
			$tbody .= '
										<tbody>';
					while($res = $sql->fetch_object()) {
						$class = ($i&1) ? '' : ' class="alt-row"';
						$tbody .='
											<tr'.$class.'>
												<td>'.$res->email.'</td>
												<td>
													<span id="get_info_'.$res->userID.'">'.utf8_encode($res->Information).'</span>
													<input class="set_info" id="set_info_'.$res->userID.'" type="textbox" value="'.utf8_encode($res->Information).'" onKeyPress="if (event.keyCode == 13) valide_user('.$res->userID.');" maxlength="128"></td>
												<td>
													<a href="javascript:void(0);" onclick="$(\'#set_info_'.$res->userID.'\').focus();update_user('.$res->userID.');" title="Modifier les informations de '.$res->email.'" id="edit_info_'.$res->userID.'">
														<img src="media/image/icons/pencil.png" alt="Editer" />
													</a>
													<a href="javascript:void(0);" onclick="valide_user('.$res->userID.');" title="Valider la modification de '.$res->email.'" style="display: none;" id="valide_info_'.$res->userID.'">
														<img src="media/image/icons/tick_circle.png" alt="Valider la modification" />
													</a>													
													<a href="javascript:void(0);" onclick="del_user('.$res->userID.');" title="Supprimer la fiche de '.$res->email.'" id="del_user_'.$res->userID.'">
														<img src="media/image/icons/06.png" alt="Supprimer" />
													</a>
												</td>
											</tr>';
						$i++;
					}
			$tbody .= '
										</tbody>';
			// ! Gestionnaire
			break;
			
			case 'dossier':
			// Dossier
			$thead = '
								<thead>
									<tr>
										<th>Dossier</th>
										<th>Propri&eacute;taire</th>
										<th>Etat</th>
										<th>Commentaire</th>
										<th width="120">Action</th>
									</tr>
								</thead>';
								
			$request = "SELECT * FROM `hautlac_dossier` INNER JOIN `hautlac_user` ON `hautlac_user`.`userID` = `hautlac_dossier`.`UserID`  WHERE '1'='1' ";
			if ($_POST['filter_id']) {$request .= " AND `email` LIKE '%".$_POST['filter_id']."%'";}
			if ($_POST['filter_state']!='99') {$request .= " AND `State`='".$_POST['filter_state']."'";}
			$request .= " ORDER BY `FolderID` DESC";
			
			$nb_result = $db->query($request);
			$sql = $db->query($request.$request_limit);

			$tbody .= '
										<tbody>';
					while($res = $sql->fetch_object()) {
						$sql2 = $db->query("SELECT * FROM `hautlac_form_user` WHERE `FolderID`='".$res->FolderID."' AND (`FormID`='2' OR `FormID`='3');");
						$who_folder = '&nbsp;';
						while($res2 = $sql2->fetch_object()) $who_folder .= ucfirst($res2->Response).'&nbsp;';
						$who_folder = $who_folder!='&nbsp;' ? ' &lsaquo;'.$who_folder.'&rsaquo;' : null;

						$State_Array = array(0 => 'Ouvert', 1 => 'En attente de validation', 2 => 'Ferm&eacute;');
						$State = '<select id="state_'.$res->FolderID.'" onchange="update_state(this.value, '.$res->FolderID.')">';
						foreach ($State_Array as $key => $value) {
							$current_state = $res->State==$key ? ' selected="selected"' : null;
							$State .= '<option value="'.$key.'"'.$current_state.'>'.$value.'</option>';
						}
						$State .= '</select>';
						$class = ($i&1) ? '' : ' class="alt-row"';
						$tbody .='
											<tr'.$class.'>
												<td><a href="index.php?cload=view-folder&folder='.$res->FolderID.'" title="D&eacute;tails">#&nbsp;'.$res->FolderID.$who_folder.'</a></td>
												<td><a href="mailto:'.$res->email.'?subject=Haut Lac - Folder '.$res->FolderID.$who_folder.'">'.$res->email.'</a></td>
												<td>'.$State.'</td>
												<td>
													<span id="get_info_'.$res->FolderID.'">'.utf8_encode($res->Information).'</span>
													<input class="set_info" id="set_info_'.$res->FolderID.'" type="textbox" value="'.utf8_encode($res->Information).'" onKeyPress="if (event.keyCode == 13) valide_user('.$res->FolderID.');" maxlength="128"></td>
												<td>
													<a href="javascript:void(0);" onclick="$(\'#set_info_'.$res->FolderID.'\').focus();update_user('.$res->FolderID.');" title="" id="edit_info_'.$res->FolderID.'">
														<img src="media/image/icons/pencil.png" alt="Editer" />
													</a>
													<a href="javascript:void(0);" onclick="valide_user('.$res->FolderID.');" title="Valider le commantaire" style="display: none;" id="valide_info_'.$res->FolderID.'">
														<img src="media/image/icons/tick_circle.png" alt="Valider la modification" />
													</a>
													<a href="javascript:void('.$res->FolderID.');" onclick="delete_folder('.$res->FolderID.');" title="Supprimer ce dossier" id="delete_folder_'.$res->FolderID.'">
														<img src="media/image/icons/cross_circle.png" alt="Supprimer ce dossier" />
													</a>														
												</td>
											</tr>';
						$i++;
					}
			$tbody .= '
										</tbody>';
			// ! Dossier
			break;
			
			
		default:
			exit('Erreur interne');
	}

	if ($nb_result->num_rows > 10) {
				$tfooter .= '
								<tfoot>
									<tr>
										<td colspan="99">
											<div class="pagination">
												';
			if (intval($_POST['page'])>1) {
				$tfooter .= '
												<a href="javascript:void(0);" onclick="pager(1);" title="Premi&egrave;re page">&laquo; Premi&egrave;re page</a>
												<a href="javascript:void(0);" onclick="pager('.(intval($_POST['page'])-1).');" title="Page pr&eacute;c&eacute;dente">&lsaquo; Page pr&eacute;c&eacute;dente</a>';
			}
			for ($i = 1; $i <= (intval($nb_result->num_rows / $limit) + 1); $i++) {
				$current = (intval($_POST['page'])==$i) ? ' current' : '';
				$tfooter .= '
												<a href="javascript:void(0);" onclick="pager('.$i.');" class="number'.$current.'" title="Aller &agrave; la page '.$i.' de la recherche">'.$i.'</a>';
			}
			if (intval($_POST['page']) < intval($nb_result->num_rows / $limit)) {
				$tfooter .= '												
												<a href="javascript:void(0);" onclick="pager('.(intval($_POST['page'])+1).')" title="Page suivante">Page suivante &rsaquo;</a>
												<a href="javascript:void(0);" onclick="pager('.intval($nb_result->num_rows / $limit).');" title="Derni&egrave;re page">Derni&egrave;re page &raquo;</a>';
			}
				$tfooter .= '
											</div>
											<div class="clear"></div>
										</td>
									</tr>
								</tfoot>';	
	}

	if ($sql->num_rows==0) {
		echo '<center>Aucun r&eacute;sultat pour votre recherche.</center>';
	} else {
		echo '<table>'.$thead.$tbody.$tfooter.'</table>';
	}
?>