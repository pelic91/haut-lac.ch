<?php
if (!$_POST['SSID'])  {exit('403');}

session_id($_POST['SSID']);
session_start();

foreach (glob('../class/*.php') as $file) {include_once($file);}
foreach (glob('../function/*.php') as $file) {include_once($file);}

$user = new User($db);
if (!$user->is_loaded()) exit('403 Forbidden');

foreach ($_POST as $key => $value) {
	$_POST[$key] = utf8_decode(trim(urldecode($value)));
}

$user->close_folder($_POST['folder']);
?>