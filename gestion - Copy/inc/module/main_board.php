		<div id="main-content">
				
			<noscript>
				<div class="notification error png_bg">
					<div>
						Le javascript de votre navigateur ne fonctionne pas. Merci de <a href="http://www.01net.com/telecharger/windows/Internet/navigateur/" title="Upgrade to a better browser">mettre &agrave; jour</a> votre navigateur upgrade ou <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Activer le javascript de votre navigateur">activer</a> le javacript.
					</div>
				</div>
			</noscript>
				
				<h2>Tableau de bord</h2>
				<p id="page-intro">Bienvenue <i><?php echo $user->userData['User_Name'];?></i>, que souhaitez-vous faire ?</p>
				
				<ul class="shortcut-buttons-set">
					
					<li><a class="shortcut-button" href="/gestion/index.php?cload=gestion_utilisateur"><span>
						<img src="media/image/icons/03.png" alt="Nouvel utilisateur" /><br />
						G&eacute;rer les utilisateurs
					</span></a></li>
					
					<li><a class="shortcut-button" href="/gestion/index.php?cload=gestion_gestionnaire"><span>
						<img src="media/image/icons/02.png" alt="Nouvelle concession" /><br />
						G&eacute;rer les gestionnaires
					</span></a></li>
								
				</ul>
				
				<div class="clear"></div>

				<div id="footer">
					<small>
							<?php echo $copy;?>
					</small>
				</div>
				
		</div>