<?php
class Website {

	var $server_name;
	var $page;
	var $css;
	var $flag;
	var $time_start;
	var $title;
	var $popup;
	var $db;
	var $displayErrors = false;
	var $news = array();
	 
	function Website($db) {
		$this->server_name = $_SERVER["SERVER_NAME"];
		$this->page = $_GET["cload"];
		$this->css[] = 'style'; 
		$flag_array = explode('.', $this->server_name);
		$this->flag = $flag_array[0];
	    $this->db = $db;
	    if ( !$this->db ) die(mysqli_error($db));
		$this->Start();
	}
	
	function Server_Name() {
		return $this->server_name;
	}
	
	function Flag() {
		switch ($this->flag) {
			case 'en':
				$this->flag = 'en';
				break;
			case 'www':
			case 'fr':
			default:
				$this->flag = 'fr';
		}
		return $this->flag;
	}

	function Start() {
		$time = microtime(true);
		$time = explode(" ", $time);
		$this->time_start = $time[1] + $time[0];
		return round(($this->time_start),4);
	}
	
	function Timer() {
		$time = microtime(true);
		$time = explode(" ", $time);
		$finish = $time[1] + $time[0];
		return round(($finish - $this->time_start),4);
	}
	
	function Get_Page() {
		return $this->page;
	}
	
	function Set_Page($page) {
		$this->page = $page;
	}

	function Get_CSS() {
		return '\''.implode('|', $this->css).'\'';
	}
	
	function Set_Title($title) {
		$this->title = $title;
	}
	
	function Get_Title() {
		return $this->title;
	}

	
  ////////////////////////////////////////////
  // PRIVATE FUNCTIONS
  ////////////////////////////////////////////
  
  function query($sql)
  {
	$res = $this->db->query($sql);
	if ( $this->displayErrors ) { echo '<b>Query : </b>'.$sql.' [Modif : '.$res->num_rows.']<br />'; }
	if ( !res )
		$this->error(mysql_error($this->db), $line);
	return $res;
  }

}



?>