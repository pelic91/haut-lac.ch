<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

    <div id="primary" class="content-area inner_blog blog single_page">
        <div id="content" class="site-content wrapper" role="main">
            <header class="entry-header">
                <?php while ( have_posts() ) : the_post(); ?>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                <?php endwhile; ?>
            </header>
            <div class="blog_left_area">
                <?php
                if(is_active_sidebar('blogs-sidebar-1')){
                    dynamic_sidebar('blogs-sidebar-1');
                }
                ?>
            </div>
            <?php /* The loop */ ?>
            <div class="blog_right_area entry-content">
                <?php while ( have_posts() ) : the_post(); ?>

                    <?php  $the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $type);  ?>
                    <?php if(!empty($the_url)) { ?>
                        <img src="<?php echo  $the_url[0]; ?>" alt="<?php echo  $the_url[0];  ?>"/>
                    <?php } ?>
                    <?php
                    $content = apply_filters( 'the_content', get_the_content() );
                    echo $content = str_replace( '&', '&amp;', $content );
                    ?>

                    <span class="entry-date"><span class="date_and_time_time"><?php echo esc_html( the_date('jS F , Y g:i a') ); ?></span></span>
                    <?php
                    /*
                    $category_link = get_category_link( $category_id ); ?>
                                                                <a href="<?php echo esc_url( $category_link ); ?>" title="Category Name"><?php the_category(); ?></a>
                    <?php */ ?>
                    <?php edit_post_link('Edit'); ?>
                <?php endwhile; ?>
                <?php //twentythirteen_post_nav(); ?>
                <div id="cooler-nav" class="navigation">
                    <?php // Display previous and next posts thumbnails ?>
                    <div class="nav-box previous" style="float: left; text-align: left;"> <?php
                        $prevPost = get_previous_post();
                        $prevthumbnail = get_the_post_thumbnail($prevPost->ID, array(100,100));
                        previous_post_link('%link','%title '.$prevthumbnail.''); ?>
                    </div>

                    <?php // Get thumbnail of next post?>
                    <div class="nav-box next" style="float: right; text-align: right;"> <?php
                        $nextPost = get_next_post();
                        $nextthumbnail = get_the_post_thumbnail($nextPost->ID, array(100,100));
                        next_post_link('%link',''.$nextthumbnail.' %title'); ?>
                    </div>
                </div>
                <?php comments_template(); ?>


            </div>
        </div><!-- #content -->
    </div><!-- #primary -->


<?php get_footer(); ?>