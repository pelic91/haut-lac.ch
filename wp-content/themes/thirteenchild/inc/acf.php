<?php 
// define( 'ACF_LITE' , true );
// include_once( PG_BASE_CHILD. DS . 'lib/advanced-custom-fields/acf.php' );

// require_once( PG_INC_CHILD . DS. 'acf' .DS.'acf-fallback.php' );
// require_once( get_stylesheet_directory().'/inc/acf/acf-field-select.php' );
require_once( get_stylesheet_directory().'/inc/acf/options-pages.php' );
require_once( get_stylesheet_directory().'/inc/acf/acf-checkbox-and-select.php' );

// add_filter('acf/load_field/key=field_54ae2813b98d3', 'acf_create_select');
function acf_create_select( $field ) {
	$choices = $field['choices'];
	
	foreach( $choices as $t ) {
		if ( !isset( $field['choices'][ $t->optgroup ] ) ){
			$field['choices'][ $t->optgroup ] = array();
		}
		$field['choices'][ $t->optgroup ][ $t->item_value ] = $t->item_text;
	}
	return $field;
}
?>