<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_home-settings',
		'title' => 'Home Settings',
		'fields' => array (
			array (
				'key' => 'field_547ffecf94ac4',
				'label' => 'Intro',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_547ffede94ac5',
				'label' => 'Intro Title',
				'name' => 'intro_title',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_547ffeed94ac6',
				'label' => 'Intro Subtitle',
				'name' => 'intro_subtitle',
				'type' => 'text',
				'instructions' => 'You can use <span style="color:#F00;">typer</span> shortcode to display typer content. Shortcode structure is given below.<br>
	Please enter typing texts (displayed in blue color) delimited with comma.<br>
	[<span style="color:#F00;">typer</span> <span style="color:#000;">data</span>="<span style="color:#00F;">entertainment,connectivity</span>"]education<span style="color:#F00;">[/typer]</span>',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_547fff2694ac7',
				'label' => 'Intro Buttons',
				'name' => 'intro_buttons',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_547fff7194ac8',
						'label' => 'Button Title',
						'name' => 'intro_button_title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_547fff8594ac9',
						'label' => 'Button Link',
						'name' => 'intro_button_link',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_547fffa394aca',
						'label' => 'Button Class',
						'name' => 'intro_button_class',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
				),
				'row_min' => 1,
				'row_limit' => 2,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_547ffa9c4bec6',
				'label' => 'Partners',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_547f04d3f96fe',
				'label' => 'Partners',
				'name' => 'partners',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_547f04f2f96ff',
						'label' => 'Partner Title',
						'name' => 'partner_title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_547f0505f9700',
						'label' => 'Partner Image',
						'name' => 'partner_image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_547f051ef9701',
						'label' => 'Partner Link',
						'name' => 'partner_link',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Partner',
			),
			array (
				'key' => 'field_547ffaa74bec7',
				'label' => 'Care Anywhere',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_547ffb2b15123',
				'label' => 'Care Anywhere',
				'name' => 'services',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_547ffb2b15124',
						'label' => 'Service Title',
						'name' => 'service_title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_547ffb7515127',
						'label' => 'Service Subtitle',
						'name' => 'service_subtitle',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_547ffb2b15125',
						'label' => 'Service Image',
						'name' => 'service_image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				),
				'row_min' => 3,
				'row_limit' => 3,
				'layout' => 'table',
				'button_label' => 'Add Service',
			),
			array (
				'key' => 'field_548028e6a9b1d',
				'label' => 'Request A Demo',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_54803d899b0f0',
				'label' => 'Request A Demo Form',
				'name' => 'request_a_demo_form',
				'type' => 'post_object',
				'post_type' => array (
					0 => 'wpcf7_contact_form',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_54801b03a9484',
				'label' => 'Contact Us',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_54803cabf63e4',
				'label' => 'Contact Us Form',
				'name' => 'contact_us_form',
				'type' => 'post_object',
				'post_type' => array (
					0 => 'wpcf7_contact_form',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_54801b6b472e2',
				'label' => 'Map',
				'name' => 'contact_us_map',
				'type' => 'google_map',
				'center_lat' => '40.7217048',
				'center_lng' => '-74.0003355',
				'zoom' => 17,
				'height' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/home.php',
					'order_no' => 1,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
?>