<?php
function my_blog_categories( $field ){
	$categories = get_categories(array(
		'hide_empty' => 0,
		'taxonomy'   => 'category',
	));
	$choices = array();
	foreach ($categories as $category) {
		$choices[$category->cat_ID] = $category->cat_name . ' ('.$category->category_count.')';
	}
	/*
	$field['choices'] = array(
		'sssssss' => 'Attention Seekers',
		'eeeeee' => 'Bouncing Entrances',
	);
	*/
	$field['choices'] = $choices;
	
    // Important: return the field
    return $field;
}

// v3.5.8.2 and below
add_filter('acf_load_field-tmpl_news_category', 'my_blog_categories');
add_filter('acf_load_field-blog_category_hide', 'my_blog_categories');

// v4.0.0 and above
add_filter('acf/load_field/name=tmpl_news_category', 'my_blog_categories');
add_filter('acf/load_field/name=blog_category_hide', 'my_blog_categories');
?>