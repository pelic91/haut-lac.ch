<?php 
/*
 * Options Pages
 * 
 * title: {string}  (required) the title for the sub page
 * menu: {string}  (optional) the menu name for the sub page. Defaults to title
 * slug: {string} (optional) the name of the page slug. Defaults to sanitized title
 * parent: {string} (optional) the parent slug to sit under. Defaults to the Options page main menu slug
 * capability: {string} (optional) the capability required to see the page. Defaults to the Options page main menu capability * 
 * 
 *****************************************************************/
if( function_exists('acf_add_options_sub_page') ){
	
	// Create a simple sub options page called 'Footer'
    // acf_add_options_sub_page( 'Footer' );
	
	// Create an advanced sub page called 'Footer' that sits under the General options menu
	/*acf_add_options_sub_page(array(
		'title'     => 'Footer2',
		'parent'    => 'options-general.php',
		'capability'=> 'manage_options'
    ));*/
}

function my_acf_options_page_settings( $settings )
{
	$settings['title'] = 'Theme Options';
	$settings['pages'] = array('Banner', 'Blog & News');
	
	return $settings;
}
// add_filter('acf/options_page/settings', 'my_acf_options_page_settings');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));
	
	/*
	acf_add_options_sub_page(array(
		'page_title' 	=> 'General Settings',
		'menu_title'	=> 'General',
		'parent_slug'	=> 'theme-general-settings',
	));
	*/
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Banner Settings',
		'menu_title'	=> 'Banner',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Blog & News Settings',
		'menu_title'	=> 'Blog & News',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}
?>