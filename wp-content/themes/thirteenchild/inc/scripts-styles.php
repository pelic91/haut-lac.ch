<?php
/*********************
SCRIPTS & ENQUEUEING
*********************/

// Register Scripts
function hautlac_register_scripts() {
	wp_register_script('hautlac_js', get_stylesheet_directory_uri().'/js/custom.js', array( 'jquery' ), '', true );
}
//add_action('wp_enqueue_scripts', 'hautlac_register_scripts');

// Enqueue Scripts
function hautlac_enqueue_scripts() {
	
	if (is_admin()) {
	}else{
		wp_enqueue_script('hautlac_js');
	}
}
add_action('wp_enqueue_scripts', 'hautlac_enqueue_scripts');


// Register PrettyPhoto
function hautlac_register_prettyphoto(){
	wp_register_script( 'prettyphoto', get_stylesheet_directory_uri() . '/js/jquery.prettyPhoto.min.js', array( 'jquery' ), '', true );
}
// add_action( 'wp_enqueue_scripts', 'hautlac_register_prettyphoto' );
function hautlac_enqueue_prettyphoto() {
	if( !is_admin() ){
		wp_enqueue_script('prettyphoto');
	}
}
// add_action('wp_enqueue_scripts', 'hautlac_enqueue_prettyphoto');
?>