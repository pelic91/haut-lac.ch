<?php
// This removes the annoying [�] to a Read More link
function potenzabase_excerpt_more($more) {
	global $post;
	// edit here if you like
	return '...  <a href="'. get_permalink($post->ID) . '" title="'. __('Read', 'potenzabase') . get_the_title($post->ID).'">'. __('Read more &raquo;', 'potenzabase') .'</a>';
}

//This is a modified the_author_posts_link() which just returns the link.
//This is necessary to allow usage of the usual l10n process with printf().
function potenzabase_get_the_author_posts_link() {
	global $authordata;
	if ( !is_object( $authordata ) ) {
		return false;
	}
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);
	return $link;
}

/**
 * Get file lists.
 *
 * This function can be used to get list of files as array from given folder.
 *
 * @since 1.0.0
 *
 * @param  string $source_folder Path to retrive file from.
 * @param  string $ext           Extension of files to scan in given path.
 * @param  int    $sec           Scan files for how older want to scan. 0 for ininite.
 * @param  int    $limit         Count of files to limit. 0 for ininite.
 * @return array                 Return data contains array with mixed data.
 */
function get_module_files($source_folder, $ext, $sec, $limit){
	if( !is_dir( $source_folder ) ) {
		die ( "Invalid directory.\n\n" );
	}
	
	$scanned_dirs_data = glob($source_folder.'/*', GLOB_ONLYDIR);
	
	// echo '<pre>';
	// print_r($scanned_dirs_data);
	// echo '</pre>';
	
	$FILES = glob($source_folder.DIRECTORY_SEPARATOR."*.".$ext);
	$set_limit    = 0;
	
	// echo '<pre>';
	// print_r($FILES);
	// echo '</pre>';
	
	foreach($FILES as $key => $file) {
		
		if( $limit != 0){
			if( $set_limit == $limit )    break;
		}
		
		if( filemtime( $file ) > $sec ){ // Check for time lime
			
			$FILE_LIST[$key]['path']    = substr( $file, 0, ( strrpos( $file, "\\" ) +1 ) );
			$FILE_LIST[$key]['name']    = substr( $file, ( strrpos( $file, "\\" ) +1 ) );   
			$FILE_LIST[$key]['size']    = filesize( $file );
			$FILE_LIST[$key]['date']    = date('Y-m-d G:i:s', filemtime( $file ) );
			$set_limit++;
			
		}elseif( $sec == 0 ){ // Check if time limit is set to infinite
			$FILE_LIST[$key]['path']    = substr( $file, 0, ( strrpos( $file, "\\" ) +1 ) );
			$FILE_LIST[$key]['name']    = substr( $file, ( strrpos( $file, "\\" ) +1 ) );   
			$FILE_LIST[$key]['size']    = filesize( $file );
			$FILE_LIST[$key]['date']    = date('Y-m-d G:i:s', filemtime( $file ) );
			$set_limit++;
		}
		
	}
	if(!empty($FILE_LIST)){
		return $FILE_LIST;
	} else {
		die( "No files found!\n\n" );
	}
}

/**
 * Get file lists.
	*
 * This function can be used to get list of files as array from given folder.
	*
 * @since 1.0.0
	*
 * @param  string $source_folder Path to retrive file from.
 * @param  string $ext           Extension of files to scan in given path.
 * @param  int    $sec           Scan files for how older want to scan. 0 for ininite.
 * @param  int    $limit         Count of files to limit. 0 for ininite.
 * @return array                 Return data contains array with mixed data.
 */
function get_module_folder($modules_dir, $modules_uri, $module_type = false){
	
	if( !is_dir( $modules_dir ) ) {
		die ( "Invalid directory.\n\n" );
	}
	
	$module_base = 'module.php';
	
	/*
	// @todo: convert to class?
	// all types?
	if( !$module_type )
		return array_merge(self::getModules('user'), self::getModules('core')); // @todo: reverse?
	*/
	
	/*
	if( $module_type !== 'core' && is_child_theme() ){
		$from_where = STYLESHEETPATH.'/inc/custom_post/post_type_modules';
	}else{
		$from_where = TEMPLATEPATH.'/inc/custom_post/post_type_modules';
	}
	*/
	
	$modules_data = array();
	
	$modules = glob($modules_dir.'/*', GLOB_ONLYDIR);
	
	$module_headers = array(
		'module_name' => 'Module Name',
		'module_base' => '',
		'module_dir'  => '',
		'module_uri'  => '',
		'description' => 'Description',
		'author'      => 'Author',
		'url'         => 'Author URI',
		'version'     => 'Version',
		'priority'    => 'Priority',
		'module_type' => '',
		'auto'        => 'Auto Enable',
	);
	
	foreach($modules as $module ) {
		
		if(!is_readable($module.DS.$module_base)) continue;
		
		$data = get_file_data($module.DS.$module_base, $module_headers);
		
		if(empty($data['module_name'])) continue;
		
		$data['module_base'] = basename($module);
		$data['module_dir']  = fixslash($module);
		$data['module_uri']  = $modules_uri . '/' . basename($module);
		$data['module_type'] = $module_type;
		
		$modules_data[] = $data;
	}
	
	return $modules_data;
}

/*
TODO: Delete this later
$source_folder =  dirname( __FILE__ ).DIRECTORY_SEPARATOR.'extensions';// Path
$ext = "php";                                                          // File Extension to scan
$sec = "0";                                                            // Files older time in seconds. 0 for infinite.
$limit = 0;                                                            // Limit count of files to return. 0 for infinite.

// echo '<pre>';
// print_r(glob_files($source_folder, $ext, $sec, $limit));
// echo '</pre>';
*/

function fixslash($path){
	$sep = DIRECTORY_SEPARATOR;
	if( $sep == '/' ){
		$fix_sep = "\\";
	}elseif( $sep == '\\' ){
		$fix_sep = '/';
	}
	$path = str_replace($fix_sep,$sep,$path);
	return $path;
}

function my_site_map(){
	return 'site-map';
}
// add_filter( 'site_map', 'my_site_map', 10 );