<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area inner_blog blog">
		<div id="content" class="site-content wrapper" role="main">
        
		<?php if ( have_posts() ) : ?>
			<header class="entry-header">
				<h1 class="entry-title"><?php printf( __( 'Category Archives: %s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>

				<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->
             <div class="blog_left_area">
             <?php
                if(is_active_sidebar('blogs-sidebar-1')){
                dynamic_sidebar('blogs-sidebar-1');
                }
                ?>
            </div>

            <div class="blog_right_area ">
                
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
           <div class="consectetur_section"> 
                <div class="consectetur_details">
            <span class="entry-date"><?php echo get_the_date('M'); ?><span><?php echo get_the_date('j'); ?></span><?php echo get_the_date('Y'); ?></span>
             <span class="blog_tital"><a href="<?php echo get_permalink( $post_id ); ?>"> <?php the_title(); ?></a></span>
             
              <div class="consectetur_content">

                                        

                                        <?php  $the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $type);  ?>

                                           <?php if(!empty($the_url)) { ?>

                                                        <img src="<?php echo  $the_url[0]; ?>" alt="<?php echo  $the_url[0];  ?>"/>

                                            <?php } ?>

                        <div class="blog_details">

                        <?php the_excerpt(); ?>

                        <a href="<?php echo get_permalink( $post_id ); ?>" class="view_more">VIEW MORE</a>

                         <?php $my_var = get_comments_number( $post_id ); ?>

                                        <span class="comment"><?php echo $my_var; ?></span>

                                        <span class="date_and_time_time"><?php echo esc_html( get_the_date('jS F , Y g:i a') ); ?></span></div>

                                         <!--<span class="date_and_time_time">--><?php //echo get_the_date('g:i a'); ?><!--</span>-->

                        </div> 

                            

                         

                            
                    </div>
                    <div class="consectetur_shadow"></div>

              </div>
				<?php //get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
              
			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
        
        </div>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>