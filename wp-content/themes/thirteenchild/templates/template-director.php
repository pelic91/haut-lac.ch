<?php
/**
 * Template Name: director
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */


get_header(); ?>
<style type="text/css">
body{
    overflow-x: hidden;
}
</style>

	<div id="primary" class="content-area inner_blog the_directors">
		<div id="content" class="site-content wrapper" role="main">
        	<?php while ( have_posts() ) : the_post(); ?>
        		<header class="entry-header">
						

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->
            	<?php endwhile; ?>

            <?php
			 //$content_blocks = get_field('directors_block');
             
             //print_r($content_blocks);
			
			// check if the flexible content field has rows of data
			if( have_rows('directors_block') ){
				
             //   echo 'test';
				// loop through the rows of data
				while ( have_rows('directors_block') ) {
				    
                    	the_row();
                    if( get_row_layout() == 'directors_information' ){
						?>
						<section class="<?php echo get_row_layout();?> content_top <?php echo $third_class; ?>">
                          <?php  $directors_image= get_sub_field('directors_image');
                          
                
                        $directors_image_title           = $directors_image['title'];
                        $directors_image_alt             = $directors_image['alt'];
                        
                        $full_image_size     = 'large';
                        $gal_img             = $gal_data['sizes'][$full_image_size];
                        $gal_img_width       = $gal_data['sizes'][$full_image_size.'-width'];
                        $gal_img_height      = $gal_data['sizes'][$full_image_size.'-height'];
                        
                        $thumb_image_size    = 'medium';
                        $directors_image_thumb       = $directors_image['sizes'][$thumb_image_size];
                        $directors_image_thumb_width = $directors_image['sizes'][$thumb_image_size.'-width'];
                        $directors_image_height= $directors_image['sizes'][$thumb_image_size.'-height'];
                        ?>
                        <img src="<?php  echo $directors_image_thumb;?>" title="<?php echo $directors_image_title; ?>" alt="<?php echo $directors_image_alt; ?>"/>
                               <span> <?php the_sub_field('directors_name'); ?></span>
                                <?php the_sub_field('directors_postion'); ?><br />
                                <?php the_sub_field('directors_place'); ?>
	
						</section>
						<?php
					               }else{
				// no layouts found
		              	}                        
                }
            }
            ?>
            <?php get_sidebar(); ?>
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<?php comments_template(); ?>
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>