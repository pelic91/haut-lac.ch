<?php
/**
 * Template Name: Home
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<style type="text/css">
body{
    overflow-x: hidden;
}
</style>	

	<div id="primary" class="site-content">
		<div id="content" role="main french_content">
            <div class="wrapper">
			<?php
			// $content_blocks = get_field('content_blocks');
			
			// check if the flexible content field has rows of data
			if( have_rows('content_blocks') ){
				
                $count="0";
				
				// loop through the rows of data
				while ( have_rows('content_blocks') ) {
					the_row();
					
				    $count++;
					
                    if($count=='3'){
                        $third_class="school_section third_sections";
                        $count="0";
                    }else{
                        $third_class="school_section";
                    }
					
					global $section_layout;
					$section_layout = get_row_layout();
					get_template_part( 'content/home/section', $section_layout );
					
					if( get_row_layout() == 'home_new_campus' ){
					}elseif( get_row_layout() == 'top_academics' || get_row_layout() == 'academics_top'){
					}elseif( get_row_layout() == 'school_information' ){
					}elseif( get_row_layout() == 'school_logos' ){
					}
               	}
			}
			if( have_rows('content_blocks') ){
				
				// loop through the rows of data
				while ( have_rows('content_blocks') ) {
				    the_row();
					
					if( get_row_layout() == 'school_info' ){
					}elseif( get_row_layout() == 'enrol_info' ){
					}elseif( get_row_layout() == 'partner_logos' ){
					}elseif( get_row_layout() == 'community_life' ){
					}
				}
			}
			/*
			while ( have_posts() ) : the_post();
				?>
				<?php get_template_part( 'content', 'page' ); ?>
				<?php comments_template( '', true ); ?>
				<?php
			endwhile; // end of the loop.
			*/
			?>
        </div>
	</div><!-- #content -->
</div><!-- #primary -->

<script type='text/javascript' src='<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.flexisel.js'></script>
<script>
jQuery(function($){
	var len = $(".flexiselsummer_two").find('li').length;
	var windwidth = $(window).width();
	$(window).load(function() {
		if(len>=4){
			$("ul.logo_slider" ).siblings().css("display","none");
			//$(".nbs-flexisel-nav-right").css("display","none");
			//$(".nbs-flexisel-nav-left").css("display","none");
		}
		if(windwidth < 481){
			$(".nbs-flexisel-nav-right").css("display","block");
			$(".nbs-flexisel-nav-left").css("display","block");
		}
	});
	jQuery("#flexiselsummer").flexisel({
		visibleItems: 3,
	});
	jQuery(".flexiselsummer_two").flexisel({
		visibleItems: 2,
		clone:true,
		enableResponsiveBreakpoints: true,
		responsiveBreakpoints: {
			portrait: {
				changePoint:480,
				visibleItems: 2
			}
		}
	});
});
</script>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

<?php /* ?>

<?php if(!empty($capmpus_image_1_img_thumb)){ ?> <li> <img src="<?php  echo $capmpus_image_1_img_thumb;?>" title="<?php echo $capmpus_image_1_image_title; ?>" alt="<?php echo $$capmpus_image_2_image_alt; ?>"/></li><?php } ?>
                                        <?php if(!empty($capmpus_image_2_img_thumb)){ ?><li><img src="<?php  echo $capmpus_image_2_img_thumb;?>" title="<?php echo $capmpus_image_2_image_title; ?>" alt="<?php echo $$capmpus_image_2_image_alt; ?>"/></li><?php } ?>
                                        <?php if(!empty($capmpus_image_3_img_thumb)){ ?><li><img src="<?php  echo $capmpus_image_3_img_thumb;?>" title="<?php echo $capmpus_image_3_image_title; ?>" alt="<?php echo $capmpus_image_3_image_alt; ?>"/></li><?php } ?>
<?php */ ?>