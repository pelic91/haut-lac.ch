/*jQuery(document).ready(function($) {
	if ($('body.home').length > 0) {
		setmenu();
		$('#menu-menu-1').after('<div class="dummylayer"></div>');
		$('.dummylayer').hide();
		$('.nav-menu').hover(function() {
			if ($('.menu_new').length > 0) {
				$('.nav-menu > li > .sub-menu').stop().slideDown();
				$('.dummylayer').slideDown();
			}
		});
		
		var maxHeight = Math.max.apply(null, $(".nav-menu > li > .sub-menu").map(function (){
			return $(this).height();
		}).get());
		$('.dummylayer').height(maxHeight + 20);
		
		var hideTimer = null;
		$('.dummylayer, .nav-menu').bind('mouseleave', function() {
			hideTimer = setTimeout(function() {
				$('.nav-menu > li > .sub-menu').stop().slideUp();
				$('.dummylayer').slideUp();
			}, 1000);
		});		
		$('.dummylayer, .nav-menu').bind('mouseenter', function() {
			if (hideTimer !== null) {
				clearTimeout(hideTimer);
			}
		});
				
	}
});

jQuery( window ).resize(function() {
 setmenu();
});

function setmenu(){
	
		if (jQuery(window).width() > 650) {	
		if (jQuery('body.home').length > 0)	jQuery('#menu-menu-1').addClass('menu_new');
		} else{
			jQuery('#menu-menu-1').removeClass('menu_new');
			jQuery('.dummylayer').hide();
		}
			

}*/

/**/

//jQuery is required to run this code
$( document ).ready(function() {

    scaleVideoContainer();

    initBannerVideoSize('.video-container .poster img');
    initBannerVideoSize('.video-container .filter');
    initBannerVideoSize('.video-container video');

    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.video-container .poster img');
        scaleBannerVideoSize('.video-container .filter');
        scaleBannerVideoSize('.video-container video');
    });

});

function scaleVideoContainer() {

    var height = $(window).height() + 5;
    var unitHeight = parseInt(height) + 'px';
    $('.homepage-hero-module').css('height',unitHeight);

}

function initBannerVideoSize(element){

    $(element).each(function(){
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);

}

function scaleBannerVideoSize(element){

    var windowWidth = $(window).width(),
    windowHeight = $(window).height() + 5,
    videoWidth,
    videoHeight;

    console.log(windowHeight);

    $(element).each(function(){
        var videoAspectRatio = $(this).data('height')/$(this).data('width');

        $(this).width(windowWidth);

        if(windowWidth < 1000){
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});

            $(this).width(videoWidth).height(videoHeight);
        }

        $('.homepage-hero-module .video-container video').addClass('fadeIn animated');

    });
}



$(document).ready(function(){
    $('#close').click(function() {
        $(this).parents('div').css('display', 'none');
        create_cookie('startTourClosed', '1', 7, '/');
    });
});
/**
 * Create cookie with javascript
 *
 * @param {string} name cookie name
 * @param {string} value cookie value
 * @param {int} days2expire
 * @param {string} path
 */
function create_cookie(name, value, days2expire, path) {
    var date = new Date();
    date.setTime(date.getTime() + (days2expire * 24 * 60 * 60 * 1000));
    var expires = date.toUTCString();
    document.cookie = name + '=' + value + ';' +
        'expires=' + expires + ';' +
        'path=' + path + ';';
}



$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $("#startTour").addClass("titleColor");
        $('body').find('#navbar-sticky-wrapper').addClass('is-sticky');
    }else{
        $("#startTour").removeClass("titleColor");
        $('body').find('#navbar-sticky-wrapper').removeClass('is-sticky');
    }

});