<?php
/* --------------------------------------------------------------------------
 * CONSTANTS & VARIABLES
 * --------------------------------------------------------------------------*/
if( ! defined( 'DS' ) )                 define( 'DS', DIRECTORY_SEPARATOR ); // directory separator to fix slashes based on loaded os

// Base Paths
if( ! defined( 'PG_BASE_CHILD' ) )      define( 'PG_BASE_CHILD',     get_stylesheet_directory() );					//root server path of the child theme
if( ! defined( 'PG_BASE_URL_CHILD' ) )  define( 'PG_BASE_URL_CHILD', esc_url( get_stylesheet_directory_uri() ) );	//url of the loaded child theme

// Include Paths
if( ! defined( 'PG_INC_CHILD' ) )      define( 'PG_INC_CHILD',     PG_BASE_CHILD.DS.'inc' );	//root server path of the child theme
if( ! defined( 'PG_INC_URL_CHILD' ) )  define( 'PG_INC_URL_CHILD', PG_BASE_URL_CHILD.'/inc' );	//url of the loaded child theme

// Vendor Paths
if( ! defined( 'PG_VENDOR_CHILD' ) )      define( 'PG_VENDOR_CHILD',     PG_BASE_CHILD.DS.'vendor' );    //root server path of the child theme
if( ! defined( 'PG_VENDOR_URL_CHILD' ) )  define( 'PG_VENDOR_URL_CHILD', PG_BASE_URL_CHILD.'/vendor' );  //url of the loaded child theme

// CSS Paths
if( ! defined( 'PG_CSS_CHILD' ) )      define( 'PG_CSS_CHILD',     PG_BASE_CHILD.DS.'css' );    //root server path of the child theme
if( ! defined( 'PG_CSS_URL_CHILD' ) )  define( 'PG_CSS_URL_CHILD', PG_BASE_URL_CHILD.'/css' );  //url of the loaded child theme

// JS Paths
if( ! defined( 'PG_JS_CHILD' ) )      define( 'PG_JS_CHILD',     PG_BASE_CHILD.DS.'js' );    //root server path of the child theme
if( ! defined( 'PG_JS_URL_CHILD' ) )  define( 'PG_JS_URL_CHILD', PG_BASE_URL_CHILD.'/js' );  //url of the loaded child theme

add_filter( 'widget_text', 'do_shortcode');
include('inc/scripts-styles.php');
include('inc/gestion_admin.php');
include('inc/custom-functions.php');
include('inc/acf.php');
include('inc/cpt/init.php');

if (!(current_user_can('administrator'))) {
	function remove_wpcf7() {
		remove_menu_page( 'wpcf7' );
	}
	add_action('admin_menu', 'remove_wpcf7');
}
add_image_size( 'recent_article_thumb', 199, 149, true );
add_image_size( 'recent_post', 300, 300, true );

if(!isset($_SESSION)){
		session_set_cookie_params(86400, '', '', FALSE);
		session_cache_limiter('nocache');
		//session_save_path($_SERVER['DOCUMENT_ROOT'].'/tmp');
		//session_save_path(ABSPATH.'tmp');
		session_start();
}
remove_filter('the_content', 'wptexturize');
function twentythirteen_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'twentythirteen_logo_section' , array(
    'title'       => __( 'Logo', 'twentythirteen' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
) );
$wp_customize->add_setting( 'twentythirteen_logo' );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'twentythirteen_logo', array(
    'label'    => __( 'Logo', 'twentythirteen' ),
    'section'  => 'twentythirteen_logo_section',
    'settings' => 'twentythirteen_logo',
) ) );
}
//add actions
add_action('customize_register', 'twentythirteen_theme_customizer');
add_action('init','home_page_carousel');

function home_page_carousel(){
	$labels = array(
		'name'                 => __( 'Carousel'),
		'singular_name'        => __( 'Item'),
		'all_items'            => __( 'All Items'),
		'add_new'              => __( 'Add New Item'),
		'add_new_item'         => __( 'Add New Item'),
		'edit_item'            => __( 'Edit Item'),
		'new_item'             => __( 'New Item'),
		'view_item'            => __( 'View Item'),
		'search_items'         => __( 'Search Items'),
		'not_found'            => __( 'No Item found'),
		'not_found_in_trash'   => __( 'No Item found in Trash'),
		'parent_item_colon'    => ''
	);
	$args = array(
		'labels'               => $labels,
		'public'               => true,
		'publicly_queryable'   => true,
		'_builtin'             => false,
		'show_ui'              => true,
		'query_var'            => true,
		'rewrite'              => array( "slug" => "home-carousel" ),
		'capability_type'      => 'post',
		'hierarchical'         => false,
		'menu_position'        => 20,
		'supports'             => array( 'title','thumbnail', 'page-attributes' ),
		'taxonomies'           => array(),
		'has_archive'          => true,
		'show_in_nav_menus'    => false
	);
	register_post_type( 'carousel', $args );
}

add_action('init','home_page_academics_carousel');



function home_page_academics_carousel(){

	$labels = array(

		'name'                 => __( 'Academics Carousel'),

		'singular_name'        => __( 'Item'),

		'all_items'            => __( 'All Items'),

		'add_new'              => __( 'Add New Item'),

		'add_new_item'         => __( 'Add New Item'),

		'edit_item'            => __( 'Edit Item'),

		'new_item'             => __( 'New Item'),

		'view_item'            => __( 'View Item'),

		'search_items'         => __( 'Search Items'),

		'not_found'            => __( 'No Item found'),

		'not_found_in_trash'   => __( 'No Item found in Trash'),

		'parent_item_colon'    => ''

	);

	$args = array(
		'labels'               => $labels,
		'public'               => true,
		'publicly_queryable'   => true,
		'_builtin'             => false,
		'show_ui'              => true,
		'query_var'            => true,
		'rewrite'              => array( "slug" => "academics-home-carousel" ),
		'capability_type'      => 'post',
		'hierarchical'         => false,
		'menu_position'        => 20,
		'supports'             => array( 'title','thumbnail', 'page-attributes' ),
		'taxonomies'           => array(),
		'has_archive'          => true,
		'show_in_nav_menus'    => false
	);

	register_post_type( 'academics_carousel', $args );

}

class Social_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Social_Widget', // Base ID
			__('social media', 'text_domain'), // Name
			array( 'description' => __( 'A social media Widget ', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
        $facebook = apply_filters( 'facebook', $instance['facebook'] );
        $twitter = apply_filters( 'twitter', $instance['twitter'] );
        $pinterest = apply_filters( 'pinterest', $instance['pinterest'] );
        $youtube = apply_filters( 'youtube', $instance['youtube'] );
        $google = apply_filters( 'google', $instance['google'] );
        $instagram = apply_filters( 'instagram', $instance['instagram'] );
        $linkedin = apply_filters( 'linkedin', $instance['linkedin'] );
        $vimeo = apply_filters( 'vimeo', $instance['vimeo'] );
        
        
		echo $args['before_widget'];
        if ( ! empty( $title ) ){
			echo $args['before_title'] . $title . $args['after_title'];
            }
        echo '<ul class="social_icon">';
		if ( ! empty( $facebook ) ){
            
			echo  '<li>'.'<a href='.$facebook.' class="facebook" target="_blank">Facebook</a>'.'</li>';
            }
            
         if ( ! empty( $twitter ) ){
			echo  '<li>'.'<a href='.$twitter.' class="twitter" target="_blank" >Twitter</a>'.'</li>';
            }
            
         if ( ! empty( $pinterest ) ){
			echo  '<li>'.'<a href='.$pinterest.' class="pinterest" target="_blank" >Pinterest</a>'.'</li>';
            }
         
         if ( ! empty( $youtube ) ){
			echo  '<li>'.'<a href='.$youtube.' class="youtube" target="_blank" >Youtube</a>'.'</li>';
            }
         
         if ( ! empty( $google ) ){
			echo  '<li>'.'<a href='.$google.' class="google_plus" target="_blank" >Google+</a>'.'</li>';
            }
            
         if ( ! empty( $instagram ) ){
			echo  '<li>'.'<a href='.$instagram.' class="instagram" target="_blank" >Instagram </a>'.'</li>';
            }
         if ( ! empty( $linkedin ) ){
			echo '<li>'.'<a href='.$linkedin.' class="linkedin"  target="_blank" >Linkedin</a>'.'</li>';
            } 
          if ( ! empty( $vimeo ) ){
			echo '<li>'.'<a href='.$vimeo.' class="vimeo"  target="_blank" >vimeo</a>'.'</li>';
            }    
         
		 echo '</ul>';
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance ) ) {
			$title = $instance[ 'title' ];
            $facebook = $instance[ 'facebook' ];
            $twitter = $instance[ 'twitter' ];
            $pinterest = $instance[ 'pinterest' ];
            $youtube = $instance[ 'youtube' ];
            $google = $instance[ 'google' ];
            $instagram = $instance[ 'instagram' ];
            $linkedin = $linkedin[ 'linkedin' ];
            $vimeo = $vimeo[ 'vimeo' ];
            
		}
		else {
			$title = __( 'New title', 'text_domain' );
            $facebook = __( 'New facebook', 'text_domain' );
            $twitter = __( 'New Twitter ', 'text_domain' );
            $pinterest = __( 'New pinterest', 'text_domain' );
            $youtube = __( 'New youtube', 'text_domain' );
            $google = __( 'New google', 'text_domain' );
            $instagram = __( 'New instance', 'text_domain' );
            $linkedin = __( 'New linkedin', 'text_domain' );
            $vimeo = __( 'New vimeo', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		
        
        
		<label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e( 'facebook:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" type="text" value="<?php echo esc_attr( $facebook ); ?>" />
		
        
        
		<label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e( 'twitter:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" type="text" value="<?php echo esc_attr( $twitter ); ?>" />
        
        <label for="<?php echo $this->get_field_id( 'pinterest' ); ?>"><?php _e( 'pinterest:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'pinterest' ); ?>" name="<?php echo $this->get_field_name( 'pinterest' ); ?>" type="text" value="<?php echo esc_attr( $pinterest ); ?>" />
        
        <label for="<?php echo $this->get_field_id( 'youtube' ); ?>"><?php _e( 'youtube:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'youtube' ); ?>" name="<?php echo $this->get_field_name( 'youtube' ); ?>" type="text" value="<?php echo esc_attr( $youtube ); ?>" />

        <label for="<?php echo $this->get_field_id( 'google' ); ?>"><?php _e( 'google:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'google' ); ?>" name="<?php echo $this->get_field_name( 'google' ); ?>" type="text" value="<?php echo esc_attr( $google ); ?>" />

        <label for="<?php echo $this->get_field_id( 'instagram' ); ?>"><?php _e( 'instagram:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'instagram' ); ?>" name="<?php echo $this->get_field_name( 'instagram' ); ?>" type="text" value="<?php echo esc_attr( $instagram ); ?>" />

        <label for="<?php echo $this->get_field_id( 'linkedin' ); ?>"><?php _e( 'linkedin:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'linkedin' ); ?>" name="<?php echo $this->get_field_name( 'linkedin' ); ?>" type="text" value="<?php echo esc_attr( $linkedin ); ?>" />
        
        <label for="<?php echo $this->get_field_id( 'vimeo' ); ?>"><?php _e( 'vimeo:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'vimeo' ); ?>" name="<?php echo $this->get_field_name( 'vimeo' ); ?>" type="text" value="<?php echo esc_attr( $vimeo ); ?>" />

		
        </p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['facebook'] = ( ! empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : '';
        $instance['twitter'] = ( ! empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : '';
        $instance['pinterest'] = ( ! empty( $new_instance['pinterest'] ) ) ? strip_tags( $new_instance['pinterest'] ) : '';
        $instance['youtube'] = ( ! empty( $new_instance['youtube'] ) ) ? strip_tags( $new_instance['youtube'] ) : '';
        $instance['google'] = ( ! empty( $new_instance['google'] ) ) ? strip_tags( $new_instance['google'] ) : '';
        $instance['instagram'] = ( ! empty( $new_instance['instagram'] ) ) ? strip_tags( $new_instance['instagram'] ) : '';
        $instance['linkedin'] = ( ! empty( $new_instance['linkedin'] ) ) ? strip_tags( $new_instance['linkedin'] ) : '';
        $instance['vimeo'] = ( ! empty( $new_instance['vimeo'] ) ) ? strip_tags( $new_instance['vimeo'] ) : '';
		return $instance;
	}

} 


    register_widget( 'Social_Widget' );   
    
function twentythieteen_widget_init()
{
register_sidebar( array(
'name' => 'Header Sidebar',
'id' => 'footer-sidebar-1',
'description' => 'Appears in the Header area',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => 'Content Sidebar ',
'id' => 'content-sidebar-1',
'description' => 'Appears in the content home template area',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => 'Blog Sidebar ',
'id' => 'blogs-sidebar-1',
'description' => 'Appears in the content home template area',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

	register_sidebar( array(
		'name' => 'Home Events ',
		'id' => 'home-events-1',
		'description' => 'Appears in the content home campus area',
		'before_widget' => '<aside id="%1$s" class="%2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="patten_tital">
                <h3>',
		'after_title' => '</h3>
            </div>',
	) );
}
add_action('widgets_init','twentythieteen_widget_init');  

// // Register Styles
// function my_register_styles() {
	// die('dinesh111111111111');
	// wp_register_style('custom-admin-css', get_stylesheet_directory_uri().'/css/admin.css', array(), '', 'all' );
// }
// add_action('wp_enqueue_scripts', 'my_register_styles');

// // Enqueue Styles
// function my_enqueue_styles() {
	// die('dinesh111111111111');
	// if (is_admin()) {
		// wp_enqueue_style( 'custom-admin-css' );
	// }
// }
// add_action('wp_enqueue_scripts', 'my_enqueue_styles');

function load_custom_wp_admin_style() {
	wp_register_style( 'custom_wp_admin_css', get_stylesheet_directory_uri() . '/css/admin.css', false, '1.0.0' );
	wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


// qTranslate it
function qtranslate_it( $content = '' ){
	if( $content == '' || is_array($content) ){
		return;
	}
	$translated_content = qtrans_useCurrentLanguageIfNotFoundUseDefaultLanguage( $content );
	return $translated_content;
}

function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Head blue',  
			'block' => 'span',  
			'classes' => 'the_directors_content',
			
			
		),
        
        array(  
			'title' => 'Head Green',  
			'block' => 'h3',  
			'classes' => 'entry-content',
			
		),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

function lang_button_func(){
	global $q_config;
	
	$return = '';
	
	$languages = $q_config['enabled_languages'];
	
	if( is_array($languages) ){
		// set links to translations of current page
		$return .= '<ul class="qtrans_language_chooser">';
		foreach( $languages as $language) {
			if($language != qtrans_getLanguage()) {
			}
			global $qtranslate_slug;
			$language_url = $qtranslate_slug->get_current_url($language);
			$return .= '<li class="lang-'.$language.'"><a hreflang="'.$language.'" href="'.$language_url.'" rel="alternate" /><span>'.$language.'</span></a></li>';
		}
		$return .= '</ul>';
	}
	// $return .= '<pre>xxxxxxxxxxx>>'.var_export($languages,false).'</pre>';
	return $return;
}
add_shortcode('lang_button', 'lang_button_func_dump');

if ( ! function_exists( 'twentythirteen_paging_nav_dump' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_paging_nav_dump() {
	global $wp_query;

	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 )
		return;
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'twentythirteen' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span><!--:fr--> Anciens articles <!--:--><!--:en-->Older posts <!--:-->', 'twentythirteen' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( '<!--:fr-->Articles récents <!--:--><!--:en-->Newer posts <!--:--> <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;
function register_styles(){
	
	wp_register_style( 'navgoco-css', get_stylesheet_directory_uri() . '/css/jquery.navgoco.css','',null);
}
function enqueue_styles(){
	wp_enqueue_style( 'navgoco-css' );
}

function register_scripts(){
	wp_register_script( 'navgoco-js', get_stylesheet_directory_uri() . '/js/jquery.navgoco.js', array( 'jquery' ), null, true);
}
function enqueue_scripts(){
	wp_enqueue_script( 'navgoco-js' );
	
}
/*
if(!is_admin())
{
	add_action( 'wp_enqueue_scripts', 'register_styles' );
	add_action( 'wp_enqueue_scripts', 'enqueue_styles' );
	
	// Scripts
	add_action( 'wp_enqueue_scripts', 'register_scripts' );
	add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );
		
	
}*/

if ( ! function_exists( 'wpex_style_select' ) ) {
	function wpex_style_select( $buttons ) {
		array_push( $buttons, 'styleselect' );
		return $buttons;
	}
}
add_filter( 'mce_buttons', 'wpex_style_select' );
/*


function my_mce_before_init_insert_data_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Blue',  
			'block' => 'span',  
            'classes'=> 'direction_content_blue',
            'styles' => array(
                'color' => '#004b97',
            ),
			
		),  
		array(  
			'title' => 'Green',  
			'inline' => 'span',  
            'classes'=> 'direction_content_green',
            'styles' => array(
                'color' => '#419438',
                
            ),
		),
		
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_data_formats' );*/

function cws_hidden_theme_12345( $r, $url ) {
	if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
		return $r; // Not a theme update request. Bail immediately.
 
	$themes = unserialize( $r['body']['themes'] );
	unset( $themes[ get_option( 'template' ) ] );
	unset( $themes[ get_option( 'stylesheet' ) ] );
	$r['body']['themes'] = serialize( $themes );
	return $r;
}
add_filter( 'http_request_args', 'cws_hidden_theme_12345', 5, 2 );

function five_posts_on_homepage( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
        // $query->set( 'posts_per_page', 1 );
		
		// Hide categories
		if( function_exists('get_field') ){
			$blog_category_hide = get_field('blog_category_hide', 'option');
		}
		
		// $query->set( 'category__not_in', array( 5, 6 ) );
        $query->set( 'category__not_in', $blog_category_hide );
    }
}
add_action( 'pre_get_posts', 'five_posts_on_homepage' );

// add_action( 'pre_get_posts', 'wpml_custom_query', 100 );
function wpml_custom_query( $query ) {
	$query->query_vars['posts_per_page'] = 1;
	
	if( is_author() ) {
	}
	
	return $query;
}


function wpcodex_filter_main_search_post_limits( $limit, $query ) {
	$paged = get_query_var('paged');
	
	$posts_per_page = get_option( 'posts_per_page', 5 );
	
	$limit_count = $posts_per_page;
	$limit_offset = 0;
	
	if( $paged != 0 ){
		$limit_offset = ($paged*$limit_count) - $limit_count;
	}
	
	// if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
	if ( ! is_admin() && $query->is_main_query() && $query->is_home() ) {
		return "LIMIT $limit_offset, $limit_count";
	}
	
	return $limit;
}

add_filter( 'post_limits', 'wpcodex_filter_main_search_post_limits', 100, 2 );
?>
