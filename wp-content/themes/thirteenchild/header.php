<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    ob_start("ob_gzhandler");
} else {
    ob_start();
}
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <?php /* ?> <meta charset="<?php bloginfo( 'charset' ); ?>"><?php */ ?>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <script src="<?php echo get_theme_root_uri(); ?>/thirteenchild/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo get_theme_root_uri(); ?>/thirteenchild/js/jquery-migrate-1.2.1.min.js"></script>
    <?php if (is_front_page()): ?>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <?php endif; ?>
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->
    <script src="<?php echo get_theme_root_uri(); ?>/thirteenchild/js/jquery.navgoco.js"></script>
    <link rel="stylesheet" href="<?php echo get_theme_root_uri(); ?>/thirteenchild/css/jquery.navgoco.css"/>
    <script type="text/javascript" id="left_inner_menu-javascript">
        jQuery(document).ready(function () {
            $('.current_page_parent').addClass('open');
            $("#left_inner_menu").navgoco({accordion: true});
        });
    </script>
    <?php wp_head(); ?>
    <!-- End Visual Website Optimizer Asynchronous Code -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <meta name="msapplication-TileColor" content="#da532c">


    <script type="text/javascript">
        adroll_adv_id = "B4VJGRF23NC2FJUQIJQC2H";
        adroll_pix_id = "AKQFKAYQWFFTHNFZ3SM6AZ";
        (function () {
            var oldonload = window.onload;
            window.onload = function () {
                __adroll_loaded = true;
                var scr = document.createElement("script");
                var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                scr.setAttribute('async', 'true');
                scr.type = "text/javascript";
                scr.src = host + "/j/roundtrip.js";
                ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
                if (oldonload) {
                    oldonload()
                }
            };
        }());
    </script>

    <?php
    $tourStarted = isset($_COOKIE['startTourClosed']) ? 'set_hide' : 'set_show';

    //print_r();exit;
    //site_url

    $fr = '';
    if (strpos($_SERVER['REQUEST_URI'], 'fr')) {
        $fr = '/fr';
    }
    ?>

</head>
<?php flush(); ?>
<body <?php body_class(); ?>>

<div id="startTour" class="animated  flipInX <?= $tourStarted; ?>">
    <h3><?php echo do_shortcode('[wpml_translate lang=\'en\'] Virtual Visit[/wpml_translate][wpml_translate lang=\'fr\']Visite virtuelle[/wpml_translate]'); ?></h3>
    <a href="<?php get_site_url(); ?><?php echo $fr; ?>/about-us/virtual-visit/"><img
            src="http://www.haut-lac.ch/wp-content/uploads/2015/12/map.png" alt="start tour"></a>

    <div id="close"><i class="fa fa-close"></i></div>
</div>
<div id="page" class="hfeed site">
    <header id="masthead" class="site-header" role="banner">
        <div class="wrapper">
            <div class="header_left">

                <div class='site-logo'>
                    <?php if (get_theme_mod('twentythirteen_logo')) { ?>

                        <a href='<?php echo esc_url(home_url('/')); ?>'
                           title='<?php echo esc_attr(get_bloginfo('name', 'display')); ?>' rel='home'><img
                                src='<?php echo esc_url(get_theme_mod('twentythirteen_logo')); ?>'
                                alt='<?php echo esc_attr(get_bloginfo('name', 'display')); ?>'></a>

                    <?php } else { ?>

                        <a class="home-link" href="<?php echo esc_url(home_url('/')); ?>"
                           title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">

                            <h1 class="site-title"><?php bloginfo('name'); ?></h1>

                            <h2 class="site-description"><?php bloginfo('description'); ?></h2>

                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="header_right">
                <?php
                if (is_active_sidebar('footer-sidebar-1')) {
                    dynamic_sidebar('footer-sidebar-1');
                }
                ?>
            </div>
        </div>

    </header><!-- #masthead -->

    <div id="navbar-sticky-wrapper" class="sticky-wrapper" style="height: 53px;">
        <div id="navbar" class="navbar">
            <div class="wrapper">
                <nav id="site-navigation" class="navigation main-navigation" role="navigation">
                    <?php get_template_part('content/header/nav/lang_select'); ?>
                    <?php get_template_part('content/header/nav/search'); ?>
                    <h3 class="menu-toggle"><?php _e('Menu', 'twentythirteen'); ?></h3>
                    <a class="screen-reader-text skip-link" href="#content"
                       title="<?php esc_attr_e('Skip to content', 'twentythirteen'); ?>"><?php _e('Skip to content', 'twentythirteen'); ?></a>
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav-menu', 'after' => '<span class="nav_arrow"></span>',)); ?>
                    <?php //get_search_form(); ?>
                </nav><!-- #site-navigation -->
            </div><!-- #navbar -->
        </div>
    </div>
    <div class="banner">
        <?php if (is_front_page()) {
            echo do_shortcode('[responsive_slider]');
        } ?>
    </div>

    <div id="main" class="site-main">

        <?php if (is_front_page()): ?>
            <?php include('recent_news.php'); ?>
        <?php endif; ?>
