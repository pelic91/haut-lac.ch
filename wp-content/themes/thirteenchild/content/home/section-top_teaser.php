

<?php if(false): ?>
<section class="school_section <?php echo get_row_layout();?> content_top <?php echo $third_class; ?>">
	<div class="welcome_area">
		<span class="welcome_tital"><?php the_sub_field('home_top_teaser_title'); ?></span>
		<p><?php the_sub_field('home_top_teaser_content'); ?></p>
		<?php
		$top_teaser_link_title = get_sub_field('home_top_teaser_link');
		if(!empty($top_teaser_link_title)){
			?>
			<a class="blue_but" href="<?php the_sub_field('home_top_teaser_link'); ?>"><span><?php  the_sub_field('top_teaser_link_title'); ?></span><span class="blue_but_shadow"></span></a>
			<?php
		}
		?>
	</div>
	<!--<a href="<?php //the_sub_field('home_top_teaser_link'); ?>"><?php  //the_sub_field('top_teaser_link_title'); ?></a> -->
</section>
<?php endif; ?>