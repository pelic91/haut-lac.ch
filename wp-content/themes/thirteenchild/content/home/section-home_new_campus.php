<?php
$capmpus_image_1             = get_sub_field('capmpus_image_1');
$capmpus_image_1_image_title = $capmpus_image_1['title'];
$capmpus_image_1_image_alt   = $capmpus_image_1['alt'];
$thumb_image_size            = 'medium';
$capmpus_image_1_img_thumb   = $capmpus_image_1['sizes'][$thumb_image_size];
$capmpus_image_1_thumb_width = $capmpus_image_1['sizes'][$thumb_image_size.'-width'];
$capmpus_image_1_thumb_height= $capmpus_image_1['sizes'][$thumb_image_size.'-height'];

$capmpus_image_2             = get_sub_field('capmpus_image_2');
$capmpus_image_2_image_title = $capmpus_image_2['title'];
$$capmpus_image_2_image_alt  = $capmpus_image_2['alt'];
$thumb_image_size            = 'medium';
$capmpus_image_2_img_thumb   = $capmpus_image_2['sizes'][$thumb_image_size];
$capmpus_image_2_thumb_width = $capmpus_image_2['sizes'][$thumb_image_size.'-width'];
$capmpus_image_2_thumb_height= $capmpus_image_2['sizes'][$thumb_image_size.'-height'];

$capmpus_image_3             = get_sub_field('capmpus_image_3');
$capmpus_image_3_image_title = $capmpus_image_3['title'];
$capmpus_image_3_image_alt   = $capmpus_image_3['alt'];
$thumb_image_size            = 'medium';
$capmpus_image_3_img_thumb   = $capmpus_image_3['sizes'][$thumb_image_size];
$capmpus_image_3_thumb_width = $capmpus_image_3['sizes'][$thumb_image_size.'-width'];
$capmpus_image_3_thumb_height= $capmpus_image_3['sizes'][$thumb_image_size.'-height'];
?>
<section class="<?php echo get_row_layout();?> <?php echo $third_class; ?>">
	<div class="new_campus">
		<div class="patten_tital">
			<span class="patten_blog_left"></span>
			<h2><?php the_sub_field('capmus_title');?></h2>
			<span class="patten_blog_right"></span>
		</div>
		<ul class="summer" id="flexiselsummer">
			<?php
			$carousel = new WP_Query( array(
				'post_type' => 'carousel',
				'order' => 'ASC',
				'orderby' => 'menu_order',
				'posts_per_page' => '-1'
			) );
			$carouselstr = '';
			$ci=1;
			if($carousel->have_posts()){
				while ( $carousel->have_posts() ){
					$carousel->the_post();
					global $post;
					
					if ( has_post_thumbnail() ) {
						?>
						<li class="<?php echo ( ( ($ci%3)==0 ) ? 'last-car' : '' );?>">
							<?php
							echo get_the_post_thumbnail( $post->ID, 'slide-thumbnail', array( 'class' => 'slide-thumbnail' ) );
							?>
						</li>
						<?php
						$ci++;
					}
				}
			}
			wp_reset_query();
			?>
		</ul>
	</div>
</section>