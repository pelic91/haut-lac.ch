<?php
/*
$capmpus_image_1             =get_sub_field('capmpus_image_1');
$capmpus_image_1_image_title = $capmpus_image_1['title'];
$capmpus_image_1_image_alt   = $capmpus_image_1['alt'];
$thumb_image_size            = 'medium';
$capmpus_image_1_img_thumb   = $capmpus_image_1['sizes'][$thumb_image_size];
$capmpus_image_1_thumb_width = $capmpus_image_1['sizes'][$thumb_image_size.'-width'];
$capmpus_image_1_thumb_height= $capmpus_image_1['sizes'][$thumb_image_size.'-height'];

$capmpus_image_2             =get_sub_field('capmpus_image_2');
$capmpus_image_2_image_title = $capmpus_image_2['title'];
$$capmpus_image_2_image_alt  = $capmpus_image_2['alt'];
$thumb_image_size            = 'medium';
$capmpus_image_2_img_thumb   = $capmpus_image_2['sizes'][$thumb_image_size];
$capmpus_image_2_thumb_width = $capmpus_image_2['sizes'][$thumb_image_size.'-width'];
$capmpus_image_2_thumb_height= $capmpus_image_2['sizes'][$thumb_image_size.'-height'];

$capmpus_image_3             =get_sub_field('capmpus_image_3');
$capmpus_image_3_image_title = $capmpus_image_3['title'];
$capmpus_image_3_image_alt   = $capmpus_image_3['alt'];
$thumb_image_size            = 'medium';
$capmpus_image_3_img_thumb   = $capmpus_image_3['sizes'][$thumb_image_size];
$capmpus_image_3_thumb_width = $capmpus_image_3['sizes'][$thumb_image_size.'-width'];
$capmpus_image_3_thumb_height= $capmpus_image_3['sizes'][$thumb_image_size.'-height'];
*/
?>
<div class="tab_frame">
    <div class="tab_white">
        <h2><?php echo do_shortcode('[wpml_translate lang=\'en\']Join the conversation[/wpml_translate][wpml_translate lang=\'fr\']Rejoignez la conversation[/wpml_translate]');?></h2>
        <ul class="nav nav-tabs">
            <li class="active facebook_tab"><a data-toggle="tab" id="facebook_tab" href="#home"><i class="fa fa-facebook"></i></a></li>
            <li class="twitter_tab"><a data-toggle="tab" href="#menu1" id="twitter_tab"><i class="fa fa-twitter"></i></a></li>
        </ul>

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active facebook_feed fadeIn ">
           <?php echo do_shortcode('[custom-facebook-feed]');?>
            </div>
            <div id="menu1" class="tab-pane fade twitter_feed fadeIn ">
                <?php echo do_shortcode("[AIGetTwitterFeeds ai_username='HautLac' ai_numberoftweets='1']"); ?>
            </div>
        </div>
    </div>
</div>


<section class="school_section <?php echo get_row_layout();?>">
	<div class="patten_tital">
		<span class="patten_blog_left"></span>
		<h2><?php the_sub_field('academics_title');?></h2>
		<span class="patten_blog_right"></span>
	</div>
	<?php
	$academics_infos = get_sub_field('academics_infos');
	$academics_infos_count = count($academics_infos);
	if( have_rows('academics_infos') ){
		?>
		<div class="academics-section academics-infos academics-infos-<?php echo $academics_infos_count;?> " data-match-height=".academics-section > section > .school_box">
			<?php
			while( have_rows('academics_infos') ){
				the_row();
				++$acainfo_sr;


				$acinfo_img        = get_sub_field('school_information_image');
				$acinfo_img_title  = $acinfo_img['title'];
				$acinfo_img_alt    = $acinfo_img['alt'];
				$thumb_image_size  = 'medium';
				$acinfo_img_src    = $acinfo_img['sizes'][$thumb_image_size];
				$acinfo_img_width  = $acinfo_img['sizes'][$thumb_image_size.'-width'];
				$acinfo_img_height = $acinfo_img['sizes'][$thumb_image_size.'-height'];

				$acinfo_title            = get_sub_field('school_information_title');
				$acinfo_desc             = get_sub_field('school_information_description');
				$acinfo_read_more_link   = get_sub_field('school_information_read_more');
				$acinfo_read_more_title  = get_sub_field('school_information_read_more_title');
				?>
				<div class="school_information school_section <?php echo ( ( ($acainfo_sr % $academics_infos_count)==0 ) ? 'last' : '' );?>">
					<div class="school_box">
						<div class="school_img">
							<img src="<?php echo $acinfo_img_src;?>" title="<?php echo $acinfo_img_title; ?>" alt="<?php echo $acinfo_img_alt; ?>"/>
							<span class="school_shadow"></span>
						</div>
						<div class="school_data">
							<span class="school_tital"><?php echo $acinfo_title;?></span>
							<?php
							if($acinfo_desc){
								?>
								<p class="school_details"><?php echo $acinfo_desc;?></p>
								<?php
							}
							?>
							<?php
							if( $acinfo_read_more_link ){
								?>
								<a class="white_but" href="<?php  echo $acinfo_read_more_link;?>">
								<?php
							}
							?>
							<span><?php echo $acinfo_read_more_title;?></span>
							<?php
							if( $acinfo_read_more_link ){
								?>
								</a>
								<?php
							}
							?>
							<span class="white_but_shadow"></span>
						</div>
						<span class="fold"></span>
						<div class="clearfix"></div>
					</div>
					<span class="school_box_shadow"></span>
				</div>
				<?php
			}
			?>
		</div>
		<?php
	}
	?>
	<div class="clearfix"></div>
</section>