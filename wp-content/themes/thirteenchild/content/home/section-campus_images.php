<section class="school_section <?php echo get_row_layout(); ?>">
    <div class="new_campus" style="width:100%">

        <div class="rpwe-block1">
            <div class="patten_tital">
                <div class="patten_tital">
                    <h3><?php echo do_shortcode('[wpml_translate lang=\'en\']This week at Haut-Lac[/wpml_translate][wpml_translate lang=\'fr\']Cette semaine à Haut-Lac[/wpml_translate]');?></h3>
                </div>
            </div>
            <?php dynamic_sidebar( 'home-events-1' ); ?>
        </div>
        <div class="rpwe-block2">
            <div class="patten_tital">
                <h3><?php echo do_shortcode('[wpml_translate lang=\'en\']Pictures of the Week[/wpml_translate][wpml_translate lang=\'fr\']Photos de la semaine[/wpml_translate]');?></h3>
            </div>
            <?php echo do_shortcode( '[instagram-feed  showheader=false]' );?>
        </div>
    </div>
</section>
