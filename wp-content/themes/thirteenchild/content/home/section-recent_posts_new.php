<?php
/*
global $sitepress;
$current_lang = $sitepress->get_current_language(); // save current language
$default_lang = $sitepress->get_default_language(); //get the default language
$sitepress->switch_lang($default_lang); //fetch posts in default language

//query args
$custom_query_args = array(
	'posts_per_page'   => 3,
    // 'cat' => 1
);

//build query
$custom_query = new wp_query($custom_query_args);

if ( $custom_query->have_posts() ) {
	//loop
	while ( $custom_query->have_posts() ) {
		$custom_query->the_post();
		
		//check if a translation exist
		$t_post_id = icl_object_id($post->ID, 'post', false, $current_lang);
		if(!is_null($t_post_id)){
			$t_post = get_post( $t_post_id);
			?>
			<a href="<?php echo get_permalink($t_post_id); ?>" title="<?php echo get_the_title($t_post_id); ?>"><?php echo get_the_title($t_post_id); ?></a>
			<?php
		}else{
			?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			<?php
		}
	}
	wp_reset_query();
}
$sitepress->switch_lang($current_lang);
?>
<br>
<br>
<br>
<br>
<br>
<?php */?>

<section class="school_section <?php echo get_row_layout();?>">
	<div class="widget rpwe_widget recent-posts-extended">
		<div class="patten_tital">
			<span class="patten_blog_left"></span>
				<h2><?php echo __('Latest News', 'twentythirteen')?></h2>
			<span class="patten_blog_right"></span>
		</div>
		<?php
		global $sitepress;
		$current_lang = $sitepress->get_current_language(); // save current language
		$default_lang = $sitepress->get_default_language(); //get the default language
		$sitepress->switch_lang($current_lang); //fetch posts in default language
		
		// echo $current_lang."==".$default_lang;
		$recent_posts_args = array(
			'posts_per_page'   => 3,
			// 'orderby'          => 'date',
			// 'order'            => 'DESC',
			// 'post_type'        => 'post',
			// 'post_status'      => 'publish',
			'post__not_in'     => get_option( 'sticky_posts' ),
			// 'suppress_filters' => true
		);
		$recent_posts_array = new wp_query($recent_posts_args);
		
		echo $recent_posts_array->request;
		if ( $recent_posts_array->have_posts() ) {
			?>
			<div class="rpwe-block ">
				<ul class="rpwe-ul">
					<?php
					while ( $recent_posts_array->have_posts() ) {
						$recent_posts_array->the_post();
							
						$post_id = get_the_ID();
						
						//check if a translation exist
						$t_post_id = icl_object_id($post->ID, 'post', false, $current_lang);
						if(!is_null($t_post_id)){
							$post_id = $t_post_id;
						}
						?>
						<li class="rpwe-li rpwe-clearfix">
							<?php
							if(!is_null($t_post_id)){
								$t_post = get_post( $t_post_id);
								?>
								<a href="<?php echo get_permalink($t_post_id); ?>" title="<?php echo get_the_title($t_post_id); ?>"><?php echo get_the_title($t_post_id); ?></a>
								<?php
							}else{
								?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								<?php
							}
							/*
							if(!is_null($t_post_id)){
								$t_post = get_post( $t_post_id);
								
								get_permalink($t_post_id);
								get_the_title($t_post_id);
								get_the_title($t_post_id);
							}else{
								get_permalink();
								get_the_title();
							}
							*/

							if ( has_post_thumbnail() ){
								?>
								<a class="rpwe-img" href="<?php echo esc_url( get_permalink($post_id) );?>" rel="bookmark">
									<img class="rpwe-alignleft rpwe-thumb" alt="" src="">
									<?php
									echo get_the_post_thumbnail( $post_id,
										'recent_post',
										array( 
											'class' => $args['thumb_align'] . ' rpwe-thumb the-post-thumbnail',
											'alt'   => esc_attr( get_the_title($post_id) )
										)
									);
									?>
								</a>
								<?php
							}
							?>
							<h3 class="rpwe-title">
								<a rel="bookmark" title="<?php echo sprintf( esc_attr__( 'Permalink to %s', 'rpwe' ), the_title_attribute( "echo=0&post=$post_id" ) );?>" href="<?php echo esc_url( get_permalink($post_id) );?>">
									<?php echo esc_attr( get_the_title($post_id) );?>
								</a>
							</h3>
							<?php
							$date = get_the_date('', $post_id);
							?>
							<time datetime="<?php echo esc_html( get_the_date( 'c', $post_id ) );?>" class="rpwe-time published"><?php echo esc_html( $date );?></time>
							<div class="rpwe-summary">
								<?php
								echo wp_trim_words( get_the_excerpt(), 25, ' &hellip;' );
								?>
								<a href="<?php echo esc_url( get_permalink($post_id) );?>" class="more-link"><?php echo __('read more');?></a>
							</div>
						</li>
						<?php
					}
					?>
				</ul>
			</div>
			<?php
		}
		wp_reset_query();
		$sitepress->switch_lang($current_lang);
		?>
	</div>
</section>