<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/2/2015
 * Time: 4:54 PM
 */

?>
<div class="wrapper">
    <section class="school_section logos-hide_arroaws">

        <ul class="flexiselsummer_two logo_slider">
            <?php
            while (have_rows('logos')) {
                the_row();

                // vars
                $image = get_sub_field('schoolinfo_logo_image');
                $title = get_sub_field('schoolinfo_logo_image_title');
                $link = get_sub_field('schoolinfo_logo_image_link');

                // $school_logo_title = get_sub_field('schoolinfo_logo_image_title');
                // $school_logo_image = get_sub_field('schoolinfo_logo_image');
                // $school_logo_image = $school_logo_image['url'];
                // $school_logo_link  = get_sub_field('schoolinfo_logo_image_link');
                ?>
                <li class="slide">
                    <?php if ($link): ?>
                    <a href="<?php echo $link; ?>">
                        <?php endif; ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>"
                             title="<?php echo $title; ?>"/>
                        <?php if ($link): ?>
                    </a>
                <?php endif; ?>
                </li>
                <?php
            }
            ?>
        </ul>

    </section>
</div>