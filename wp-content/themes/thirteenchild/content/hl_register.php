<div id="contentlong">
<?php
/*
			echo "<pre>";
			print_r($_SESSION);
			echo "</pre>";
*/
			require_once(ABSPATH.'gestion/inc/function/config.php');
			require_once(ABSPATH.'gestion/inc/class/user.class.php');
			
			$user = new User($db);
			$err = false;

			$tmp = explode('/',$_SERVER['REQUEST_URI']);
			
			if(qtrans_getLanguage()=='en'){
				$tmp[2] = "eng";
			}elseif(qtrans_getLanguage()=='fr') {
				$tmp[2] = "fre";
			}else{
				$tmp[2] = "eng";
			}
			
			$lang = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? '/eng' : '/'.$tmp[2];
			$langraw = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? 'eng' : $tmp[2];
			
			$submit_label = $lang=='/eng' ? 'Send ' : 'Envoyer ';
			
			if (!empty($_GET['activate'])){
				$hash = $user->escape($_GET['activate']);
				$sql = $user->query("SELECT `{$user->tbFields['active']}` FROM `{$user->dbTable}` WHERE `activationHash` = '$hash' LIMIT 1",__LINE__);
				if ( $sql->num_rows==1 ){
					$res = $sql->fetch_array();
					if ( $res[0] == 1 ) {
						echo $lang=='/eng' ? 'Your account is already active.' : 'Votre compte est d&eacute;j&agrave; activ&eacute;.';
					}
						
					else{
						if ($user->query("UPDATE `{$user->dbTable}` SET `{$user->tbFields['active']}` = 1 WHERE `activationHash` = '$hash' LIMIT 1", __LINE__)) {
							echo $lang=='/eng' ?
								'Your account has been activated, <a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/inscription-'.$lang.'">Please login now.</a>':
								'Votre compte vient d\'&ecirc;tre activ&eacute;, <a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/inscription-'.$lang.'">veuillez vous identifier &agrave; pr&eacute;sent.</a>';
	
						} else {
							echo $lang=='/eng' ? 'Error, please contact the administrator.': 'Erreur, veuillez contacter l\'administrateur.';
						}
					}
				} else {
					echo $lang=='/eng' ? 'This account does not exist.': 'Ce compte n\'existe pas.';
				}
				$err = true;
			}

			if (!empty($_POST)) {
				$erreur = array();

				if (!preg_match('/^[a-zA-Z0-9._-]{1,64}@[a-z0-9._-]{2,64}\.[a-z]{2,4}$/', $_POST['login'])) {$erreur[0] = true; $err = true;}
				if (!preg_match('/^[a-zA-Z0-9]{6,12}$/', $_POST['pass'])) {$erreur[1] = true; $err = true;}
				if ($_POST['pass'] != $_POST['pass2']) {$erreur[2] = true; $err = true;}
				
				if (!$err) {
					$hash = $user->randomPass(40);
					$data = array(
						'email' => $_POST['login'],
						'password' => $_POST['pass'],
						'activationHash' => $hash,
						'active' => 0
					);
					$userID = $user->insertUser($data);
					if ($userID==0) {
						echo $lang=='/eng' ? 'An error occurred while creating your account.': 'Une erreur est survenue lors de la cr&eacute;ation de votre compte.';
					} else {
						/*echo $lang=='/eng' ? 
							'Your account has been created, thank you to validate the link in the email you will receive.': 
							'Votre compte vient d\'&ecirc;tre cr&eacute;&eacute;, merci de valider le lien dans l\'email que vous allez recevoir.';
						$email_content = $lang=='/eng' ? 'To activate your account, click (or paste) the following link : ' : 'Pour activer votre compte, cliquez sur (ou collez) le lien suivant : ';
						$email_subject = $lang=='/eng' ? 'Activate your account' : 'Activez votre compte';
						$email = $email_content.'http://'. $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] .'?activate='.$hash;
												
						mail($_POST['login'], 'Haut Lac - '.$email_subject, $email);*/
						
						echo $lang=='/eng' ? 
							'Your account has been created.': 
							'Votre compte vient d\'&ecirc;tre cr&eacute;&eacute;.';
						
					}
				}
			}
			if (empty($_GET['activate']) && !$email) {
				if ($lang=='/eng') {
					echo '
				<p>Create an account (all fields are required)<br /><br /></p>
				<form name="login" method="post">
					<label class="login">Email address</label><input type="text" name="login" value="'.$_POST['login'].'" />';

					if ($erreur[0])
						{echo '<br/><span class="login_error">Invalid email address</span>' ;} else
						{echo '<br/><span class="login_info">An email containing a validation link will be sent</span>';}

					echo '
					<br />
					<label class="login">Password</label><input type="password" name="pass" />';
						
					if ($erreur[1])
						{echo '<br/><span class="login_error">Between 6 and 12 characters (only letters and numbers)</span>' ;} else
						{echo '<br/><span class="login_info">Between 6 and 12 characters (only letters and numbers)</span>';}

					echo '
					<br />
					<label class="login">Checking password</label><input type="password" name="pass2" />';

					if ($erreur[2])
						{echo '<br/><span class="login_error">Password not identical</span>' ;} else
						{echo '<br/><span class="login_info">Rewrite your password</span>';}

					echo '
					<br />
					<input type="submit" value="'.$submit_label.'" />&nbsp;<input type="button" onclick="window.location.replace(\'index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/inscription-'.$langraw.'\');" value="Retour" />
				</form>';
				} else {
					echo '
				<p>Cr&eacute;er un compte (tous les champs sont obligatoires)<br /><br /></p>
				<form name="login" method="post">
					<label class="login">Adresse email</label><input type="text" name="login" value="'.$_POST['login'].'" />';

					if ($erreur[0])
						{echo '<br/><span class="login_error">Adresse email incorrect</span>' ;} else
						{echo '<br/><span class="login_info">Un email contenant un lien de validation vous sera envoy&eacute;</span>';}

					echo '
					<br />
					<label class="login">Mot de passe</label><input type="password" name="pass" />';
						
					if ($erreur[1])
						{echo '<br/><span class="login_error">Entre 6 et 12 caract&egrave;res (seulement en lettre et en chiffre)</span>' ;} else
						{echo '<br/><span class="login_info">Entre 6 et 12 caract&egrave;res (seulement en lettre et en chiffre)</span>';}

					echo '
					<br />
					<label class="login">V&eacute;rification du mot de passe</label><input type="password" name="pass2" />';

					if ($erreur[2])
						{echo '<br/><span class="login_error">Mot de passe non identique</span>' ;} else
						{echo '<br/><span class="login_info">R&eacute;&eacute;crivez votre mot de passe</span>';}

					echo '
					<br />
					<input type="submit" value="'.$submit_label.'" />&nbsp;<input type="button" onclick="window.location.replace(\'index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/inscription-'.$langraw.'\');" value="Retour" />
				</form>';
				
				}
			} 
?>
</div>