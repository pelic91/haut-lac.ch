<div id="contentlong">
<?php
/*
			echo "<pre>";
			print_r($_SESSION);
			echo "</pre>";
*/
//			$edit_or_new_folder = get_permalink(757);

			$login_hl_page = get_permalink(2037);
			$edit_or_new_folder = get_permalink(2048);
			$logout_page = get_permalink(2042);

			require_once(ABSPATH.'gestion/inc/function/config.php');
			require_once(ABSPATH.'gestion/inc/class/user.class.php');
		
			$user = new User($db);
			
			$tmp = explode('/',$_SERVER['REQUEST_URI']);
			
			if(qtrans_getLanguage()=='en') {
				$tmp[2] = "eng";
			}elseif(qtrans_getLanguage()=='fr') {
				$tmp[2] = "fre";
			}else{
				$tmp[2] = "eng";
			}
			
			$lang = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? '/eng' : '/'.$tmp[2];
			$langraw = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? 'eng' : $tmp[2];
			
			if ($user->is_loaded()){
				$folder = $user->get_folder();
				if (count($folder)==0) {
					echo $lang=='/eng' ?
						'<p>You don\'t have any current folder.</p>':
						'<p>Vous n\'avez aucun dossier en cours.</p>';
				} else {
					echo $lang=='/eng' ?
						'<p>You have '.count($folder).' folder(s).</p>':
						'<p>vous avez '.count($folder).' dossier(s).</p>';
					echo '<ul>';
					$name_folder = $lang=='/eng' ? 'Folder' : 'Dossier';
					$name_open  = $lang=='/eng' ? 'Open' : 'Ouvert';
					$name_load = $lang=='/eng' ? 'Folder under investigation' : 'Dossier en cours d\'&eacute;tude';
					$name_finish = $lang=='/eng' ? 'Finished' : 'Termin&eacute;';
					foreach ($folder as $number => $state) {
						$who = ($folder[$number][2] && $folder[$number][3]) ? '&laquo;'.$folder[$number][2]['response'].'&nbsp;'.$folder[$number][3]['response'].'&raquo;&nbsp;' : '' ;
//						if ($folder[$number]['State']==0) echo '<li>'.$name_folder.' <a href="index.php'.$lang.'/load_folder-'.$langraw.'?folder='.$number.'">'.$who.'#HL-'.date('Y').'-'.$number.'</a>&nbsp;('.$name_open.')</li>';
						if ($folder[$number]['State']==0) echo '<li>'.$name_folder.' <a href="'.$edit_or_new_folder.'?folder='.$number.'">'.$who.'#HL-'.date('Y').'-'.$number.'</a>&nbsp;('.$name_open.')</li>';
						if ($folder[$number]['State']==1) echo '<li>'.$name_folder.' '.$who.'#HL-'.date('Y').'-'.$number.'&nbsp;('.$name_load.')</li>';
						if ($folder[$number]['State']==2) echo '<li>'.$name_folder.' '.$who.'#HL-'.date('Y').'-'.$number.'&nbsp;('.$name_finish.')</li>';

					}
					echo '</ul>';
				}
/*
				echo $lang=='/eng' ?
						'<p><a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/load_folder-'.$langraw.'">Create a new folder</a></p>':
						'<p><a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/load_folder-'.$langraw.'">Cr&eacute;er un nouveau dossier</a></p>';
*/

				echo $lang=='/eng' ?
						'<p><a href="'.$edit_or_new_folder.'">Create a new folder</a></p>':
						'<p><a href="'.$edit_or_new_folder.'">Cr&eacute;er un nouveau dossier</a></p>';

/*
				echo $lang=='/eng' ?
						'<p><a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/login_out-'.$langraw.'">Login out</a></p>':
						'<p><a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/login_out-'.$langraw.'">D&eacute;connexion</a></p>';
*/
				echo $lang=='/eng' ?
						'<p><a href="'.$logout_page.'">Login out</a></p>':
						'<p><a href="'.$logout_page.'">D&eacute;connexion</a></p>';
			} else {
				
				/*
				echo $lang=='/eng' ?
						'<p>You must be <a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/inscription-'.$langraw.'"> identify </ a> to access your folders.</p>':
						'<p>Vous devez &ecirc;tre <a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/inscription-'.$langraw.'">identifier</a> pour acc&eacute;der &agrave; vos dossiers.</p>';
				*/
				
				echo $lang=='/eng' ?
						'<p>You must be <a href="'.$login_hl_page.'"> identify </ a> to access your folders.</p>':
						'<p>Vous devez &ecirc;tre <a href="'.$login_hl_page.'">identifier</a> pour acc&eacute;der &agrave; vos dossiers.</p>';
			}
?>
</div>