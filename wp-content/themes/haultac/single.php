<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Haultac
 */

get_header(); ?>


<?php if (has_post_thumbnail()): ?>
    <div class="banner">
        <?php the_post_thumbnail(); ?>
    </div>
<?php endif; ?>

    <div class="under_header_color">
        <div class="box_1"></div>
        <div class="box_2"></div>
        <div class="box_3"></div>
        <div class="box_4"></div>
    </div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="inner_page blog_page">
                <div class="container">
                    <div class="col-md-4">

                        <?php if (is_active_sidebar('blog_sidebar_menu')) : ?>
                            <div id="blog_sidebar_menu" class="blog_sidebar_menu widget-area" role="complementary">
                                <?php dynamic_sidebar('blog_sidebar_menu'); ?>
                            </div><!-- #primary-sidebar -->
                        <?php endif; ?>


                    </div>
                    <div class="col-md-8">

                        <?php
                        while (have_posts()) : the_post();

                            get_template_part('template-parts/content', 'single');


                            // If comments are open or we have at least one comment, load up the comment template.
                            if (comments_open() || get_comments_number()) :
                                comments_template();
                            endif;

                        endwhile; // End of the loop.
                        ?>
                       <div class=navigation>
                           <div class="nav-box previous" style="float: left; text-align: left;"> <?php
                               $prevPost = get_previous_post();
                               $prevthumbnail = get_the_post_thumbnail($prevPost->ID, array(100,100));
                               previous_post_link('%link','%title '.$prevthumbnail.''); ?>
                           </div>

                           <?php // Get thumbnail of next post?>
                           <div class="nav-box next" style="float: right; text-align: right;"> <?php
                               $nextPost = get_next_post();
                               $nextthumbnail = get_the_post_thumbnail($nextPost->ID, array(100,100));
                               $title = "<div class='title'>next_post_link('%title')</div>";
                               next_post_link('%link', '%title'.$nextthumbnail.'');

                               ?>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
