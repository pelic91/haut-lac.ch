<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Haultac
 */
$base = get_stylesheet_directory_uri();
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
    <!-- Bootstrap core CSS -->
    <link href="<?= $base; ?>/style.css" rel="stylesheet">
    <link href="<?= $base; ?>/css/font-awesome.css" rel="stylesheet">
    <link href="<?= $base; ?>/preloader.css" rel="stylesheet">
    <meta content="IE=9" http-equiv="X-UA-Compatible">
<?php

$class = '';
if(is_user_logged_in()){
    $class = 'logged-in';
}
?>

</head>

<body <?php body_class(); ?> id="<?= $class;?>" >
<?php if(is_front_page()): ?>
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
</div>

<?php endif; ?>

<header>
    <div class="container_full_size">
        <div class="container">
            <div class="header_sector">
                <div class="col-md-3">
                    <div class="logo">
                        <a href="<?php echo get_option('home'); ?>"><img src="<?= $base ?>/images/logo.png"
                                                                         alt=" <?php bloginfo('name'); ?>"
                                                                         title=" <?php bloginfo('name'); ?>"
                                                                         class="img-responsive"></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="col-md-8" style="text-align: center">
                    <span class="simple_text">
                        <?php wpml_e__if_language( ' "Living, learning and achieving"', 'en' ); ?><?php wpml_e__if_language( '"Vivre, apprendre, réussir"', 'fr'); ?>
                    </span>
                    </div>
                    <div class="col-md-4">

                        <div class="info">
                            <div class="telephone_number">
                                <a class="login_to_intr" href="http://intranet.haut-lac.ch/"><img src="<?= $base ?>/images/login-HL.png"/></a>

                            </div>
                            <div class="social">
                                <a href="https://www.facebook.com/hautlac/" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/HautLac" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a href="https://www.linkedin.com/company/haut-lac-international-bilingual-school" target="_blank"><i class="fa fa-linkedin"></i></a>
                            </div>
                            <div class="lang">

                                <?php if (is_active_sidebar('lang_switch')) : ?>
                                    <div id="language_switcher" class="switcher widget-area" role="complementary">
                                        <?php dynamic_sidebar('lang_switch'); ?>
                                    </div><!-- #primary-sidebar -->
                                <?php endif; ?>

                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container_full_size">
        <nav class="navbar navbar-default" role="navigation" id="navbar-sticky-wrapper">


            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo get_option('home'); ?>"><img src="<?= $base;?>/images/logo_ideogram.png"></a>
                </div>

                <?php
                wp_nav_menu(array(
                        'menu' => 'primary',
                        'theme_location' => 'primary',
                        'depth' => 2,
                        'container' => 'div',
                        /*   ' '=> 'test',*/
                        'container_class' => 'collapse navbar-collapse in',
                        'container_id' => 'bs-example-navbar-collapse-1',
                        'menu_class' => 'nav navbar-nav',
                        'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                        'walker' => new wp_bootstrap_navwalker())
                );
                ?>

                <div class="search-form_header">
                    <form class="navbar-form form-inline" role="search" method="get" id="searchformtop" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div class="input-group clearfix">
                            <input name="s" id="search_lg" style="min-width: 4em;" type="text" class="search-query form-control pull-right s_exp" autocomplete="off" placeholder="">

                            <div class="input-group-btn">
                                <button class="btn btn-transparent">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </nav>
    </div>

</header>


<?php if (is_front_page()) : ?>
    <div class="banner" style="">
        <?= do_shortcode('[responsive_slider]') ?>
        <div class="banner-image">
            <img src="<?= $base ?>/images/banner_middle.png">
        </div>
        <div id="scroll_to">
            <span class="btn_srcoll">
              <a href="#about_section" class="go_to_about_section"> <i
                      class="fa fa-chevron-circle-down"><span>Scroll</span></i></a>
            </span>
        </div>
    </div>
<?php endif; ?>



