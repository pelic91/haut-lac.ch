<?php
/**
 * Haultac functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Haultac
 */

if (!function_exists('haultac_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function haultac_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Haultac, use a find and replace
         * to change 'haultac' to the name of your theme in all the template files.
         */
        load_theme_textdomain('haultac', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'haultac'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Formats.
         * See https://developer.wordpress.org/themes/functionality/post-formats/
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('haultac_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));
    }
endif;
add_action('after_setup_theme', 'haultac_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function haultac_content_width()
{
    $GLOBALS['content_width'] = apply_filters('haultac_content_width', 640);
}

add_action('after_setup_theme', 'haultac_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function haultac_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'haultac'),
        'id' => 'sidebar-1',
        'description' => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'haultac_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function haultac_scripts()
{
  //  wp_enqueue_style('haultac-style', get_stylesheet_uri());

    wp_enqueue_script('haultac-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);

    wp_enqueue_script('haultac-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'haultac_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';

register_nav_menus(array(
    'primary' => __('Primary Menu', 'Haultac'),
));



include('inc/videos-function.php');


add_image_size( 'mycustomsize', 219,165, true );

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init()
{

    register_sidebar(array(
        'name' => 'Lang Swicther',
        'id' => 'lang_switch',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="hidden">',
        'after_title' => '</h2>',
    ));

}

add_action('widgets_init', 'arphabet_widgets_init');


function sidebar_widgets_init()
{

    register_sidebar(array(
        'name' => 'Sidebar Menu Widget',
        'id' => 'sidebar_menu',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        /*   'before_title' => '<h2>',
           'after_title'   => '</h2>',*/
    ));

}



add_action('widgets_init', 'sidebar_widgets_init');


function blog_sidebar_widgets_init()
{

    register_sidebar(array(
        'name' => 'Blog Sidebar Menu Widget',
        'id' => 'blog_sidebar_menu',
        'before_widget' => '<div class="popular">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title'   => '</h4>',
    ));

}

add_action('widgets_init', 'blog_sidebar_widgets_init');


function wpb_adding_scripts()
{
    wp_enqueue_script('custom', get_template_directory_uri() . '/js/custom.js', array(), '20120206', true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), true);
}

add_action('wp_enqueue_scripts', 'wpb_adding_scripts');


function haultac_paging_nav()
{
    global $wp_query;

    // Don't print empty markup if there's only one page.

    ?>

    <nav class="navigation paging-navigation" role="navigation">
        <h1 class="screen-reader-text"><?php _e('Posts navigation', 'twentythirteen'); ?></h1>

        <div class="nav-links">

            <?php if (get_next_posts_link()) : ?>
                <div
                    class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&larr;</span><!--:fr--> Anciens articles <!--:--><!--:en-->Older posts <!--:-->', 'twentythirteen')); ?></div>
            <?php endif; ?>

            <?php if (get_previous_posts_link()) : ?>
                <div
                    class="nav-next"><?php previous_posts_link(__('<!--:fr-->Articles récents <!--:--><!--:en-->Newer posts <!--:--> <span class="meta-nav">&rarr;</span>', 'twentythirteen')); ?></div>
            <?php endif; ?>

        </div><!-- .nav-links -->
    </nav><!-- .navigation -->
    <?php
}


/********************* Add a class on body  *****************************/

add_filter('body_class', 'wp_parent_body_class');
function wp_parent_body_class($classes)
{
    if (is_page()) {
        $classes = array();
        $parents = get_post_ancestors(get_the_ID());
        $id = ($parents) ? $parents[count($parents) - 1] : get_the_ID();
        if (!$id) {
            $id = get_the_ID();
        }
        $map = array(56 => 'about-us', 57 => 'academics', 691 => 'school-life', 11935 => 'about-us', 12061 => 'academics', 12211 => 'school-life', 708 => 'admissions', 12229 => 'admissions', 2089 => 'contact', 12317 => 'contact', 711 => 'calendar', 12232 => 'calendar');
        $classes[] = isset($map[$id]) ? 'body-' . $map[$id] : 'body';

    }

    return $classes;
}



function alx_browser_body_class( $classes ) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

    if($is_lynx) $classes[] = 'lynx';
    elseif($is_gecko) $classes[] = 'gecko';
    elseif($is_opera) $classes[] = 'opera';
    elseif($is_NS4) $classes[] = 'ns4';
    elseif($is_safari) $classes[] = 'safari';
    elseif($is_chrome) $classes[] = 'chrome';
    elseif($is_IE) {
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $browser = substr( "$browser", 25, 8);
        if ($browser == "MSIE 7.0"  ) {
            $classes[] = 'ie7';
            $classes[] = 'ie';
        } elseif ($browser == "MSIE 6.0" ) {
            $classes[] = 'ie6';
            $classes[] = 'ie';
        } elseif ($browser == "MSIE 8.0" ) {
            $classes[] = 'ie8';
            $classes[] = 'ie';
        } elseif ($browser == "MSIE 9.0" ) {
            $classes[] = 'ie9';
            $classes[] = 'ie';
        } else {
            $classes[] = 'ie';
        }
    }
    else $classes[] = 'unknown';

    if( $is_iphone ) $classes[] = 'iphone';

    return $classes;
}
add_filter( 'body_class', 'alx_browser_body_class' );





/****************Options Theme *****************/

