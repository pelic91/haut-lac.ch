<?php
$category__in = 999999999;
if( function_exists('get_field') ){
    $tmpl_news_category = get_field('tmpl_news_category');
}
if( $tmpl_news_category ){
    $category__in = 13;
}
//print_r($_SERVER);

if(strpos('fr', $_SERVER['REQUEST_URI'])){
    $category__in = 24;
}
// global $wp_query;

//echo $category__in;exit;
// the query
$the_query = new WP_Query( 'category__in='.$category__in.'&posts_per_page=3' ); ?>

<div class="recent-articles-wrapper">
    <div class="recent-articles-header">
        <?php
        if( function_exists('get_field') ){
            $recent_news_title = get_field('recent_news_title');
        }
        if( !$recent_news_title ){
            $recent_news_title = 'Recent Articles';
        }
        ?>
        <h2><?php echo $recent_news_title;?></h2>
    </div>
    <?php
    // Posts are found
    if ( $the_query->have_posts() ) {
        ?>
        <div class="recent-articles">
            <?php
            $i = 1;
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                global $post;
                ?>
                <div id="post-<?php the_ID(); ?>" class="recent-article<?php echo ( $i == 3 ? ' last' : '' );?>">
                    <div class="article-thumb">
                        <a href="<?php the_permalink(); ?>">
                            <?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail('mycustomsize');
                            }else{
                                $default_src = get_stylesheet_directory_uri().'/images/default/default_199x149.jpg';
                                $img_src = image_resize($default_src, 219,165);
                                ?>
                                <?php /* ?><img src="http://dummyimage.com/199x149/e1e1e1/ffffff.png&text=<?php the_title(); ?>" alt="<?php the_title(); ?>"/><?php */?>
                                <img src="<?php echo $img_src;?>" alt="<?php the_title(); ?>"/>
                                <?php
                            }
                            ?>
                        </a>
                    </div>
                    <h3 class="article-title">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h3>
                </div>
                <?php
                $i++;
            }
            ?>
        </div>
        <?php
        // Reset Query
        wp_reset_postdata();

    }else{ // Posts not found
        echo '<h4>' . __( 'Posts not found', 'su' ) . '</h4>';
    }
    ?>
    <div class="clearfix"></div>
</div>