<?php
wp_enqueue_script('prettyphoto');
$category__in = 999999999;
if( function_exists('get_field') ){
    $tmpl_news_category = get_field('tmpl_news_category');
}
if( $tmpl_news_category ){
    $category__in = $tmpl_news_category;
}
// global $wp_query;

// the query
// $the_query = new WP_Query( 'category__in='.$category__in.'&posts_per_page=3' );
// $the_query = new WP_Query( array( 'post_type' => 'post', 'post__not_in' => array( 2, 5, 12, 14, 20 ) ) );
$the_query = new WP_Query( array( 'post_type' => 'video',  'posts_per_page' => 3 ) );
?>

<div class="recent-articles-wrapper">
    <div class="recent-articles-header">
        <?php
        if( function_exists('get_field') ){
            $recent_videos_title = get_field('recent_videos_title');
        }
        if( !$recent_videos_title ){
            $recent_videos_title = 'Recent Videos';
        }
        ?>
        <h2><?php echo $recent_videos_title;?></h2>
    </div>
    <?php
    // Posts are found
    if ( $the_query->have_posts() ) {
        ?>
        <div class="recent-articles">
            <?php
            $i = 1;
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                global $post;

                $video = get_field('video');
                $video_url = get_post_meta( get_the_ID(), 'video', true );

                if( $video ){
                    ?>
                    <div id="post-<?php the_ID(); ?>" class="recent-article recent-video<?php echo ( $i == 3 ? ' last' : '' );?>">
                        <div class="article-thumb">
                            <?php /* ?><a href="#ipad-video-<?php the_ID(); ?>" rel="prettyPhoto"><?php */ ?>
                            <?php
                            // https://www.youtube.com/watch?v=w9OhG7Wx1CY
                            ?>
                            <a href="<?php echo $video_url;?>" rel="prettyPhoto" title="<?php echo $video->title;?>">
                                <?php

                              //  print_r($video);
                                /*
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('recent_article_thumb');
                                }else{
                                    ?>
                                    <img src="http://dummyimage.com/199x149/e1e1e1/ffffff.png&text=<?php the_title(); ?>" alt="<?php the_title(); ?>"/>
                                    <?php
                                }
                                */
                                ?>
                                <?php echo $video->html;?>

                                <span class="player-btn"></span>
                            </a>
                        </div>
                        <h3 class="article-title">
                            <?php the_title(); ?>
                        </h3>
                    </div>
                    <?php
                    $i++;
                }
            }
            ?>
        </div>
        <?php
        wp_reset_postdata(); // Reset Query

    }else{ // Posts not found
        echo '<h4>' . __( 'Posts not found', 'su' ) . '</h4>';
    }
    ?>
    <div class="clearfix"></div>
</div>