<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Haultac
 */

get_header(); ?>

<div class="under_header_color">
    <div class="box_1"></div>
    <div class="box_2"></div>
    <div class="box_3"></div>
    <div class="box_4"></div>
</div>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">


        <div class="container inner_page">
            <div class="col-md-12">
                <header class="entry-header">
                    <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
                </header><!-- .entry-header -->
            </div>
            <div class="col-md-4">

                <?php if (is_active_sidebar('sidebar_menu')) : ?>
                    <div id="sidebar_menu" class="sidebar_menu widget-area" role="complementary">
                        <?php dynamic_sidebar('sidebar_menu'); ?>
                    </div><!-- #primary-sidebar -->
                <?php endif; ?>
            </div>
            <div class="col-md-8">
                <?php while (have_posts()) : the_post(); ?>

                    <?php get_template_part('template-parts/content', 'afterLogin'); ?>

                <?php endwhile; // end of the loop. ?>
            </div>
        </div>


    </main>


    <!-- #main -->

</div><!-- #primary -->


<?php get_footer(); ?>
