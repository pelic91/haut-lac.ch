<?php
/**
 * Template Name: Page Template
 *
 */
$base = get_stylesheet_directory_uri();

get_header();
?>
<div class="under_header_color">
    <div class="box_1"></div>
    <div class="box_2"></div>
    <div class="box_3"></div>
    <div class="box_4"></div>
</div>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="inner_page">
            <div class="container ">
                <div class="col-md-12">
                    <header class="entry-header">
                        <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
                    </header><!-- .entry-header -->
                </div>
            </div>

            <?php while (have_posts()) : the_post(); ?>

                <?php get_template_part('template-parts/content', 'afterLogin'); ?>

            <?php endwhile; // end of the loop. ?>
        </div>

    </main>


    <!-- #main -->

</div><!-- #primary -->


<?php get_footer(); ?>
