/**
 * Created by User on 3/3/2016.
 */
    //PRELOADER
jQuery(window).load(function () {
    jQuery('#status').delay(800).fadeOut('slow');
    jQuery('#preloader').delay(1000).fadeOut('slow');
    jQuery('body').delay(2000).css({'overflow': 'visible'});
    jQuery(".spinner > div").css({"background-color": "#ffffff"});
    jQuery("#preloader").css({"background-color": "#053264"});
})

/*jQuery(window).scroll(function(){
    $('#vfbp-running-total').stop().animate({"marginTop": ($(window).scrollTop() + 1) + "px"}, "slow" );
});*/

jQuery(window).scroll(function () {

    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $('body').find('#navbar-sticky-wrapper').addClass('is-sticky');
    } else {
        $('body').find('#navbar-sticky-wrapper').removeClass('is-sticky');
    }


});

jQuery(document).ready(function () {
    setTraingle();
    scrollMenu();
    setMenuDisabled();

    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

});


function setTraingle() {
    $ = jQuery;
    var a = $('#education_section img').height();
    var b = $('#education_section img').width() / 2;
    $('#triangleBlack').css({borderTopWidth: a});
    $('#triangleBlack').css({borderLeftWidth: b});
    $('#triangleBlack1').css({borderTopWidth: a});
    $('#triangleBlack1').css({borderLeftWidth: b});

}

var responsive_menu_width = 768;

function setMenuDisabled() {
    if (jQuery('body').width() > responsive_menu_width) {
        jQuery('a.maybe-disabled').addClass('disabled');
    }
    else {
        jQuery('a.maybe-disabled').removeClass('disabled');
    }
}

function scrollMenu(){
    $('ul.nav.navbar-nav li a').on('click', function (e) {
        var targetSec = $(this).text();
        $('html, body').animate({
            scrollTop: $('#' + targetSec).offset().top
        }, 2000);
    });
}




jQuery('a.go_to_about_section').click(function () {
    jQuery('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top - 65
    }, 1000);
    return false;
});



/*
$= jQuery;
// On window load. This waits until images have loaded which is essential
$(window).load(function(){

    // Fade in images so there isn't a color "pop" document load and then on window load
    $(".adverts_section img").fadeIn(500);

    // clone image
    $('.adverts_section img').each(function(){
        var el = $(this);
        el.css({"position":"absolute"}).wrap("<div class='img_wrapper' style='display: inline-block'>").clone().addClass('img_grayscale').css({"position":"absolute","z-index":"998","opacity":"0"}).insertBefore(el).queue(function(){
            var el = $(this);
            el.parent().css({"width":this.width,"height":this.height});
            el.dequeue();
        });
        this.src = grayscale(this.src);
    });

    // Fade image
    $('.adverts_section img').mouseover(function(){
        $(this).parent().find('img:first').stop().animate({opacity:1}, 1000);
    })
    $('.adverts_section img.vc_single_image-img.attachment-full').mouseout(function(){
        $(this).stop().animate({opacity:0}, 1000);
    });

});

// Grayscale w canvas method
function grayscale(src){
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    var imgObj = new Image();
    imgObj.src = src;
    canvas.width = imgObj.width;
    canvas.height = imgObj.height;
    ctx.drawImage(imgObj, 0, 0);
    var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
    for(var y = 0; y < imgPixels.height; y++){
        for(var x = 0; x < imgPixels.width; x++){
            var i = (y * 4) * imgPixels.width + x * 4;
            var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
            imgPixels.data[i] = avg;
            imgPixels.data[i + 1] = avg;
            imgPixels.data[i + 2] = avg;
        }
    }
    ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
    return canvas.toDataURL();
}
*/


