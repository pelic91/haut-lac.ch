<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Haultac
 */

get_header();
?>


    <div class="under_header_color">
        <div class="box_1"></div>
        <div class="box_2"></div>
        <div class="box_3"></div>
        <div class="box_4"></div>
    </div>

    <div class="container">
        <section id="primary" class="content-area">
            <main id="main" class="site-main" role="main">




                <div class="inner_page blog_page">
                    <div class="container ">
                        <div class="col-md-12">
                            <header class="page-header">
                                <h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'haultac' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                            </header><!-- .page-header -->
                        </div>
                    </div>


                    <div class="container">
                        <div class="col-md-4">

                            <?php if (is_active_sidebar('blog_sidebar_menu')) : ?>
                                <div id="blog_sidebar_menu" class="blog_sidebar_menu widget-area" role="complementary">
                                    <?php dynamic_sidebar('blog_sidebar_menu'); ?>
                                </div><!-- #primary-sidebar -->
                            <?php endif; ?>


                        </div>
                        <div class="col-md-8">

                            <section class="blog_content">


                                <?php while (have_posts()) : the_post(); ?>
                                    <div class="single_post">
                                        <div class="title"><a href="<?php the_permalink(); ?>"
                                                              title="<?php the_title_attribute(); ?>"><?php the_title(); ?>

                                            </a>
                                            <div class="social">
                                                <?php if(function_exists('kc_add_social_share')) kc_add_social_share(); ?>
                                            </div>
                                            <div class="line_box"><span class="box"></span></div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="time">
                                                <span class="day"> <?php the_time('j'); ?></span>
                                                <span class="month"> <?php the_time('F'); ?></span>
                                                <span class="year"> <?php the_time('Y'); ?></span>

                                            </div>
                                        </div>

                                        <div class="col-md-10">
                                            <div class="content">
                                                <?php
                                                if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                                    the_post_thumbnail();
                                                }
                                                ?>
                                                <?php the_excerpt('') ?></div>
                                            <div class="read_more"><a href="<?php the_permalink(); ?>">Read More <i class="fa fa-angle-double-right"></i></a></div>
                                            <div class="author"><?php wpml_e__if_language( 'Written by ', 'en' ); ?>
                                                <?php wpml_e__if_language( 'Rédigé par', 'fr'); ?>
                                                <?php the_author() ?>  <?php wpml_e__if_language( 'Posted in', 'en' ); ?>
                                                <?php wpml_e__if_language( 'Posté dans', 'fr'); ?>
                                                <?php the_category(', ', '') ?>
                                                </br>
                                                <?php the_tags(); ?>
                                            </div>
                                        </div>


                                    </div>
                                <?php endwhile; // end of the loop. ?>

                            </section>





                        </div>
                    </div>
                </div>

            </main><!-- #main -->
        </section><!-- #primary -->
    </div>
<?php
get_footer();
