<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Haultac
 */


$base = get_stylesheet_directory_uri();
get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <section class="error-404 not-found">

                    <div class="inner_page blog_page">
                        <div class="container ">
                            <div class="col-md-12">
                                <header class="page-header">
                                    <h1 class="page-title text-center"><?php esc_html_e('Oops! That page can&rsquo;t be found.', 'haultac'); ?></h1>
                                </header><!-- .page-header -->
                            </div>
                        </div>
                        <br/>

                        <div class="page-content">
                            <img src="<?= $base; ?>/images/404_custom_page.png" class="img-responsive"
                                 style="margin: 0 auto"/>
                            <br/>
                            <br/>
                            <br/>
                        </div><!-- .page-content -->
                </section><!-- .error-404 -->
            </div>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
