<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/12/2016
 * Time: 12:58 PM
 */


function codex_video_init() {
    $labels = array(
        'name'               => _x( 'Videos', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Video', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Videos', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Video', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'video', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Video', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Video', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Video', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Video', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Videos', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Videos', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Videos:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No videos found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No videos found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'video' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'video', $args );
}
codex_video_init();