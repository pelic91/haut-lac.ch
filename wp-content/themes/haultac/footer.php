<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Haultac
 */
$base = get_stylesheet_directory_uri();
?>
</div><!-- #page -->

<?php if(is_front_page()):?>
    <div id="taggingimage"
         class="vc_row wpb_row vc_inner vc_row-fluid single_image_footer vc_custom_1457534084968 vc_row-has-fill"
         xmlns="http://www.w3.org/1999/html">
        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
            <div class="vc_column-inner vc_custom_1457355262743">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">

                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                <img width="1920" height="566" src="<?= $base; ?>/images//background_footer.png" class="vc_single_image-img attachment-full tagImageSingle" alt="background_footer" srcset="<?= $base; ?>/images/background_footer.png 150w, <?= $base; ?>/images/background_footer.png 300w, <?= $base; ?>/images/background_footer.png 768w, <?= $base; ?>/images//background_footer-1024x302.png 1024w, <?= $base; ?>/images/background_footer.png 1350w, <?= $base; ?>/images/background_footer.png 1920w" sizes="(max-width: 1920px) 100vw, 1920px">
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
<div id="green_footer_section" class="footer_side " xmlns="http://www.w3.org/1999/html">
    <div class=" container text_title">
        <div class="col-md-3">
            <img src="<?= $base; ?>/images/logo_footter.png" class="" alt="logo_footter">
        </div>
        <div class="col-md-9">
            <?php
            wpml_e__if_language( '<p>EXPERIENCE THE HAUT-LAC <strong>LEARNING </strong>ENVIRONMENT</p>', 'en' );
            wpml_e__if_language( ' <p>DÉCOUVREZ L\'ENVIRONNEMENT <strong>ÉDUCATIF</strong> DU HAUT-LAC</p>', 'fr' );
            ?>
        </div>
    </div>
</div>

<div id="blue_footer_section" class="blue_footer">
    <div class="angle_image">
        <img width="1920" height="45" src="<?= $base; ?>/images/test.png" class="vc_single_image-img attachment-full"
             alt="test"
             srcset="<?= $base; ?>/images/test.png, /<?= $base; ?>/images/test.png, <?= $base; ?>/images/test.png, <?= $base; ?>/images/test.png"
             sizes="(max-width: 1920px) 100vw, 1920px">
    </div>

    <div class="container">
        <div class="footer_box_1 col-md-3">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="fa fa-plus"></i>
                                        <?php wpml_e__if_language( 'Primary Campus', 'en' ); ?>
                                        <?php wpml_e__if_language( 'Campus primaire', 'fr'); ?>

                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p>
                                        CH: 1806 St-Légier-La Chiésaz</br>
                                        Email: info@haut-lac.ch</br>
                                        <a href="tel:+41215555000"> Phone: +41 21 555 5000</a><br/>
                                        <a href="tel:+41215555001">Fax: +41 21 555 5001</a><br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="fa fa-plus"></i>
                                        <?php wpml_e__if_language( 'Secondary Campus', 'en' ); ?>
                                        <?php wpml_e__if_language( 'Campus secondaire', 'fr'); ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p>
                                        CH: 1806 St-Légier-La Chiésaz</br>
                                        Email: info@haut-lac.ch</br>
                                        <a href="tel:+41215555000"> Phone: +41 21 555 5000</a><br/>
                                        <a href="tel:+41215555001">Fax: +41 21 555 5001</a><br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="fa fa-plus"></i>
                                        <?php wpml_e__if_language( 'Crèche', 'en' ); ?>
                                        <?php wpml_e__if_language( 'Crèche', 'fr'); ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        Rue du Nord, 3-5<br/>
                                        CH - 1800 Vevey<br/>
                                        Email: info@haut-lac.ch<br/>
                                        <a href="tel:+41215555000"> Phone: +41 21 555 5000</a><br/>
                                        <a href="tel:+41215555001">Fax: +41 21 555 5001</a><br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="footer_box_2 col-md-3">
            <h5>
            <?php wpml_e__if_language( 'Quick Links', 'en' ); ?><?php wpml_e__if_language( 'LIENS UTILES', 'fr' ); ?>
            </h5>
            <p>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>/about-us">  <?php wpml_e__if_language( 'About us', 'en' ); ?><?php wpml_e__if_language( 'Présentation', 'fr'); ?></a><br>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>/about-us/jobshaut-lac/"><?php wpml_e__if_language( 'Careers', 'en' ); ?><?php wpml_e__if_language( 'Carrières', 'fr'); ?></a><br>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>/admissions/terms-conditions/"><?php wpml_e__if_language( 'Terms of use', 'en' ); ?><?php wpml_e__if_language( 'Conditions d\'utilisation', 'fr'); ?></a><br>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>/privacy-policy/"><?php wpml_e__if_language( 'privacy policy', 'en' ); ?><?php wpml_e__if_language( 'Politique de Confidentialité', 'fr'); ?></a><br>
            </p>


        </div>
        <div class="footer_box_3 col-md-3">
            <h5><?php wpml_e__if_language( 'Follow us With', 'en' ); ?><?php wpml_e__if_language( ' Suivez-nous Avec', 'fr'); ?></h5>
            <p>
                <a href="https://www.facebook.com/hautlac/" target="_blank"><i class="fa fa-facebook"></i> Facebook</a><br>
                <a href="https://twitter.com/HautLac" target="_blank"><i class="fa fa-twitter"></i>Twitter</a><br>
                <a href="https://www.linkedin.com/company/haut-lac-international-bilingual-school" target="_blank"><i class="fa fa-linkedin"></i>Linkedin</a><br>
            </p>

        </div>
        <div class="footer_box_4 col-md-3">
            <h5>
                <?php wpml_e__if_language( 'Quick Contact', 'en' ); ?><?php wpml_e__if_language( 'CONTACT RAPIDE', 'fr'); ?>
            </h5>
            <?php wpml_e__if_language( '[vfb id=44]', 'en' ); ?><?php wpml_e__if_language( '[vfb id=45]', 'fr'); ?>
        </div>
    </div>

    <div class="wpb_single_image wpb_content_element vc_align_center">
        <figure class="wpb_wrapper vc_figure">
            <img width="529" height="234" src="<?= $base; ?>/images/mountitns.png"
                 class="vc_single_image-img attachment-full" alt="mountitns"
                 srcset="<?= $base; ?>/images/mountitns.png, <?= $base; ?>/images/mountitns.png"
                 sizes="(max-width: 529px) 100vw, 529px">

        </figure>
    </div>
</div>

<div id="copyright_section" class=" copyright">
    <div class="container">
        <h2>&copy; <?= date("Y"); ?> haut-lac.ch, Tous droits réservés</h2>
    </div>
</div>


<!-- W3TC-include-js-head -->
<?php wp_footer(); ?>


</body>
</html>




