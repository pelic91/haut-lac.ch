<?php
/**
 * Template Name: Blog
 *
 */
$base = get_stylesheet_directory_uri();

get_header();

if (get_option('page_for_posts')) {
    $posts_page = get_post(get_option('page_for_posts'));
    $posts_page_id = $posts_page->ID;
    $posts_name = $posts_page->post_name;
} else {
    $posts_page = null;
    $posts_page_id = 0;
    $posts_name = '';
}
//_p( $posts_page, 1);
?>
<?php if ($posts_page_id && has_post_thumbnail($posts_page_id)) : ?>
    <div class="banner">
        <?php
        $thumb = get_the_post_thumbnail($posts_page_id);
        echo $thumb;
        //the_post_thumbnail($posts_page_id);
        ?>
    </div>
<?php endif; ?>

<div class="under_header_color">
    <div class="box_1"></div>
    <div class="box_2"></div>
    <div class="box_3"></div>
    <div class="box_4"></div>
</div>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="inner_page blog_page">
            <div class="container ">
                <header>
                    <?php
                    echo '<h1 class="page-title">' . $posts_name . '</h1>';
                    the_archive_description('<div class="taxonomy-description">', '</div>');
                    ?>
                </header><!-- .page-header -->
                <?php
                if ($posts_page) {
                    echo apply_filters('the_content', $posts_page->post_content);
                }
                ?>
                <h3 class="blog_title">

                    <?= get_post_meta($posts_page_id, 'blog_description', true) ?>
                </h3>
            </div>

            <?php ?>
            <div class="container">
                <div class="col-md-4">

                    <?php if (is_active_sidebar('blog_sidebar_menu')) : ?>
                        <div id="blog_sidebar_menu" class="blog_sidebar_menu widget-area" role="complementary">
                            <?php dynamic_sidebar('blog_sidebar_menu'); ?>
                        </div><!-- #primary-sidebar -->
                    <?php endif; ?>


                </div>
                <div class="col-md-8">

                    <section class="blog_content">
                        <?php
                        if (have_posts()) : ?>
                            <?php
                            /* Start the Loop */
                            while (have_posts()) : the_post(); ?>
                                <div class="single_post">
                                    <div class="title"><a href="<?php the_permalink(); ?>"
                                                          title="<?php the_title_attribute(); ?>"><?php the_title(); ?>

                                        </a>

                                        <div class="social">
                                            <?php if (function_exists('kc_add_social_share')) kc_add_social_share(); ?>
                                        </div>
                                        <div class="line_box"><span class="box"></span></div>
                                    </div>

                                    <div class="col-md-2 col-xs-4">
                                        <div class="time">
                                            <span class="day"> <?php the_time('j'); ?></span>
                                            <span class="month"> <?php the_time('F'); ?></span>
                                            <span class="year"> <?php the_time('Y'); ?></span>

                                        </div>
                                    </div>

                                    <div class="col-md-10">
                                        <div class="content">
                                            <?php
                                            if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                                the_post_thumbnail();
                                            }
                                            ?>
                                            <?php the_excerpt('') ?></div>
                                        <div class="read_more"><a href="<?php the_permalink(); ?>">Read More <i
                                                    class="fa fa-angle-double-right"></i></a></div>
                                        <div class="author"> <?php wpml_e__if_language('Written by ', 'en'); ?>
                                            <?php wpml_e__if_language('Rédigé par', 'fr'); ?>
                                            <?php the_author() ?>  <?php wpml_e__if_language('Posted in', 'en'); ?>
                                            <?php wpml_e__if_language('Posté dans', 'fr'); ?>
                                            <?php the_category(', ', '') ?>
                                            </br>
                                            <?php the_tags(); ?>
                                        </div>
                                    </div>


                                </div>
                            <?php endwhile; // end of one post ?>
                            <div class="pagination">
                                <?php wp_pagenavi(); ?>
                            </div>
                        <?php endif; //end of loop ?>

                    </section>


                </div>
            </div>
        </div>

    </main>


    <!-- #main -->

</div><!-- #primary -->


<?php get_footer(); ?>
