<?php
/**
 * Template Name: Blog
 *
 */
$base = get_stylesheet_directory_uri();

get_header();
?>
<?php if (has_post_thumbnail()): ?>
    <div class="banner">
        <?php the_post_thumbnail(); ?>
    </div>
<?php endif; ?>

<div class="under_header_color">
    <div class="box_1"></div>
    <div class="box_2"></div>
    <div class="box_3"></div>
    <div class="box_4"></div>
</div>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="inner_page blog_page">
            <div class="container ">
                <div class="col-md-12">
                    <header class="entry-header">
                        <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
                    </header><!-- .entry-header -->
                </div>

                <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part('template-parts/content', 'afterLogin'); ?>
                <?php endwhile; // end of the loop. ?>
            </div>


            <div class="container">
                <div class="col-md-4">

                    <?php if (is_active_sidebar('blog_sidebar_menu')) : ?>
                        <div id="blog_sidebar_menu" class="blog_sidebar_menu widget-area" role="complementary">
                            <?php dynamic_sidebar('blog_sidebar_menu'); ?>
                        </div><!-- #primary-sidebar -->
                    <?php endif; ?>
                </div>

                <div class="col-md-8">

                    <section class="blog_content">

                        <?php

                        $my_query = new WP_Query('paged=' . $paged); ?>
                        <?php if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post(); ?>
                            <div class="single_post">
                                <div class="title"><a href="<?php the_permalink(); ?>"
                                                      title="<?php the_title_attribute(); ?>"><?php the_title(); ?>

                                    </a>
                                    <div class="social">
                                        <?php if(function_exists('kc_add_social_share')) kc_add_social_share(); ?>
                                    </div>
                                    <div class="line_box"><span class="box"></span></div>
                                </div>

                                <div class="col-md-2 col-xs-4">
                                    <div class="time">
                                        <span class="day"> <?php the_time('j'); ?></span>
                                        <span class="month"> <?php the_time('F'); ?></span>
                                        <span class="year"> <?php the_time('Y'); ?></span>

                                    </div>
                                </div>

                                <div class="col-md-10">
                                    <div class="content">
                                        <?php
                                        if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                            the_post_thumbnail();
                                        }
                                        ?>
                                        <?php the_excerpt('') ?></div>
                                    <div class="read_more"><a href="<?php the_permalink(); ?>">Read More <i class="fa fa-angle-double-right"></i></a></div>
                                    <div class="author">
                                        <?php wpml_e__if_language( 'Written by ', 'en' ); ?>
                                        <?php wpml_e__if_language( 'Rédigé par', 'fr'); ?>
                                        <?php the_author() ?>  <?php wpml_e__if_language( 'Posted in', 'en' ); ?>
                                        <?php wpml_e__if_language( 'Posté dans', 'fr'); ?>
                                        <?php the_category(', ', '') ?>
                                    </br>
                                        <?php the_tags(); ?>
                                    </div>
                                </div>


                            </div>
                        <?php endwhile; // end of one post ?>
                            <div class="pagination">
                                <?php wp_pagenavi(['query'=>$my_query]); ?>
                            </div>
                        <?php endif; //end of loop ?>

                    </section>





                </div>
            </div>
        </div>

    </main>


    <!-- #main -->

</div><!-- #primary -->


<?php get_footer(); ?>
