<?php
/*
 * Plugin Name: WPML Text Widget
 * Version: 2.0
 * Plugin URI: http://www.about.me/cyberwani
 * Description: Multilingual Text Widget For WPML 
 * Author: Dinesh Kesarwani
 * Author URI: http://www.about.me/cyberwani
 *	License:
    Copyright 2015  Thomas Egtvedt  (email : tmeweb@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2, 
    as published by the Free Software Foundation. 
    
    You may NOT assume that you can use any other version of the GPL.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    The license for this software can likely be found here: 
    http://www.gnu.org/licenses/gpl-2.0.html
 */
// class qTextxWidget extends WP_Widget
class WMPLTextWidget extends WP_Widget
{
	public $wtw_enabled_langs_num; # enabled languages number
	public $wtw_enabled_langs; # enabled languages @array
	
	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		/*
		parent::__construct(
			'wmpltextwidget', // Base ID
			__( 'WPML Text Widget', 'text_domain' ), // Name
			array( // Args
				'classname'   => 'widget_text WMPLTextWidget',
				'description' => __( "Multilingual text widget working with WPML")
			),
			array( // control_ops
				'width' => 'auto',
				'height' => 'auto'
			)
		);
		*/
		$widget_ops  = array('classname' => 'widget_text WMPLTextWidget', 'description' => __('Multilingual text widget working with WPML.'));// Args
		$control_ops = array('width' => 400, 'height' => 350); // control_ops
		parent::__construct('wmpltextwidget', __('WPML Text Widget'), $widget_ops, $control_ops);
		
		$this->wtw_enabled_langs = icl_get_languages('skip_missing=N&orderby=id&order=desc&link_empty_to=str'); // get enabled languages
		$this->wtw_enabled_langs_num = count($this->wtw_enabled_langs); // get enabled languages number
	}
	
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		
		$title = 'title_'.ICL_LANGUAGE_CODE;
		$text  = 'text_'.ICL_LANGUAGE_CODE;
		if ( ! empty( $instance[$title] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance[$title] ). $args['after_title'];
		}
		?>
		<div class="textwidget"><?php echo $instance[$text];?></div>
		<?php
		echo $args['after_widget'];
	}
	
	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		foreach($this->wtw_enabled_langs as $wtw_lang){
			$wtw_lang_code = $wtw_lang['code'];
			$wtw_lang_name = $wtw_lang['native_name'];
			$title = 'title_'.$wtw_lang_code;
			$text  = 'text_'.$wtw_lang_code;
			
			$$title = ! empty( $instance[$title] ) ? $instance[$title] : '';
			$$text  = ! empty( $instance[$text] ) ? $instance[$text]   : '';
		}
		foreach($this->wtw_enabled_langs as $wtw_lang){
			$wtw_lang_code = $wtw_lang['code'];
			$wtw_lang_name = $wtw_lang['native_name'];
			$title = 'title_'.$wtw_lang_code;
			$text  = 'text_'.$wtw_lang_code;
			?>
			<p>
				<label for="<?php echo $this->get_field_id($title);?>"><?php _e('Title');?> [<?php echo $wtw_lang_name; ?>]</label>
				<input class="widefat" id="<?php echo $this->get_field_id( $title ); ?>" name="<?php echo $this->get_field_name( $title ); ?>" type="text" value="<?php echo esc_attr( $$title ); ?>">
			</p>
			<p>
				<label for="<?php echo $this->get_field_id($text);?>">
					<?php _e('Content');?> [<?php echo $wtw_lang_name; ?>]
				</label>
				<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id($text); ?>" name="<?php echo $this->get_field_name($text);?>"><?php echo $$text; ?></textarea>
			</p>
			<?php
		}
		?>
		<?php 
	}
	
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		foreach($this->wtw_enabled_langs as $wtw_lang){
			$wtw_lang_code = $wtw_lang['code'];
			$wtw_lang_name = $wtw_lang['native_name'];
			$title = 'title_'.$wtw_lang_code;
			$text  = 'text_'.$wtw_lang_code;
			
			$instance[$title] = ( ! empty( $new_instance[$title] ) ) ? strip_tags( $new_instance[$title] ) : '';
			$instance[$text]  = ( ! empty( $new_instance[$text] ) )  ? $new_instance[$text] : '';
		}

		return $instance;
	}
}
function WMPLTextWidgetInit() {
	register_widget('WMPLTextWidget');
}
add_action('widgets_init', 'WMPLTextWidgetInit');
?>