<?php class SP_recentpostpro_Widget extends WP_Widget {

    function SP_recentpostpro_Widget() {

        $widget_ops = array('classname' => 'SP_recentpostpro_Widget', 'description' => __('Displayed Latest Post Items with slider', 'post') );
        $control_ops = array( 'width' => 350, 'height' => 450, 'id_base' => 'sp_recentpostpro_widget' );
        $this->WP_Widget( 'sp_recentpostpro_widget', __('Latest Post Slider Widget', 'post'), $widget_ops, $control_ops );
    }

    function form($instance) {
        $defaults = array(
        'limit'             => 5,
        'title'             => '',
        "date"              => false, 
        'show_category'     => false,
        'category'          => 0,
		'arrows'            => "true",
        'autoplay'          => "true",      
        'autoplayInterval'  => 3000,                
        'speed'             => 300,
        );

        $instance = wp_parse_args( (array) $instance, $defaults );
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $num_items = isset($instance['num_items']) ? absint($instance['num_items']) : 5;
    ?>
      <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
      <p><label for="<?php echo $this->get_field_id('num_items'); ?>">Number of Items: <input class="widefat" id="<?php echo $this->get_field_id('num_items'); ?>" name="<?php echo $this->get_field_name('num_items'); ?>" type="text" value="<?php echo attribute_escape($num_items); ?>" /></label></p>
      <p>
            <input id="<?php echo $this->get_field_id( 'date' ); ?>" name="<?php echo $this->get_field_name( 'date' ); ?>" type="checkbox"<?php checked( $instance['date'], 1 ); ?> />
            <label for="<?php echo $this->get_field_id( 'date' ); ?>"><?php _e( 'Display Date', 'post' ); ?></label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id( 'show_category' ); ?>" name="<?php echo $this->get_field_name( 'show_category' ); ?>" type="checkbox"<?php checked( $instance['show_category'], 1 ); ?> />
            <label for="<?php echo $this->get_field_id( 'show_category' ); ?>"><?php _e( 'Display Category', 'post' ); ?></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category:', 'post' ); ?></label>
            <?php
                $dropdown_args = array( 'taxonomy' => 'category', 'class' => 'widefat', 'show_option_all' => __( 'All', 'post' ), 'id' => $this->get_field_id( 'category' ), 'name' => $this->get_field_name( 'category' ), 'selected' => $instance['category'] );
                wp_dropdown_categories( $dropdown_args );
            ?>
        </p>
<!-- Widget Order: Select Arrows -->
        <p>
            <label for="<?php echo $this->get_field_id( 'arrows' ); ?>"><?php _e( 'Arrows:', 'post' ); ?></label>
            <select name="<?php echo $this->get_field_name( 'arrows' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'arrows' ); ?>">
                <?php foreach ( $this->get_other_options() as $k => $v ) { ?>
                <option value="<?php echo $k; ?>"<?php selected( $instance['arrows'], $k ); ?>><?php echo $v; ?></option>
            <?php } ?>
            </select>
        </p>

         <!-- Widget Order: Select Auto play -->
        <p>
            <label for="<?php echo $this->get_field_id( 'autoplay' ); ?>"><?php _e( 'Auto Play:', 'post' ); ?></label>
            <select name="<?php echo $this->get_field_name( 'autoplay' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'autoplay' ); ?>">
                <?php foreach ( $this->get_other_options() as $k => $v ) { ?>
                <option value="<?php echo $k; ?>"<?php selected( $instance['autoplay'], $k ); ?>><?php echo $v; ?></option>
            <?php } ?>
            </select>
        </p>
        <!-- Widget ID:  AutoplayInterval -->
        <p>
            <label for="<?php echo $this->get_field_id( 'autoplayInterval' ); ?>"><?php _e( 'Autoplay Interval:', 'post' ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'autoplayInterval' ); ?>"  value="<?php echo $instance['autoplayInterval']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'autoplayInterval' ); ?>" />
        </p>
        <!-- Widget ID:  Speed -->
        <p>
            <label for="<?php echo $this->get_field_id( 'speed' ); ?>"><?php _e( 'Speed:', 'post' ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'speed' ); ?>"  value="<?php echo $instance['speed']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'speed' ); ?>" />
        </p>		
    <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = $new_instance['title'];
        $instance['num_items'] = $new_instance['num_items'];
        $instance['date'] = (bool) esc_attr( $new_instance['date'] );
        $instance['show_category'] = (bool) esc_attr( $new_instance['show_category'] );
        $instance['category']      = intval( $new_instance['category'] );
		$instance['arrows']             = esc_attr( $new_instance['arrows'] );
        $instance['autoplay']           = esc_attr( $new_instance['autoplay'] );
        $instance['autoplayInterval']   = intval( $new_instance['autoplayInterval'] );
        $instance['speed']              = intval( $new_instance['speed'] );
        return $instance;
    }
	function get_other_options () {
         $args = array(
                    'true' => __( 'True', 'post' ),
                    'false' => __( 'False', 'post' )
                    );    
         return $args;
        }
    function widget($post_args, $instance) {
        extract($post_args, EXTR_SKIP);

        $current_post_name = get_query_var('name');

        $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
        $num_items = empty($instance['num_items']) ? '5' : apply_filters('widget_title', $instance['num_items']);
        if ( isset( $instance['date'] ) && ( 1 == $instance['date'] ) ) { $date = "true"; } else { $date = "false"; }
        if ( isset( $instance['show_category'] ) && ( 1 == $instance['show_category'] ) ) { $show_category = "true"; } else { $show_category = "false"; }
        if ( isset( $instance['category'] ) && is_numeric( $instance['category'] ) ) $category = intval( $instance['category'] );
		if ( isset( $instance['arrows'] ) && in_array( $instance['arrows'], array_keys( $this->get_other_options() ) ) ) { $args['arrows'] = $instance['arrows']; }
        if ( isset( $instance['autoplay'] ) && in_array( $instance['autoplay'], array_keys( $this->get_other_options() ) ) ) { $args['autoplay'] = $instance['autoplay']; }
        if ( isset( $instance['autoplayInterval'] ) && ( 0 < count( $instance['autoplayInterval'] ) ) ) { $args['autoplayInterval'] = intval( $instance['autoplayInterval'] ); }
        if ( isset( $instance['speed'] ) && ( 0 < count( $instance['speed'] ) ) ) { $args['speed'] = intval( $instance['speed'] ); }
        $postcount = 0;

        echo $before_widget;

?>
             <h2 class="widget-title"><?php echo $title ?></h2>
          
            <div class="recent-post-slider design-w1 <?php echo $no_p?>">
              
            <?php // setup the query
            $post_args = array( 'suppress_filters' => true,
                           'posts_per_page' => $num_items,
                           'post_type' => 'post',
                           'order' => 'DESC',
						   'cat'       => $category
                         );


            $cust_loop = new WP_Query($post_args);
               $post_count = $cust_loop->post_count;
          $count = 0;
           
            if ($cust_loop->have_posts()) : while ($cust_loop->have_posts()) : $cust_loop->the_post(); $postcount++;
                    $count++;
              
                    ?>
					
					 <div class="post-slides">  
						<div class="post-grid-content">
						<div class="post-overlay">
							<div class="post-image-bg">
							<?php the_post_thumbnail('large'); ?>
								<?php if($show_category == 'true') { ?>
								<div class="recentpost-categories">		
									<?php echo get_the_category_list( $separator, $parents, $post_id ); ?>
						</div>
								<?php } ?>
							</div>
							<div class="post-short-content">
							 <h2 class="post-title">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
							</h2>
						<?php if($date == "true") { ?>
							<div class="post-date">		
								<?php echo get_the_date(); ?>
								</div>
						<?php }?>
							</div>	
							</div>	
								
						</div>
					</div>
				
                 
            <?php endwhile;
            endif;
             wp_reset_query(); ?>

  
            </div>
			 <script type="text/javascript">
    jQuery(document).ready(function(){
	     jQuery('.recent-post-slider.design-w1').slick({
			dots: false,
			infinite: true,
			speed: <?php echo $instance['speed']?>,
			arrows:<?php echo $instance['arrows']?>,
			autoplay: <?php echo $instance['autoplay']?>,
			autoplaySpeed:<?php echo $instance['autoplayInterval']?>,
			slidesToShow: 1,
			slidesToScroll: 1,
	  });
	  

    });


  </script>
<?php
        echo $after_widget;
    }
}
/* Register the widget */
function sp_recentpost_widgetpro_load_widgets() {
    register_widget( 'SP_recentpostpro_Widget' );
}
/* Load the widget */
add_action( 'widgets_init', 'sp_recentpost_widgetpro_load_widgets' );


class SP_Postlistpro_Widget extends WP_Widget {

    function SP_Postlistpro_Widget() {

        $widget_ops = array('classname' => 'SP_Postlistpro_Widget', 'description' => __('Displayed Latest Post Items in list view', 'post') );
        $control_ops = array( 'width' => 350, 'height' => 450, 'id_base' => 'sp_postlistpro_widget' );
        $this->WP_Widget( 'sp_postlistpro_widget', __('Latest Post List View', 'post'), $widget_ops, $control_ops );
    }

    function form($instance) {
        $defaults = array(
        'limit'             => 5,
        'title'             => '',
        "date"              => false, 
        'show_category'     => false,
        'category'          => 0,
        );

        $instance = wp_parse_args( (array) $instance, $defaults );
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $num_items = isset($instance['num_items']) ? absint($instance['num_items']) : 5;
    ?>
      <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
      <p><label for="<?php echo $this->get_field_id('num_items'); ?>">Number of Items: <input class="widefat" id="<?php echo $this->get_field_id('num_items'); ?>" name="<?php echo $this->get_field_name('num_items'); ?>" type="text" value="<?php echo attribute_escape($num_items); ?>" /></label></p>
      <p>
            <input id="<?php echo $this->get_field_id( 'date' ); ?>" name="<?php echo $this->get_field_name( 'date' ); ?>" type="checkbox"<?php checked( $instance['date'], 1 ); ?> />
            <label for="<?php echo $this->get_field_id( 'date' ); ?>"><?php _e( 'Display Date', 'post' ); ?></label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id( 'show_category' ); ?>" name="<?php echo $this->get_field_name( 'show_category' ); ?>" type="checkbox"<?php checked( $instance['show_category'], 1 ); ?> />
            <label for="<?php echo $this->get_field_id( 'show_category' ); ?>"><?php _e( 'Display Category', 'post' ); ?></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category:', 'post' ); ?></label>
            <?php
                $dropdown_args = array( 'taxonomy' => 'category', 'class' => 'widefat', 'show_option_all' => __( 'All', 'post' ), 'id' => $this->get_field_id( 'category' ), 'name' => $this->get_field_name( 'category' ), 'selected' => $instance['category'] );
                wp_dropdown_categories( $dropdown_args );
            ?>
        </p>	
    <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = $new_instance['title'];
        $instance['num_items'] = $new_instance['num_items'];
        $instance['date'] = (bool) esc_attr( $new_instance['date'] );
        $instance['show_category'] = (bool) esc_attr( $new_instance['show_category'] );
        $instance['category']      = intval( $new_instance['category'] );   
        return $instance;
    }
    function widget($post_args, $instance) {
        extract($post_args, EXTR_SKIP);

        $current_post_name = get_query_var('name');

        $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
        $num_items = empty($instance['num_items']) ? '5' : apply_filters('widget_title', $instance['num_items']);
        if ( isset( $instance['date'] ) && ( 1 == $instance['date'] ) ) { $date = "true"; } else { $date = "false"; }
        if ( isset( $instance['show_category'] ) && ( 1 == $instance['show_category'] ) ) { $show_category = "true"; } else { $show_category = "false"; }
        if ( isset( $instance['category'] ) && is_numeric( $instance['category'] ) ) $category = intval( $instance['category'] );
        $postcount = 0;

        echo $before_widget;

?>
             <h2 class="widget-title"><?php echo $title ?></h2>
            <!--visual-columns-->
            <?php if($date == "false" && $show_category == "false"){ 
                $no_p = "no_p";
                }?>
            <div class="sp_post_static design-w2 <?php echo $no_p?>">
              
            <?php // setup the query
            $post_args = array( 'suppress_filters' => true,
                           'posts_per_page' => $num_items,
                           'post_type' => 'post',
                           'order' => 'DESC',
						    'cat'       => $category
                         );

            
            $cust_loop = new WP_Query($post_args);
               $post_count = $cust_loop->post_count;
          $count = 0;
           
            if ($cust_loop->have_posts()) : while ($cust_loop->have_posts()) : $cust_loop->the_post(); $postcount++;
                    $count++;
             
                    ?>
					
					
					 <div class="post-grid">
						  <div class="post-image-bg">
								 <a  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_post_thumbnail('medium'); ?></a>
								<?php if($show_category == 'true') { ?>
								<div class="recentpost-categories">		
								<?php echo get_the_category_list( $separator, $parents, $post_id ); ?>	
						</div>
								<?php } ?>
								</div>
								<div class="post-grid-content">
								<div class="post-content">
									 <h3 class="post-title">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
									</h3>
									<?php if($date == "true") { ?>
							<div class="post-date">		
								<?php echo get_the_date(); ?>
								</div>
						<?php }?>
										</div>
								</div>
								
							</div>
				
                 
            <?php endwhile;
            endif;
             wp_reset_query(); ?>

  
            </div>
			
<?php
        echo $after_widget;
    }
}
/* Register the widget */
function sp_post_widgetlistpro_load_widgets() {
    register_widget( 'SP_Postlistpro_Widget' );
}
/* Load the widget */
add_action( 'widgets_init', 'sp_post_widgetlistpro_load_widgets' );

class PRO_SP_Post_thmb_Widget extends WP_Widget {

    function PRO_SP_Post_thmb_Widget() {

        $widget_ops = array('classname' => 'PRO_SP_Post_thmb_Widget', 'description' => __('Displayed Latest Post Items in a sidebar with thumbnails', 'post') );
        $control_ops = array( 'width' => 350, 'height' => 450, 'id_base' => 'pro_sp_post_thumb_widget' );
        $this->WP_Widget( 'pro_sp_post_thumb_widget', __('Latest Post with Thumb', 'post'), $widget_ops, $control_ops );
    }

    function form($instance) {	
        $defaults = array(
        'limit'             => 5,
        'title'             => '',
        "date"              => false, 
        'show_category'     => false,
        'category'          => 0,
        );

        $instance = wp_parse_args( (array) $instance, $defaults );
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $num_items = isset($instance['num_items']) ? absint($instance['num_items']) : 5;
    ?>
      <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
      <p><label for="<?php echo $this->get_field_id('num_items'); ?>">Number of Items: <input class="widefat" id="<?php echo $this->get_field_id('num_items'); ?>" name="<?php echo $this->get_field_name('num_items'); ?>" type="text" value="<?php echo attribute_escape($num_items); ?>" /></label></p>
    	<p>
            <input id="<?php echo $this->get_field_id( 'date' ); ?>" name="<?php echo $this->get_field_name( 'date' ); ?>" type="checkbox"<?php checked( $instance['date'], 1 ); ?> />
            <label for="<?php echo $this->get_field_id( 'date' ); ?>"><?php _e( 'Display Date', 'post' ); ?></label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id( 'show_category' ); ?>" name="<?php echo $this->get_field_name( 'show_category' ); ?>" type="checkbox"<?php checked( $instance['show_category'], 1 ); ?> />
            <label for="<?php echo $this->get_field_id( 'show_category' ); ?>"><?php _e( 'Display Category', 'post' ); ?></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category:', 'post' ); ?></label>
            <?php
                $dropdown_args = array( 'taxonomy' => 'category', 'class' => 'widefat', 'show_option_all' => __( 'All', 'post' ), 'id' => $this->get_field_id( 'category' ), 'name' => $this->get_field_name( 'category' ), 'selected' => $instance['category'] );
                wp_dropdown_categories( $dropdown_args );
            ?>
        </p>
    <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = $new_instance['title'];
        $instance['num_items'] = $new_instance['num_items'];
        $instance['date'] = (bool) esc_attr( $new_instance['date'] );
        $instance['show_category'] = (bool) esc_attr( $new_instance['show_category'] );
        $instance['category']      = intval( $new_instance['category'] );
        return $instance;
    }
    function widget($news_args, $instance) {
        extract($news_args, EXTR_SKIP);

        $current_post_name = get_query_var('name');

        $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
        $num_items = empty($instance['num_items']) ? '5' : apply_filters('widget_title', $instance['num_items']);
        if ( isset( $instance['date'] ) && ( 1 == $instance['date'] ) ) { $date = "true"; } else { $date = "false"; }
        if ( isset( $instance['show_category'] ) && ( 1 == $instance['show_category'] ) ) { $show_category = "true"; } else { $show_category = "false"; }
        if ( isset( $instance['category'] ) && is_numeric( $instance['category'] ) ) $category = intval( $instance['category'] );
        $postcount = 0;
        echo $before_widget;
?>
             <h4 class="widget-title"><?php echo $title ?></h4>
            <!--visual-columns-->
            <?php if($date == "false" && $show_category == "false"){ 
                $no_p = "no_p";
                }?>
            <div class="sp_post_static design-w3 <?php echo $no_p;?>">
			 
            <?php // setup the query
            $news_args = array( 'suppress_filters' => true,
                           'posts_per_page' => $num_items,
                           'post_type' => 'post',
                           'order' => 'DESC',
						    'cat'       => $category
                         );
          
            $cust_loop = new WP_Query($news_args);
            $post_count = $cust_loop->post_count;
          $count = 0;
            if ($cust_loop->have_posts()) : while ($cust_loop->have_posts()) : $cust_loop->the_post(); $postcount++;
                    $count++;
             
                    ?>
                   
				   
				    <div class="post-list">
						<div class="post-list-content">
						<div class="post-left-img">
						<div class="post-image-bg">
							 <a  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">                   		
                  	<?php  the_post_thumbnail( array(70, 70) ); ?>   </a>
							</div>
							</div>
							<div class="post-right-content">
							<?php if($show_category == 'true') { ?>
								<div class="recentpost-categories">		
							<?php echo get_the_category_list( $separator, $parents, $post_id ); ?>
						</div>
								<?php } ?>
							 <h3 class="post-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
							</h3>
							<?php if($date == "true") { ?>
							<div class="post-date">		
								<?php echo get_the_date(); ?>
								</div>
						<?php }?>
								
								</div>
						</div>
				</div>
				   
				   
				   
            <?php endwhile;
            endif;
             wp_reset_query(); ?>

                 </div>
<?php
        echo $after_widget;
    }
}
/* Register the widget */
function pro_sp_post_thumb_widget_load_widgets() {
    register_widget( 'PRO_SP_Post_thmb_Widget' );
}

/* Load the widget */
add_action( 'widgets_init', 'pro_sp_post_thumb_widget_load_widgets' );