<?php 
function pro_get_wprps_slider( $atts, $content = null ){
            // setup the query
            extract(shortcode_atts(array(
		"limit" => '',	
		"category" => '',
		"design" => '',	
        "show_date" => '',
        "show_category_name" => '',
        "show_content" => '',
        "content_words_limit" => '',
		"dots"     			=> '',
		"arrows"     		=> '',				
		"autoplay"     		=> '',		
		"autoplay_interval"  => '',				
		"speed"             => '',
		"show_read_more"   => '',		
		
	), $atts));
	// Define limit
	if( $limit ) { 
		$posts_per_page = $limit; 
	} else {
		$posts_per_page = '8';
	}
	if( $category ) { 
		$cat = $category; 
	} else {
		$cat = '';
	}	
	
	if( $design ) { 
		$postdesign = $design; 
	} else {
		$postdesign = 'design-1';
	}	
	
    if( $show_date ) { 
        $showDate = $show_date; 
    } else {
        $showDate = 'true';
    }
	if( $show_category_name ) { 
        $showCategory = $show_category_name; 
    } else {
        $showCategory = 'true';
    }
    if( $show_content ) { 
        $showContent = $show_content; 
    } else {
        $showContent = 'true';
    }
	 if( $content_words_limit ) { 
        $words_limit = $content_words_limit; 
    } else {
        $words_limit = '20';
    }
	
	if( $dots ) { 
		$dotsv = $dots; 
	} else {
		$dotsv = 'true';
	}
	
	if( $arrows ) { 
		$arrowsv = $arrows; 
	} else {
		$arrowsv = 'true';
	}
	
	if( $autoplay ) { 
		$autoplayv = $autoplay; 
	} else {
		$autoplayv = 'true';
	}
	
	if( $autoplay_interval ) { 
		$autoplayIntervalv = $autoplay_interval; 
	} else {
		$autoplayIntervalv = '3000';
	}
	
	if( $speed ) { 
		$speedv = $speed; 
	} else {
		$speedv = '300';
	}
	
	if( $show_read_more ) { 
		$showreadmore = $show_read_more; 
	} else {
		$showreadmore = 'true';
	}

	
	ob_start();
	
	$post_type 		= 'post';
	$orderby 		= 'post_date';
	$order 			= 'DESC';
				 
		
        $args = array ( 
            'post_type'      => $post_type, 
            'orderby'        => $orderby, 
            'order'          => $order,
            'posts_per_page' => $posts_per_page, 
			'cat'       => $cat
            
            ); 
            
        $query = new WP_Query($args);
      $post_count = $query->post_count;
         
             if ( $query->have_posts() ) :
			 ?>
		<div class="recent-post-slider <?php echo $postdesign; ?>">
				<?php
			 while ( $query->have_posts() ) : $query->the_post();
                         
               
             switch ($postdesign) {
				 case "design-1":
					include('designs/design-1.php');
					break;
				 case "design-2":
					include('designs/design-2.php');
					break;
				 case "design-3":
					include('designs/design-3.php');
					break;
				 case "design-4":
					include('designs/design-4.php');
					break;
				 case "design-5":
					include('pro/designs/design-5.php');
					break;
				 case "design-6":
					include('pro/designs/design-6.php');
					break;		
				 default:					 
						include('designs/design-1.php');
					}


					endwhile; ?>
		  </div><!-- #post-## -->		
		  <?php
            endif; ?>
<script type="text/javascript">
		jQuery(document).ready(function(){
		jQuery('.recent-post-slider.<?php echo $postdesign; ?>').slick({
			dots: <?php echo $dotsv; ?>,
			infinite: true,
			arrows: <?php echo $arrowsv; ?>,
			speed: <?php echo $speedv; ?>,
			autoplay: <?php echo $autoplayv; ?>,						
			autoplaySpeed: <?php echo $autoplayIntervalv; ?>,
			slidesToShow: 1,
			slidesToScroll: 1,
			responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
		});
	});
	</script>			
			<?php
             wp_reset_query(); 
				
		return ob_get_clean();			             
	}
add_shortcode('recent_post_slider','pro_get_wprps_slider');

