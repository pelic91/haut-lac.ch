<?php 
function pro_get_wprps_carousel( $atts, $content = null ){
            // setup the query
            extract(shortcode_atts(array(
		"limit" => '',	
		"category" => '',
		"design" => '',	
        "show_date" => '',
        "show_category_name" => '',
        "show_content" => '',
        "content_words_limit" => '',
		"dots"     			=> '',
		"arrows"     		=> '',				
		"autoplay"     		=> '',		
		"autoplay_interval"  => '',				
		"speed"             => '',
        "show_read_more"   => '',
		"slides_to_show"	=> '',
		"slides_to_scroll" => '',
		
	), $atts));
	// Define limit
	if( $limit ) { 
		$posts_per_page = $limit; 
	} else {
		$posts_per_page = '8';
	}
	if( $category ) { 
		$cat = $category; 
	} else {
		$cat = '';
	}	
	
	if( $design ) { 
		$postdesign = $design; 
	} else {
		$postdesign = 'design-7';
	}	
	
    if( $show_date ) { 
        $showDate = $show_date; 
    } else {
        $showDate = 'true';
    }
	if( $show_category_name ) { 
        $showCategory = $show_category_name; 
    } else {
        $showCategory = 'true';
    }
    if( $show_content ) { 
        $showContent = $show_content; 
    } else {
        $showContent = 'true';
    }
	 if( $content_words_limit ) { 
        $words_limit = $content_words_limit; 
    } else {
        $words_limit = '20';
    }
	
	if( $dots ) { 
		$dotsv = $dots; 
	} else {
		$dotsv = 'true';
	}
	
	if( $arrows ) { 
		$arrowsv = $arrows; 
	} else {
		$arrowsv = 'true';
	}
	
	if( $autoplay ) { 
		$autoplayv = $autoplay; 
	} else {
		$autoplayv = 'true';
	}
	
	if( $autoplay_interval ) { 
		$autoplayIntervalv = $autoplay_interval; 
	} else {
		$autoplayIntervalv = '3000';
	}
	
	if( $speed ) { 
		$speedv = $speed; 
	} else {
		$speedv = '300';
	}
	if( $show_read_more ) { 
		$showreadmore = $show_read_more; 
	} else {
		$showreadmore = 'true';
	}
	if( $slides_to_show ) { 
		$slidesToShow = $slides_to_show; 
	} else {
		$slidesToShow = '3';
	}
	if( $slides_to_scroll ) { 
		$slidesToScroll = $slides_to_scroll; 
	} else {
		$slidesToScroll = '1';
	}

	
	ob_start();
	
	$post_type 		= 'post';
	$orderby 		= 'post_date';
	$order 			= 'DESC';
				 
		
        $args = array ( 
            'post_type'      => $post_type, 
            'orderby'        => $orderby, 
            'order'          => $order,
            'posts_per_page' => $posts_per_page, 
			'cat'       => $cat
            
            ); 
            
        $query = new WP_Query($args);
      $post_count = $query->post_count;
         
             if ( $query->have_posts() ) :
			 ?>
		<div class="recent-post-carousel <?php echo $postdesign; ?>">
				<?php
			 while ( $query->have_posts() ) : $query->the_post();
                         
               
             switch ($postdesign) {
				 case "design-7":
					include('designs/design-7.php');
					break;
				 case "design-8":
					include('designs/design-8.php');
					break;
                 case "design-9":
					include('designs/design-9.php');
					break;
				  case "design-10":
					include('designs/design-10.php');
					break;
                  case "design-11":
					include('designs/design-11.php');
					break;
				 case "design-12":
					include('designs/design-12.php');
					break;
				case "design-13":
					include('designs/design-13.php');
					break;
				case "design-14":
					include('designs/design-14.php');
					break;	
                case "design-15":
					include('designs/design-15.php');
					break;	
                 case "design-16":
					include('designs/design-16.php');
					break;					
				 default:					 
						include('designs/design-7.php');
					}


					endwhile; ?>
		  </div><!-- #post-## -->		
		  <?php
            endif; ?>
<script type="text/javascript">
		jQuery(document).ready(function(){
		jQuery('.recent-post-carousel.<?php echo $postdesign; ?>').slick({
			dots: <?php echo $dotsv; ?>,
			infinite: true,
			arrows: <?php echo $arrowsv; ?>,
			speed: <?php echo $speedv; ?>,
			autoplay: <?php echo $autoplayv; ?>,						
			autoplaySpeed: <?php echo $autoplayIntervalv; ?>,
			slidesToShow: <?php echo $slidesToShow; ?>,
			slidesToScroll: <?php echo $slidesToScroll; ?>,
			responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 640,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
		});
	});
	</script>			
			<?php
             wp_reset_query(); 
				
		return ob_get_clean();			             
	}
add_shortcode('recent_post_carousel','pro_get_wprps_carousel');
