<div class="post-slides">  
		<div class="post-grid-content">
		<div class="post-overlay">

			<div class="medium-6 columns">
			<div class="post-image-bg">
				<?php the_post_thumbnail('url'); ?>
				<?php if($showCategory == "true") { ?>
					<div class="recentpost-categories">		
			<?php echo get_the_category_list( $separator, $parents, $post_id ); ?>
		</div>
				<?php } ?>
			</div>
			</div>

			<div class="medium-6 columns">
			<div class="post-short-content">
			<div class="bottom-content">
			 <h1 class="post-title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h1>
			<?php if($showDate == "true") {  ?>	
			<div class="post-date">		
				<?php echo get_the_date(); ?>
				</div>
				<?php } 
				if($showContent == "true") {  ?>	
			<div class="post-content">
					<?php $excerpt = get_the_content();?>
					<p><?php echo pro_wprps_limit_words($excerpt,$words_limit); ?>...</p>
					<?php if($showreadmore == 'true') { ?>
					<p><a class="readmorebtn" href="<?php the_permalink(); ?>">Read More &raquo;</a></p>
					<?php } ?>
			</div>
			<?php } ?>
			</div>
			</div>	
			</div>

			</div>	
				
		</div>
	</div>