 <div class="post-slides">
		<div class="post-grid-content">
		<div class="post-image-bg">
			<?php the_post_thumbnail('url'); ?>
			</div>
			<?php if($showCategory == "true") { ?>
				<div class="recentpost-categories">		
			<?php echo get_the_category_list( $separator, $parents, $post_id ); ?>
		</div>
				<?php } ?>
			 <h2 class="post-title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h2>
			<?php if($showDate == "true") {  ?>	
			<div class="post-date">		
				<?php echo get_the_date(); ?>
				</div>
				<?php } 
				if($showContent == "true") {  ?>	
				<div class="post-content">
					<?php $excerpt = get_the_content();?>
					<p><?php echo pro_wprps_limit_words($excerpt,$words_limit); ?>...</p>
					<?php if($showreadmore == 'true') { ?>
					<p><a class="readmorebtn" href="<?php the_permalink(); ?>">Read More &raquo;</a></p>
					<?php } ?>
				</div>
				<?php } ?>
		</div>
	</div>