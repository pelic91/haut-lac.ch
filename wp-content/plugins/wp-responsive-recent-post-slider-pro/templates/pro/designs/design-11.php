<div class="post-slides">  
		<div class="post-grid-content">
		<div class="post-overlay">
			<div class="post-image-bg">
			<?php the_post_thumbnail('url'); ?>
				<?php if($showCategory == "true") { ?>
					<div class="recentpost-categories">		
			<?php echo get_the_category_list( $separator, $parents, $post_id ); ?>
		</div>
				<?php } ?>
			</div>
			<div class="post-short-content">
			 <h2 class="post-title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h2>
			<?php if($showDate == "true") {  ?>	
			<div class="post-date">		
				<?php echo get_the_date(); ?>
				</div>
				<?php } ?>
			</div>	
			</div>	
				
		</div>
	</div>