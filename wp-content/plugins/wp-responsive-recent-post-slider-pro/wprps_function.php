<?php
function pro_wprps_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}


// Manage Category Shortcode Columns

add_filter("manage_category_custom_column", 'pro_category_columns', 10, 3);
add_filter("manage_edit-category_columns", 'pro_category_manage_columns'); 
function pro_category_manage_columns($theme_columns) {
    $new_columns = array(
            'cb' => '<input type="checkbox" />',
            'name' => __('Name'),
            'post_slider_shortcode' => __( 'Category Shortcode', 'post' ),
            'slug' => __('Slug'),
            'posts' => __('Posts')
			);
    return $new_columns;
}

function pro_category_columns($out, $column_name, $theme_id) {
    $theme = get_term($theme_id, 'category');
    switch ($column_name) {      

        case 'title':
            echo get_the_title();
        break;
        case 'post_slider_shortcode':        

             echo '[recent_post_slider category="' . $theme_id. '"]<br />';
			  echo '[recent_post_carousel category="' . $theme_id. '"]';
        break;

        default:
            break;
    }
    return $out;   

}