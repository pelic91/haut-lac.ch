<?php

/**
 *Plugin Name:  Custom Post Google
 * 
 */
add_action( 'init', 'codex_google_init' );

function codex_google_init() {
	$labels = array(
		'name'               => _x( 'Maps', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Map', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Maps', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Map', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'map', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Map', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Map', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Map', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Map', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Maps', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Maps', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Maps:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No maps found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No maps found in Trash.', 'your-plugin-textdomain' ),
	);
    $args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'postmaps' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
    register_post_type( 'postmaps', $args );
}
?>