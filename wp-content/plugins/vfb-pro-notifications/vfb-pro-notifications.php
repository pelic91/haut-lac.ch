<?php
/*
Plugin Name: Visual Form Builder Pro - Notifications
Plugin URI: http://vfbpro.com
Description: An add-on for Visual Form Builder Pro that connects your forms with a number of third-party services such as MailChimp and Campaign Monitor after new form submissions.
Author: Matthew Muro
Author URI: http://matthewmuro.com
Version: 1.0
*/

$vfb_notifications_load = new VFB_Pro_Notifications();

// VFB Pro Notifications class
class VFB_Pro_Notifications{

	/**
	 * The plugin API
	 *
	 * @since 1.0
	 * @var string
	 * @access protected
	 */
	protected $api_url = 'http://matthewmuro.com/plugin-api/notifications/';

	/**
	 * Constructor. Register core filters and actions.
	 *
	 * @access public
	 */
	public function __construct() {
		global $wpdb;

		// Setup global database table names
		$this->field_table_name 	= $wpdb->prefix . 'vfb_pro_fields';
		$this->form_table_name 		= $wpdb->prefix . 'vfb_pro_forms';

		// Enable saving function
		add_action( 'admin_init', array( &$this, 'save' ) );

		// Add Notifications to main VFB menu
		add_action( 'admin_menu', array( &$this, 'admin_menu' ), 40 );

		// Display Admin notices when saving
		add_action( 'admin_notices', array( &$this, 'admin_notices' ) );

		// Hook into action after email is sent
		add_action( 'vfb_after_email', array( &$this, 'notify_cell_phone' ), 10, 2 );
		add_action( 'vfb_after_email', array( &$this, 'notify_mailchimp' ), 10, 2 );
		add_action( 'vfb_after_email', array( &$this, 'notify_campaign_monitor' ), 10, 2 );
		add_action( 'vfb_after_email', array( &$this, 'notify_highrise' ), 10, 2 );
		add_action( 'vfb_after_email', array( &$this, 'notify_freshbooks' ), 10, 2 );

		// Register AJAX functions
		$actions = array(
			'mailchimp_api',
			'campaign_monitor_api',
			'campaign_monitor_get_lists',
			'highrise_api',
			'freshbooks_api',
		);

		// Add all AJAX functions
		foreach( $actions as $name ) {
			add_action( "wp_ajax_vfb_notifications_$name", array( &$this, "ajax_$name" ) );
		}

		// Load i18n
		add_action( 'plugins_loaded', array( &$this, 'languages' ) );

		// Display plugin details screen for updating
		add_filter( 'plugins_api', array( &$this, 'api_information' ), 10, 3 );

		// Hook into the plugin update check
		add_filter( 'pre_set_site_transient_update_plugins', array( &$this, 'api_check' ) );

		// For testing only
		//add_action( 'init', array( &$this, 'delete_transient' ) );
	}

	/**
	 * Load localization file
	 *
	 * @since 1.0
	 */
	public function languages() {
		load_plugin_textdomain( 'vfb-pro-notifications', false , 'vfb-pro-notifications/languages' );
	}

	/**
	 * ajax_mailchimp_api function.
	 *
	 * @access public
	 * @return void
	 */
	public function ajax_mailchimp_api() {

		$data = array();

		if ( 'vfb_notifications_mailchimp_api' !== $_REQUEST['action'] )
			return;

		$form_id = absint( $_REQUEST['form_id'] );
		$api_key = esc_html( $_REQUEST['api'] );

		if ( empty( $api_key ) )
			die(1);

		if ( !strpos( $api_key, '-' ) ) {
			echo sprintf( '<td colspan="2" class="vfb-mailchimp-response"><label class="error">%s</label></td>',
				__( 'Error: API key does not appear to be valid.', 'vfb-pro-notifications' )
			);
			die(0);
		}

		list(, $datacentre) = explode( '-', $api_key );
		$api_endpoint = str_replace( '<dc>', $datacentre, 'https://<dc>.api.mailchimp.com/2.0/' );

		$args = array(
			'apikey'	=> $api_key,
		);

		$request = wp_remote_post( "$api_endpoint/lists/list.json", array( 'body' => $args ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {
			echo sprintf( '<td colspan="2" class="vfb-mailchimp-response"><label class="error">%s</label></td>',
				__( 'No MailChimp account found. Please make sure you have entered the API key correctly.', 'vfb-pro-notifications' )
			);
			die(0);
		}

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		update_option( 'vfb-notifications-mailchimp-' . $form_id, trim( $response ) );

		$this->mailchimp_admin_output( $response, $form_id );

		die(1);
	}

	/**
	 * ajax_campaign_monitor_api function.
	 *
	 * @access public
	 * @return void
	 */
	public function ajax_campaign_monitor_api() {
		$data = array();

		if ( 'vfb_notifications_campaign_monitor_api' !== $_REQUEST['action'] )
			return;

		$form_id = absint( $_REQUEST['form_id'] );
		$api_key = esc_html( $_REQUEST['api'] );

		if ( empty( $api_key ) )
			die(1);

		$api_endpoint = 'https://api.createsend.com/api/v3/clients.json';

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' )
		);

		$request = wp_remote_get( "$api_endpoint", array( 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {
			echo sprintf( '<td colspan="2" class="vfb-campaign-monitor-response"><label class="error">%s</label></td>',
				__( 'No Campaign Monitor account found. Please make sure you have entered the API key correctly.', 'vfb-pro-notifications' )
			);
			die(0);
		}

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		update_option( 'vfb-notifications-campaign-monitor-clients-' . $form_id, trim( $response ) );

		$this->campaign_monitor_admin_client_output( $response, $form_id );

		die(1);
	}

	/**
	 * ajax_campaign_monitor_get_lists function.
	 *
	 * @access public
	 * @return void
	 */
	public function ajax_campaign_monitor_get_lists() {
		$data = array();

		if ( 'vfb_notifications_campaign_monitor_get_lists' !== $_REQUEST['action'] )
			return;

		$form_id      = absint( $_REQUEST['form_id'] );
		$api_key      = esc_html( $_REQUEST['api'] );
		$clientIDs    = esc_html( $_REQUEST['clients'] );

		if ( empty( $clientIDs ) )
			die(1);

		$api_endpoint = "https://api.createsend.com/api/v3/clients/{$clientIDs}/lists.json";

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' )
		);

		$request = wp_remote_get( "$api_endpoint", array( 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {
			echo sprintf( '<td colspan="2" class="vfb-campaign-monitor-response"><label class="error">%s</label></td>',
				__( 'No lists found. Please try again.', 'vfb-pro-notifications' )
			);
			die(0);
		}

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		update_option( 'vfb-notifications-campaign-monitor-lists-' . $form_id, trim( $response ) );

		$this->campaign_monitor_admin_output( $response, $form_id );

		die(1);
	}

	/**
	 * ajax_highrise_api function.
	 *
	 * @access public
	 * @return void
	 */
	public function ajax_highrise_api() {

		$data = array();

		if ( 'vfb_notifications_highrise_api' !== $_REQUEST['action'] )
			return;

		$form_id   = absint( $_REQUEST['form_id'] );
		$subdomain = esc_html( $_REQUEST['domain'] );
		$api_key   = esc_html( $_REQUEST['api'] );

		if ( empty( $api_key ) || empty( $subdomain ) )
			die(1);

		$args = array(
			'apikey'	=> $api_key,
		);

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' )
		);

		$request = wp_remote_get( "https://{$subdomain}.highrisehq.com/account.xml", array( 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {
			echo sprintf( '<td colspan="2" class="vfb-highrise-response"><label class="error">%s</label></td>',
				__( 'No Highrise account found. Please make sure you have entered the subdomain and API key correctly.', 'vfb-pro-notifications' )
			);
			die(0);
		}

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		update_option( 'vfb-notifications-highrise-' . $form_id, trim( $response ) );

		$this->highrise_admin_output( $form_id );

		die(1);
	}

	/**
	 * ajax_freshbooks_api function.
	 *
	 * @access public
	 * @return void
	 */
	public function ajax_freshbooks_api() {
		$data = array();

		if ( 'vfb_notifications_freshbooks_api' !== $_REQUEST['action'] )
			return;

		$form_id   = absint( $_REQUEST['form_id'] );
		$subdomain = esc_html( $_REQUEST['domain'] );
		$api_key   = esc_html( $_REQUEST['api'] );

		if ( empty( $api_key ) || empty( $subdomain ) )
			die(1);

		$args = array(
			'apikey'	=> $api_key,
		);

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' ),
		);

		$body = '<?xml version="1.0" encoding="utf-8"?>
		<request method="system.current">
		</request>';

		$request = wp_remote_post( "https://{$subdomain}.freshbooks.com/api/2.1/xml-in", array( 'body' => $body, 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {
			echo sprintf( '<td colspan="2" class="vfb-freshbooks-response"><label class="error">%s</label></td>',
				__( 'No FreshBooks account found. Please make sure you have entered the subdomain and API key correctly.', 'vfb-pro-notifications' )
			);
			die(0);
		}

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		update_option( 'vfb-notifications-freshbooks-' . $form_id, trim( $response ) );

		$this->freshbooks_admin_output( $form_id );

		die(1);
	}

	/**
	 * mailchimp_admin_output function.
	 *
	 * @access public
	 * @param mixed $response
	 * @return void
	 */
	public function mailchimp_admin_output( $response, $form_id = 0 ) {

		$chimp = json_decode( $response );
		$settings = get_option( 'vfb-notifications-' . $form_id );

		if ( $chimp->total < 1 ) {
			_e( 'No lists found. Add a list to your MailChimp account and try again.', 'vfb-pro-notifications' );
			return;
		}

		$all_fields   = $this->get_fields( $form_id );
		$email_fields = $this->get_fields( $form_id, 'email' );
		$phone_fields = $this->get_fields( $form_id, 'phone' );

		$settings['mailchimp-list']   = isset( $settings['mailchimp-list'] ) ? $settings['mailchimp-list'] : $chimp->data[0]->id;
		$settings['mailchimp-opt-in'] = isset( $settings['mailchimp-opt-in'] ) ? $settings['mailchimp-opt-in'] : 0;

		$mailchimp_settings = array(
			'mailchimp-merge-email',
			'mailchimp-merge-fname',
			'mailchimp-merge-lname',
			'mailchimp-merge-company',
			'mailchimp-merge-phone',
		);

		foreach ( $mailchimp_settings as $mailchimp ) {
			$settings[ $mailchimp ]  = isset( $settings[ $mailchimp ] ) ? $settings[ $mailchimp ] : '';
		}
?>
<tr align="top" class="vfb-mailchimp-response">
	<th>
		<label for="vfb-notify-mailchimp-list" class="">
			<?php _e( 'Select A List' , 'vfb-pro-notifications'); ?>
		</label>
	</th>
	<td>
		<select id="vfb-notify-mailchimp-list" name="vfb-notify[mailchimp-list]">
			<?php foreach ( $chimp->data as $data ) : ?>
			<option value="<?php echo esc_attr( $data->id ); ?>"<?php selected( $settings['mailchimp-list'], $data->id ); ?>><?php echo esc_html( $data->name ); ?></option>
			<?php endforeach; ?>
		</select>

		<p>
			<label for="vfb-notify-mailchimp-opt-in">
				<input type="checkbox" id="vfb-notify-mailchimp-opt-in" name="vfb-notify[mailchimp-opt-in]" value="1"<?php checked( $settings['mailchimp-opt-in'], 1 ); ?> /> <?php _e( 'Send Opt-In Email', 'vfb-pro-notifications' ); ?>
			</label>
		</p>
	</td>
</tr>

<tr align="top" class="vfb-mailchimp-response">
	<th>
		<?php _e( 'Merge Tags' , 'vfb-pro-notifications'); ?>
	</th>
	<td>
		<ul>
			<li>
				<label for="vfb-notify-mailchimp-merge-email"><?php _e( 'Email Address', 'vfb-pro-notifications' ); ?> <span class="vfb-notify-required-asterisk">*</span></label>
				<select id="vfb-notify-mailchimp-merge-email" name="vfb-notify[mailchimp-merge-email]" class="required">
					<?php $this->fields_options( $email_fields, $settings['mailchimp-merge-email'] ); ?>
				</select>
				<?php if ( !$email_fields ) : ?>
				<label class="error"><?php _e( 'No Email fields found for this form. Please edit your form to include an Email field and set Required to "Yes".', 'vfb-pro-notifications' ); ?></label>
				<?php endif; ?>
			</li>
			<li>
				<label for="vfb-notify-mailchimp-fname"><?php _e( 'First Name', 'vfb-pro-notifications' ); ?></label>
				<select id="vfb-notify-mailchimp-merge-fname" name="vfb-notify[mailchimp-merge-fname]">
					<?php $this->fields_options( $all_fields, $settings['mailchimp-merge-fname'] ); ?>
				</select>
			</li>
			<li>
				<label for="vfb-notify-mailchimp-lname"><?php _e( 'Last Name', 'vfb-pro-notifications' ); ?></label>
				<select id="vfb-notify-mailchimp-merge-lname" name="vfb-notify[mailchimp-merge-lname]">
					<?php $this->fields_options( $all_fields, $settings['mailchimp-merge-lname'] ); ?>
				</select>
			</li>
			<li>
				<label for="vfb-notify-mailchimp-company"><?php _e( 'Company', 'vfb-pro-notifications' ); ?></label>
				<select id="vfb-notify-mailchimp-merge-company" name="vfb-notify[mailchimp-merge-company]">
					<?php $this->fields_options( $all_fields, $settings['mailchimp-merge-company'] ); ?>
				</select>
			</li>
			<li>
				<label for="vfb-notify-mailchimp-phone"><?php _e( 'Phone', 'vfb-pro-notifications' ); ?></label>
				<select id="vfb-notify-mailchimp-merge-phone" name="vfb-notify[mailchimp-merge-phone]">
					<?php $this->fields_options( $phone_fields, $settings['mailchimp-merge-phone'] ); ?>
				</select>
			</li>
		</ul>
	</td>
</tr>
<?php
	}

	/**
	 * campaign_monitor_admin_client_output function.
	 *
	 * @access public
	 * @param mixed $response
	 * @param int $form_id (default: 0)
	 * @return void
	 */
	public function campaign_monitor_admin_client_output( $response, $form_id = 0 ) {

		$campaign = json_decode( $response );
		$settings = get_option( 'vfb-notifications-' . $form_id );

		if ( count( $campaign ) < 1 ) {
			_e( 'No clients found. Add a client to your Campaign Monitor account and try again.', 'vfb-pro-notifications' );
			return;
		}

		$settings['campaign-monitor-clientIDs'] = isset( $settings['campaign-monitor-clientIDs'] ) ? $settings['campaign-monitor-clientIDs'] : $campaign[0]->ClientID;
?>
<tr align="top" id="vfb-campaign-monitor-client-response" class="vfb-campaign-monitor-response">
	<th>
		<label for="vfb-notify-campaign-monitor-clientIDs" class="">
			<?php _e( 'Select A Client' , 'vfb-pro-notifications'); ?>
		</label>
	</th>
	<td>
		<select id="vfb-notify-campaign-monitor-clientIDs" name="vfb-notify[campaign-monitor-clientIDs]">
			<?php foreach ( $campaign as $data ) : ?>
			<option value="<?php echo esc_attr( $data->ClientID ); ?>"<?php selected( $settings['campaign-monitor-clientIDs'], $data->ClientID ); ?>><?php echo esc_html( $data->Name ); ?></option>
			<?php endforeach; ?>
		</select>
		<a href="#" class="button" id="vfb-notify-campaign-monitor-select-client">
			<?php _e( 'Select Client', 'vfb-pro-notifications' ); ?><img alt="" src="<?php echo admin_url( '/images/wpspin_light.gif' ); ?>" class="waiting spinner" />
		</a>
	</td>
</tr>
<?php
	}

	/**
	 * campaign_monitor_admin_output function.
	 *
	 * @access public
	 * @param mixed $response
	 * @param int $form_id (default: 0)
	 * @return void
	 */
	public function campaign_monitor_admin_output( $response, $form_id = 0 ) {

		$campaign = json_decode( $response );
		$settings = get_option( 'vfb-notifications-' . $form_id );


		if ( count( $campaign ) < 1 ) {
			_e( 'No lists found. Add a list to the selected client in Campaign Monitor and try again.', 'vfb-pro-notifications' );
			return;
		}

		$all_fields   = $this->get_fields( $form_id );
		$email_fields = $this->get_fields( $form_id, 'email' );
		$name_fields  = $this->get_fields( $form_id, 'name' );

		$settings['campaign-monitor-listIDs'] = isset( $settings['campaign-monitor-listIDs'] ) ? $settings['campaign-monitor-listIDs'] : $campaign[0]->ListID;

		$campaign_settings = array(
			'campaign-monitor-merge-email',
			'campaign-monitor-merge-name',
		);

		foreach ( $campaign_settings as $monitor ) {
			$settings[ $monitor ]  = isset( $settings[ $monitor ] ) ? $settings[ $monitor ] : '';
		}
?>
<tr align="top" class="vfb-campaign-monitor-response">
	<th>
		<label for="vfb-notify-campaign-monitor-listIDs" class="">
			<?php _e( 'Select A List' , 'vfb-pro-notifications'); ?>
		</label>
	</th>
	<td>
		<select id="vfb-notify-campaign-monitor-listIDs" name="vfb-notify[campaign-monitor-listIDs]">
			<?php foreach ( $campaign as $data ) : ?>
			<option value="<?php echo esc_attr( $data->ListID ); ?>"<?php selected( $settings['campaign-monitor-listIDs'], $data->ListID ); ?>><?php echo esc_html( $data->Name ); ?></option>
			<?php endforeach; ?>
		</select>
	</td>
</tr>

<tr align="top" class="vfb-campaign-monitor-response">
	<th>
		<?php _e( 'Merge Tags' , 'vfb-pro-notifications'); ?>
	</th>
	<td>
		<ul>
			<li>
				<label for="vfb-notify-campaign-monitor-merge-email"><?php _e( 'Email Address', 'vfb-pro-notifications' ); ?> <span class="vfb-notify-required-asterisk">*</span></label>
				<select id="vfb-notify-campaign-monitor-merge-email" name="vfb-notify[campaign-monitor-merge-email]" class="required">
					<?php $this->fields_options( $email_fields, $settings['campaign-monitor-merge-email'] ); ?>
				</select>
				<?php if ( !$email_fields ) : ?>
				<label class="error"><?php _e( 'No Email fields found for this form. Please edit your form to include an Email field and set Required to "Yes".', 'vfb-pro-notifications' ); ?></label>
				<?php endif; ?>
			</li>
			<li>
				<label for="vfb-notify-campaign-monitor-merge-name"><?php _e( 'Name', 'vfb-pro-notifications' ); ?></label>
				<select id="vfb-notify-campaign-monitor-merge-name" name="vfb-notify[campaign-monitor-merge-name]">
					<?php $this->fields_options( $name_fields, $settings['campaign-monitor-merge-name'] ); ?>
				</select>
			</li>
		</ul>
	</td>
</tr>

<?php
	}

	/**
	 * highrise_admin_output function.
	 *
	 * @access public
	 * @param int $form_id (default: 0)
	 * @return void
	 */
	public function highrise_admin_output( $form_id = 0 ) {
		$settings = get_option( 'vfb-notifications-' . $form_id );

		$all_fields   = $this->get_fields( $form_id );
		$name_fields  = $this->get_fields( $form_id, 'name' );
		$email_fields = $this->get_fields( $form_id, 'email' );
		$phone_fields = $this->get_fields( $form_id, 'phone' );

		$highrise_settings = array(
			'highrise-merge-email',
			'highrise-merge-name',
			'highrise-merge-company',
			'highrise-merge-phone',
			'highrise-note',
		);

		foreach ( $highrise_settings as $highrise ) {
			$settings[ $highrise ]  = isset( $settings[ $highrise ] ) ? $settings[ $highrise ] : '';
		}
?>
<tr align="top" class="vfb-highrise-response">
	<th>
		<?php _e( 'Merge Tags' , 'vfb-pro-notifications'); ?>
	</th>
	<td>
		<ul>
			<li>
				<label for="vfb-notify-highrise-name"><?php _e( 'Name', 'vfb-pro-notifications' ); ?> <span class="vfb-notify-required-asterisk">*</span></label>
				<select id="vfb-notify-highrise-merge-name" name="vfb-notify[highrise-merge-name]" class="required">
					<?php $this->fields_options( $name_fields, $settings['highrise-merge-name'] ); ?>
				</select>
				<?php if ( !$name_fields ) : ?>
				<label class="error"><?php _e( 'No Name fields found for this form. Please edit your form to include a Name field and set Required to "Yes".', 'vfb-pro-notifications' ); ?></label>
				<?php endif; ?>
			</li>
			<li>
				<label for="vfb-notify-highrise-merge-email"><?php _e( 'Email Address', 'vfb-pro-notifications' ); ?></label>
				<select id="vfb-notify-highrise-merge-email" name="vfb-notify[highrise-merge-email]">
					<?php $this->fields_options( $email_fields, $settings['highrise-merge-email'] ); ?>
				</select>
			</li>
			<li>
				<label for="vfb-notify-highrise-company"><?php _e( 'Company', 'vfb-pro-notifications' ); ?></label>
				<select id="vfb-notify-highrise-merge-company" name="vfb-notify[highrise-merge-company]">
					<?php $this->fields_options( $all_fields, $settings['highrise-merge-company'] ); ?>
				</select>
			</li>
			<li>
				<label for="vfb-notify-highrise-phone"><?php _e( 'Phone', 'vfb-pro-notifications' ); ?></label>
				<select id="vfb-notify-highrise-merge-phone" name="vfb-notify[highrise-merge-phone]">
					<?php $this->fields_options( $phone_fields, $settings['highrise-merge-phone'] ); ?>
				</select>
			</li>
		</ul>
	</td>
</tr>
<tr align="top" class="vfb-highrise-response">
	<th>
		<?php _e( 'Attach a Note' , 'vfb-pro-notifications'); ?>
	</th>
	<td>
		<textarea name="vfb-notify[highrise-note]" rows="5" cols="50"><?php echo $settings['highrise-note']; ?></textarea>
	</td>
</tr>
<?php
	}

	/**
	 * freshbooks_admin_output function.
	 *
	 * @access public
	 * @param mixed $form_id
	 * @return void
	 */
	public function freshbooks_admin_output( $form_id ) {
		$settings = get_option( 'vfb-notifications-' . $form_id );

		$all_fields   = $this->get_fields( $form_id );
		$name_fields  = $this->get_fields( $form_id, 'name' );
		$email_fields = $this->get_fields( $form_id, 'email' );
		$phone_fields = $this->get_fields( $form_id, 'phone' );

		$freshbooks_settings = array(
			'freshbooks-merge-email',
			'freshbooks-merge-name',
			'freshbooks-merge-company',
			'freshbooks-merge-phone',
			'freshbooks-note',
		);

		foreach ( $freshbooks_settings as $freshbooks ) {
			$settings[ $freshbooks ]  = isset( $settings[ $freshbooks ] ) ? $settings[ $freshbooks ] : '';
		}
?>
<tr align="top" class="vfb-freshbooks-response">
	<th>
		<?php _e( 'Merge Tags' , 'vfb-pro-notifications'); ?>
	</th>
	<td>
		<ul>
			<li>
				<label for="vfb-notify-freshbooks-name"><?php _e( 'Name', 'vfb-pro-notifications' ); ?> <span class="vfb-notify-required-asterisk">*</span></label>
				<select id="vfb-notify-freshbooks-merge-name" name="vfb-notify[freshbooks-merge-name]" class="required">
					<?php $this->fields_options( $name_fields, $settings['freshbooks-merge-name'] ); ?>
				</select>
				<?php if ( !$name_fields ) : ?>
				<label class="error"><?php _e( 'No Name fields found for this form. Please edit your form to include a Name field and set Required to "Yes".', 'vfb-pro-notifications' ); ?></label>
				<?php endif; ?>
			</li>
			<li>
				<label for="vfb-notify-freshbooks-merge-email"><?php _e( 'Email Address', 'vfb-pro-notifications' ); ?> <span class="vfb-notify-required-asterisk">*</span></label>
				<select id="vfb-notify-freshbooks-merge-email" name="vfb-notify[freshbooks-merge-email]" class="required">
					<?php $this->fields_options( $email_fields, $settings['freshbooks-merge-email'] ); ?>
				</select>
				<?php if ( !$email_fields ) : ?>
				<label class="error"><?php _e( 'No Email fields found for this form. Please edit your form to include an Email field and set Required to "Yes".', 'vfb-pro-notifications' ); ?></label>
				<?php endif; ?>
			</li>
			<li>
				<label for="vfb-notify-freshbooks-company"><?php _e( 'Company', 'vfb-pro-notifications' ); ?> <span class="vfb-notify-required-asterisk">*</span></label>
				<select id="vfb-notify-freshbooks-merge-company" name="vfb-notify[freshbooks-merge-company]" class="required">
					<?php $this->fields_options( $all_fields, $settings['freshbooks-merge-company'] ); ?>
				</select>
				<?php if ( !$all_fields ) : ?>
				<label class="error"><?php _e( 'No fields found for this form. Please edit your form to include a Text field and set Required to "Yes".', 'vfb-pro-notifications' ); ?></label>
				<?php endif; ?>
			</li>
			<li>
				<label for="vfb-notify-freshbooks-phone"><?php _e( 'Phone', 'vfb-pro-notifications' ); ?></label>
				<select id="vfb-notify-freshbooks-merge-phone" name="vfb-notify[freshbooks-merge-phone]">
					<?php $this->fields_options( $phone_fields, $settings['freshbooks-merge-phone'] ); ?>
				</select>
			</li>
		</ul>
	</td>
</tr>
<?php
	}

	/**
	 * Queue plugin scripts and CSS for sorting form fields
	 *
	 * @since 1.0
	 */
	public function admin_scripts() {
		wp_enqueue_style( 'vfb-notifications-admin-css', plugins_url( '/css/vfb-notifications-admin.css', __FILE__ ), array(), '2' );

		wp_enqueue_script( 'jquery-form-validation', plugins_url( 'visual-form-builder-pro/js/jquery.validate.min.js' ), array( 'jquery' ), '1.9.0', true );
		wp_enqueue_script( 'vfb-notifications-admin', plugins_url( '/js/vfb-notifications-admin.js', __FILE__ ), array( 'jquery', 'jquery-form-validation' ), '5', true );
	}

	/**
	 * Display saving messages in admin
	 *
	 * @since 1.0
	 */
	public function admin_notices() {
		if ( !is_plugin_active( 'visual-form-builder-pro/visual-form-builder-pro.php' ) )
			echo sprintf( '<div id="message" class="error"><p>%s</p></div>', __( 'Visual Form Builder Pro must also be installed and active in order for the Notifications add-on to function properly.' , 'visual-form-builder-pro' ) );

		if ( !isset( $_REQUEST['action'] ) )
			return;

		switch( $_REQUEST['action'] ) :
			case 'save-notifications' :
				echo sprintf( '<div id="message" class="updated"><p>%s</p></div>', __( 'Notification settings saved.' , 'vfb-pro-notifications' ) );
				break;
		endswitch;
	}

	/**
	 * Admin menu
	 *
	 * Adds the Notifications to VFB Pro menu
	 *
	 * @since 1.0
	 * @access public
	 */
	public function admin_menu() {
		$current_page = add_submenu_page( 'visual-form-builder-pro', __( 'Notifications', 'vfb-pro-notifications' ), __( 'Notifications', 'vfb-pro-notifications' ), 'vfb_edit_entries', 'vfb-notifications', array( &$this, 'admin' ) );

		// Load admin scripts
		add_action( 'load-' . $current_page, array( &$this, 'admin_scripts' ) );
	}

	/**
	 * Display settings page
	 *
	 *
	 * @since 1.0
	 */
	public function admin() {
		global $wpdb;

		$order 		= sanitize_sql_orderby( 'form_id ASC' );
		$forms 		= $wpdb->get_results( "SELECT * FROM $this->form_table_name ORDER BY $order" );

		// Die if no existing form is selected
		if ( !$forms ) :
			echo '<div class="vfb-form-alpha-list"><h3 id="vfb-no-forms">You currently do not have any forms.  Click <a href="' . esc_url( admin_url( 'admin.php?page=vfb-add-new' ) ) . '">here to get started</a>.</h3></div>';

		else :
			$form_selected_id = ( isset( $_REQUEST['form_id'] ) ) ? (int) $_REQUEST['form_id'] : $forms[0]->form_id;
			$form 		= $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $this->form_table_name WHERE form_id = %d", $form_selected_id ) );

			$settings  = get_option( 'vfb-notifications-' . $form_selected_id );
			$mailchimp = get_option( 'vfb-notifications-mailchimp-' . $form_selected_id );
			$campaign_monitor_clients = get_option( 'vfb-notifications-campaign-monitor-clients-' . $form_selected_id );
			$campaign_monitor_lists   = get_option( 'vfb-notifications-campaign-monitor-lists-' . $form_selected_id );
			$highrise = get_option( 'vfb-notifications-highrise-' . $form_selected_id );
			$freshbooks = get_option( 'vfb-notifications-freshbooks-' . $form_selected_id );

			$defaults = array(
				'cell-enable'               => '',
				'cell-phone'                => '',
				'cell-carrier'              => '',

				'mailchimp-enable'          => '',
				'mailchimp-api'             => '',

				'campaignmonitor-enable'    => '',
				'campaignmonitor-api'       => '',

				'highrise-enable'           => '',
				'highrise-subdomain'        => '',
				'highrise-api'              => '',

				'freshbooks-enable'           => '',
				'freshbooks-subdomain'        => '',
				'freshbooks-api'              => '',
			);

			foreach ( $defaults as $key => $value ) {
				$settings[ $key ] = isset( $settings[ $key ] ) ? $settings[ $key ] : '';
			}
?>
	<div class="wrap">
		<?php screen_icon( 'options-general' ); ?>
		<h2><?php _e( 'Notifications', '' ); ?></h2>
        <form method="post" id="notifications-switcher">
            <label for="form_id"><strong><?php _e( 'Select form:', 'vfb-pro-notifications' ); ?></strong></label>
            <select name="form_id" id="form_id">
<?php
		foreach ( $forms as $form ) :
			echo sprintf( '<option value="%1$d" %2$s id="%3$s">%4$s</option>',
				$form->form_id,
				selected( $form->form_id, $form_selected_id, 0 ),
				$form->form_key,
				stripslashes( $form->form_title )
			);
		endforeach;
?>
		</select>
        <?php submit_button( __( 'Select', 'vfb-pro-notifications' ), 'secondary', 'submit', false ); ?>
        </form>

        <form id="vfb-pro-notifications-form" method="post">
			<input name="action" type="hidden" value="save-notifications" />
			<input name="form_id" type="hidden" value="<?php echo $form_selected_id; ?>" />
			<?php wp_nonce_field( 'save-notifications' ); ?>

			<!-- Mobile Device -->
			<h3><?php _e( 'Mobile Device' , 'vfb-pro-notifications'); ?></h3>
			<table class="form-table">
				<tbody>
					<tr align="top">
						<th>
							<label for="vfb-cell-enable">
								<?php _e( 'Enable Mobile Device', 'vfb-pro-notifications' ); ?>
							</label>
						</th>
						<td>
							<input type="checkbox" class="vfb-enable-notify" id="vfb-cell-enable" name="vfb-notify[cell-enable]" value="1"<?php checked( $settings['cell-enable'], 1 ); ?> />
						</td>
					</tr>
				</tbody>
			</table>
			<table class="form-table vfb-cell-enable-wrapper vfb-notify-enable-<?php echo $settings['cell-enable'] ? 'show' : 'hide'; ?>">
				<tbody>
					<tr align="top">
						<th>
							<label for="vfb-notify-cell-phone" class="">
								<?php _e( 'Your Cell Phone Number' , 'vfb-pro-notifications'); ?>
							</label>
						</th>
						<td>
							<input type="text" class="regular-text" id="vfb-notify-cell-phone" name="vfb-notify[cell-phone]" value="<?php echo esc_html( $settings['cell-phone'] ); ?>" />
						</td>
					</tr>

					<tr align="top">
						<th>
							<label for="vfb-notify-cell-carrier" class="">
								<?php _e( 'Your Carrier' , 'vfb-pro-notifications'); ?>
							</label>
						</th>
						<td>
							<select id="vfb-notify-cell-carrier" name="vfb-notify[cell-carrier]">
								<option value=""<?php selected( $settings['cell-carrier'], '' ); ?>></option>
								<option value="Alltel"<?php selected( $settings['cell-carrier'], 'Alltel' ); ?>>Alltel</option>
								<option value="ATT-iPhone"<?php selected( $settings['cell-carrier'], 'ATT-iPhone' ); ?>>AT&T (iPhone)</option>
								<option value="ATT"<?php selected( $settings['cell-carrier'], 'ATT' ); ?>>AT&T</option>
								<option value="BellAtlantic"<?php selected( $settings['cell-carrier'], 'BellAtlantic' ); ?>>Bell Atlantic (message.bam.com)</option>
								<option value="BellCanada"<?php selected( $settings['cell-carrier'], 'BellCanada' ); ?>>Bell Canada (bellmobility.ca)</option>
								<option value="BellMobilityCanada"<?php selected( $settings['cell-carrier'], 'BellMobilityCanada' ); ?>>Bell Mobility (Canada - txt.bell.ca)</option>
								<option value="BellMobility"<?php selected( $settings['cell-carrier'], 'BellMobility' ); ?>>Bell Mobility (txt.bellmobility.ca)</option>
								<option value="Boost"<?php selected( $settings['cell-carrier'], 'Boost' ); ?>>Boost</option>
								<option value="CingularMe"<?php selected( $settings['cell-carrier'], 'CingularMe' ); ?>>Cingular (cingularme.com)</option>
								<option value="Cingular"<?php selected( $settings['cell-carrier'], 'Cingular' ); ?>>Cingular (mobile.mycingular.com)</option>
								<option value="Nextel"<?php selected( $settings['cell-carrier'], 'Nextel' ); ?>>Nextel</option>
								<option value="Rogers"<?php selected( $settings['cell-carrier'], 'Rogers' ); ?>>Rogers</option>
								<option value="Sprint"<?php selected( $settings['cell-carrier'], 'Sprint' ); ?>>Sprint</option>
								<option value="T-Mobile"<?php selected( $settings['cell-carrier'], 'T-Mobile' ); ?>>T-Mobile</option>
								<option value="Telus"<?php selected( $settings['cell-carrier'], 'Telus' ); ?>>Telus</option>
								<option value="Verizon"<?php selected( $settings['cell-carrier'], 'Verizon' ); ?>>Verizon</option>
								<option value="VirginMobile"<?php selected( $settings['cell-carrier'], 'VirginMobile' ); ?>>Virgin Mobile (vmobl.com)</option>
								<option value="VirginMobileCA"<?php selected( $settings['cell-carrier'], 'VirginMobileCA' ); ?>>Virgin Mobile CA (vmobile.ca)</option>
								<option value="Vodafone UK"<?php selected( $settings['cell-carrier'], 'Vodafone UK' ); ?>>Vodafone UK</option>
								<option value="Other"<?php selected( $settings['cell-carrier'], 'Other' ); ?>>Other</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>

			<!-- MailChimp -->
			<h3><?php _e( 'MailChimp' , 'vfb-pro-notifications'); ?></h3>
			<table class="form-table">
				<tbody>
					<tr align="top">
						<th>
							<label for="vfb-mailchimp-enable">
								<?php _e( 'Enable MailChimp', 'vfb-pro-notifications' ); ?>
							</label>
						</th>
						<td>
							<input type="checkbox" class="vfb-enable-notify" id="vfb-mailchimp-enable" name="vfb-notify[mailchimp-enable]" value="1"<?php checked( $settings['mailchimp-enable'], 1 ); ?> />
						</td>
					</tr>
				</tbody>
			</table>
			<table class="form-table vfb-mailchimp-enable-wrapper vfb-notify-enable-<?php echo $settings['mailchimp-enable'] ? 'show' : 'hide'; ?>">
				<tbody>
					<tr align="top" id="vfb-mailchimp-details">
						<th>
							<label for="vfb-notify-mailchimp-api-key" class="">
								<?php _e( 'Your MailChimp API key' , 'vfb-pro-notifications'); ?>
							</label>
						</th>
						<td>
							<input type="text" class="regular-text" id="vfb-notify-mailchimp-api-key" name="vfb-notify[mailchimp-api]" value="<?php echo esc_html( $settings['mailchimp-api'] ); ?>" />
							<a href="#" class="button" id="vfb-notify-mailchimp-api-key-check">
								<?php _e( 'Connect Account', 'vfb-pro-notifications' ); ?><img alt="" src="<?php echo admin_url( '/images/wpspin_light.gif' ); ?>" class="waiting spinner" />
							</a>
							<a href="#" class="vfb-notify-remove-connection vfb-notify-remove-<?php echo $settings['mailchimp-api'] ? 'show' : 'hide'; ?>"><?php _e( 'Remove Connection', 'vfb-pro-notifications' ); ?></a>
						</td>
					</tr>
					<?php
					if ( $mailchimp && isset( $settings['mailchimp-api'] ) && !empty( $settings['mailchimp-api'] ) )
						$this->mailchimp_admin_output( $mailchimp, $form_selected_id );
					?>
				</tbody>
			</table>

			<!-- Campaign Monitor -->
			<h3><?php _e( 'Campaign Monitor' , 'vfb-pro-notifications'); ?></h3>
			<table class="form-table">
				<tbody>
					<tr align="top">
						<th>
							<label for="vfb-campaignmonitor-enable">
								<?php _e( 'Enable Campaign Monitor', 'vfb-pro-notifications' ); ?>
							</label>
						</th>
						<td>
							<input type="checkbox" class="vfb-enable-notify" id="vfb-campaignmonitor-enable" name="vfb-notify[campaignmonitor-enable]" value="1"<?php checked( $settings['campaignmonitor-enable'], 1 ); ?> />
						</td>
					</tr>
				</tbody>
			</table>
			<table class="form-table vfb-campaignmonitor-enable-wrapper vfb-notify-enable-<?php echo $settings['campaignmonitor-enable'] ? 'show' : 'hide'; ?>">
				<tbody>
					<tr align="top" id="vfb-campaign-monitor-details">
						<th>
							<label for="vfb-notify-campaign-monitor-api-key" class="">
								<?php _e( 'Your Campaign Monitor API key' , 'vfb-pro-notifications'); ?>
							</label>
						</th>
						<td>
							<input type="text" class="regular-text" id="vfb-notify-campaign-monitor-api-key" name="vfb-notify[campaignmonitor-api]" value="<?php echo esc_html( $settings['campaignmonitor-api'] ); ?>" />
							<a href="#" class="button" id="vfb-notify-campaign-monitor-api-key-check">
								<?php _e( 'Connect Account', 'vfb-pro-notifications' ); ?><img alt="" src="<?php echo admin_url( '/images/wpspin_light.gif' ); ?>" class="waiting spinner" />
							</a>
							<a href="#" class="vfb-notify-remove-connection vfb-notify-remove-<?php echo $settings['campaignmonitor-api'] ? 'show' : 'hide'; ?>"><?php _e( 'Remove Connection', 'vfb-pro-notifications' ); ?></a>
						</td>
					</tr>
					<?php
					if ( $campaign_monitor_clients && isset( $settings['campaignmonitor-api'] ) && !empty( $settings['campaignmonitor-api'] ) )
						$this->campaign_monitor_admin_client_output( $campaign_monitor_clients, $form_selected_id );

					if ( $campaign_monitor_lists && isset( $settings['campaignmonitor-api'] ) && !empty( $settings['campaignmonitor-api'] ) )
						$this->campaign_monitor_admin_output( $campaign_monitor_lists, $form_selected_id );
					?>
				</tbody>
			</table>

			<!-- Highrise -->
			<h3><?php _e( 'Highrise' , 'vfb-pro-notifications'); ?></h3>
			<table class="form-table">
				<tbody>
					<tr align="top">
						<th>
							<label for="vfb-highrise-enable">
								<?php _e( 'Enable Highrise', 'vfb-pro-notifications' ); ?>
							</label>
						</th>
						<td>
							<input type="checkbox" class="vfb-enable-notify" id="vfb-highrise-enable" name="vfb-notify[highrise-enable]" value="1"<?php checked( $settings['highrise-enable'], 1 ); ?> />
						</td>
					</tr>
				</tbody>
			</table>
			<table class="form-table vfb-highrise-enable-wrapper vfb-notify-enable-<?php echo $settings['highrise-enable'] ? 'show' : 'hide'; ?>">
				<tbody>
					<tr>
						<th>
							<label for="vfb-notify-highrise-subdomain" class="">
								<?php _e( 'Your Highrise Subdomain' , 'vfb-pro-notifications'); ?>
							</label>
						</th>
						<td>
							<input type="text" class="regular-text" id="vfb-notify-highrise-subdomain" name="vfb-notify[highrise-subdomain]" value="<?php echo esc_html( $settings['highrise-subdomain'] ); ?>" />
						</td>
					</tr>
					<tr align="top" id="vfb-highrise-details">
						<th>
							<label for="vfb-notify-highrise-api-key" class="">
								<?php _e( 'Your Highrise API key' , 'vfb-pro-notifications'); ?>
							</label>
						</th>
						<td>
							<input type="text" class="regular-text" id="vfb-notify-highrise-api-key" name="vfb-notify[highrise-api]" value="<?php echo esc_html( $settings['highrise-api'] ); ?>" />
							<a href="#" class="button" id="vfb-notify-highrise-api-key-check">
								<?php _e( 'Connect Account', 'vfb-pro-notifications' ); ?><img alt="" src="<?php echo admin_url( '/images/wpspin_light.gif' ); ?>" class="waiting spinner" />
							</a>
							<a href="#" class="vfb-notify-remove-connection vfb-notify-remove-<?php echo $settings['highrise-api'] ? 'show' : 'hide'; ?>"><?php _e( 'Remove Connection', 'vfb-pro-notifications' ); ?></a>
						</td>
					</tr>
					<?php
					if ( $highrise && isset( $settings['highrise-api'] ) && !empty( $settings['highrise-api'] ) )
						$this->highrise_admin_output( $form_selected_id );
					?>
				</tbody>
			</table>

			<!-- FreshBooks -->
			<h3><?php _e( 'FreshBooks' , 'vfb-pro-notifications'); ?></h3>
			<table class="form-table">
				<tbody>
					<tr align="top">
						<th>
							<label for="vfb-freshbooks-enable">
								<?php _e( 'Enable FreshBooks', 'vfb-pro-notifications' ); ?>
							</label>
						</th>
						<td>
							<input type="checkbox" class="vfb-enable-notify" id="vfb-freshbooks-enable" name="vfb-notify[freshbooks-enable]" value="1"<?php checked( $settings['freshbooks-enable'], 1 ); ?> />
						</td>
					</tr>
				</tbody>
			</table>
			<table class="form-table vfb-freshbooks-enable-wrapper vfb-notify-enable-<?php echo $settings['freshbooks-enable'] ? 'show' : 'hide'; ?>">
				<tbody>
					<tr>
						<th>
							<label for="vfb-notify-freshbooks-subdomain" class="">
								<?php _e( 'Your FreshBooks Subdomain' , 'vfb-pro-notifications'); ?>
							</label>
						</th>
						<td>
							<input type="text" class="regular-text" id="vfb-notify-freshbooks-subdomain" name="vfb-notify[freshbooks-subdomain]" value="<?php echo esc_html( $settings['freshbooks-subdomain'] ); ?>" />
						</td>
					</tr>
					<tr align="top" id="vfb-freshbooks-details">
						<th>
							<label for="vfb-notify-freshbooks-api-key" class="">
								<?php _e( 'Your FreshBooks API key' , 'vfb-pro-notifications'); ?>
							</label>
						</th>
						<td>
							<input type="text" class="regular-text" id="vfb-notify-freshbooks-api-key" name="vfb-notify[freshbooks-api]" value="<?php echo esc_html( $settings['freshbooks-api'] ); ?>" />
							<a href="#" class="button" id="vfb-notify-freshbooks-api-key-check">
								<?php _e( 'Connect Account', 'vfb-pro-notifications' ); ?><img alt="" src="<?php echo admin_url( '/images/wpspin_light.gif' ); ?>" class="waiting spinner" />
							</a>
							<a href="#" class="vfb-notify-remove-connection vfb-notify-remove-<?php echo $settings['freshbooks-api'] ? 'show' : 'hide'; ?>"><?php _e( 'Remove Connection', 'vfb-pro-notifications' ); ?></a>
						</td>
					</tr>
					<?php
					if ( $freshbooks && isset( $settings['freshbooks-api'] ) && !empty( $settings['freshbooks-api'] ) )
						$this->freshbooks_admin_output( $form_selected_id );
					?>
				</tbody>
			</table>

			<?php submit_button( __( 'Save', 'vfb-pro-notifications' ), 'primary', 'vfb-notify-submit' ); ?>
        </form>
        <?php endif; ?>
	</div>
<?php
	}

	/**
	 * Save settings in database
	 *
	 * @since 1.0
	 */
	public function save() {
		global $wpdb;

		if ( !isset( $_REQUEST['vfb-notify-submit'] ) )
			return;

		if ( !wp_verify_nonce( $_REQUEST['_wpnonce'], 'save-notifications' ) )
			wp_die( 'Security Check' );

		$form_id = absint( $_POST['form_id'] );
		$data = array();

		foreach ( $_POST['vfb-notify'] as $key => $val ) {
			$data[ $key ] = esc_html( $val );
		}

		update_option( 'vfb-notifications-' . $form_id, $data );
	}

	/**
	 * notify_cell_phone function.
	 *
	 * @access public
	 * @return void
	 */
	public function notify_cell_phone( $form_id, $entry_id ) {
		global $wpdb;

		if ( !isset( $_REQUEST['vfb-submit'] ) )
			return;

		$settings = get_option( 'vfb-notifications-' . $form_id );

		// If no notification settings for this form, exit
		if ( !$settings )
			return;

		// If Enable Mobile isn't checked, exit
		if ( !isset( $settings['cell-enable'] ) || empty( $settings['cell-enable'] ) )
			return;

		// If no cell phone found for this form, exit
		if ( !isset( $settings['cell-phone'] ) || empty( $settings['cell-phone'] ) )
			return;

		// If no cell carrier assigned for this form, exit
		if ( !isset( $settings['cell-carrier'] ) || empty( $settings['cell-carrier'] ) )
			return;

		// Remove most dashes, whitespace, and other characters
		$phone = trim( str_replace( array( '-', '(', ')', ',' ), '', $settings['cell-phone'] ) );

		// Allow cell phone to be filtered
		$phone = apply_filters( 'vfb_notifications_cell_phone', $phone, $form_id, $entry_id );

		$carrier = '';

		switch ( $settings['cell-carrier'] ) :

			case 'Alltel' :
				$carrier = 'sms.alltelwireless.com';
				break;

			case 'ATT-iPhone' :
				$carrier = 'mms.att.net';
				break;

			case 'ATT' :
				$carrier = 'txt.att.net';
				break;

			case 'BellAtlantic' :
				$carrier = 'message.bam.com';
				break;

			case 'BellCanada' :
				$carrier = 'txt.bellmobility.ca';
				break;

			case 'BellMobilityCanada' :
				$carrier = 'txt.bell.ca';
				break;

			case 'BellMobility' :
				$carrier = 'txt.bellmobility.ca';
				break;

			case 'Boost' :
				$carrier = 'sms.myboostmobile.com';
				break;

			case 'CingularMe' :
				$carrier = 'cingularme.com';
				break;

			case 'Cingular' :
				$carrier = 'mobile.mycingular.com';
				break;

			case 'Nextel' :
				$carrier = 'messaging.nextel.com';
				break;

			case 'Rogers' :
				$carrier = 'sms.rogers.com';
				break;

			case 'Sprint' :
				$carrier = 'messaging.sprintpcs.com';
				break;

			case 'T-Mobile' :
				$carrier = 'tmomail.net';
				break;

			case 'Telus' :
				$carrier = 'msg.telus.com';
				break;

			case 'Verizon' :
				$carrier = 'vtext.com';
				break;

			case 'VirginMoblie' :
				$carrier = 'vmobl.com';
				break;

			case 'VirginMoblieCA' :
				$carrier = 'vmobile.ca';
				break;

			case 'Vodafone UK' :
				$carrier = 'vodafone.net';
				break;

			case 'Other' :
				$carrier = 'other';
				break;

		endswitch;

		// Make sure a carrier was assigned before proceeding
		if ( empty( $carrier ) )
			return;

		// Allow carrier to be filtered
		$carrier = apply_filters( 'vfb_notifications_cell_carrier', $carrier, $form_id, $entry_id );

		$form_title = $wpdb->get_var( $wpdb->prepare( "SELECT form_title FROM $this->form_table_name WHERE form_id = %d", $form_id ) );

		$txt      = 'other' == $carrier ? $phone : "{$phone}@{$carrier}";
		$subject  = "VFB Notification [#{$entry_id}]";
		$message  = "New entry for {$form_title}";

		// Allow cell subject to be filtered
		$subject = apply_filters( 'vfb_notifications_cell_subject', $subject, $form_id, $entry_id );

		// Allow cell message to be filtered
		$message = apply_filters( 'vfb_notifications_cell_message', $message, $form_id, $entry_id );

		wp_mail( $txt, $subject, $message );

		return;
	}

	/**
	 * notify_mailchimp function.
	 *
	 * @access public
	 * @param mixed $form_id
	 * @param mixed $entry_id
	 * @return void
	 */
	public function notify_mailchimp( $form_id, $entry_id ) {
		global $wpdb;

		if ( !isset( $_REQUEST['vfb-submit'] ) )
			return;

		$settings = get_option( 'vfb-notifications-' . $form_id );

		// If no notification settings for this form, exit
		if ( !$settings )
			return;

		// If Enable MailChimp isn't checked, exit
		if ( !isset( $settings['mailchimp-enable'] ) || empty( $settings['mailchimp-enable'] ) )
			return;

		// If no MailChimp API key, exit
		if ( !isset( $settings['mailchimp-api'] ) || empty( $settings['mailchimp-api'] ) )
			return;

		// If no MailChimp list selected, exit
		if ( !isset( $settings['mailchimp-list'] ) || empty( $settings['mailchimp-list'] ) )
			return;

		// If no email field selected, exit
		if ( !isset( $settings['mailchimp-merge-email'] ) || empty( $settings['mailchimp-merge-email'] ) )
			return;

		$api_key  = $settings['mailchimp-api'];
		$list     = $settings['mailchimp-list'];
		$optin    = isset( $settings['mailchimp-opt-in'] ) ? $settings['mailchimp-opt-in'] : false;
		$email    = isset( $settings['mailchimp-merge-email'] ) ? $settings['mailchimp-merge-email'] : '';
		$fname    = isset( $settings['mailchimp-merge-fname'] ) ? $settings['mailchimp-merge-fname'] : '';
		$lname    = isset( $settings['mailchimp-merge-lname'] ) ? $settings['mailchimp-merge-lname'] : '';
		$company  = isset( $settings['mailchimp-merge-company'] ) ? $settings['mailchimp-merge-company'] : '';
		$phone    = isset( $settings['mailchimp-merge-phone'] ) ? $settings['mailchimp-merge-phone'] : '';

		$chimp_email = $merge_tags = array();

		// Email Address
		if ( !empty( $email ) && isset( $_POST['vfb-' . $email ] ) )
			$chimp_email = array( 'email' => esc_html( $_POST['vfb-' . $email ] ) );

		// First Name
		if ( !empty( $fname ) && isset( $_POST['vfb-' . $fname ] ) )
			$merge_tags['FNAME'] = esc_html( $_POST['vfb-' . $fname ] );

		// Last Name
		if ( !empty( $lname ) && isset( $_POST['vfb-' . $lname ] ) )
			$merge_tags['LNAME'] = esc_html( $_POST['vfb-' . $lname ] );

		// Company
		if ( !empty( $company ) && isset( $_POST['vfb-' . $company ] ) )
			$merge_tags['COMPANY'] = esc_html( $_POST['vfb-' . $company ] );

		// Phone
		if ( !empty( $phone ) && isset( $_POST['vfb-' . $phone ] ) )
			$merge_tags['PHONE'] = esc_html( $_POST['vfb-' . $phone ] );

		list(, $datacentre) = explode( '-', $api_key );
		$api_endpoint = str_replace( '<dc>', $datacentre, 'https://<dc>.api.mailchimp.com/2.0/' );

		$args = array(
			'apikey'         => $api_key,
			'id'             => $list,
			'email'			 => $chimp_email,
			'merge_vars'     => $merge_tags,
			'double_optin'   => $optin,
		);

		$request = wp_remote_post( "$api_endpoint/lists/subscribe.json", array( 'body' => json_encode( $args ) ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	)
			return false;

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		return;
	}

	/**
	 * notify_campaign_monitor function.
	 *
	 * @access public
	 * @param mixed $form_id
	 * @param mixed $entry_id
	 * @return void
	 */
	public function notify_campaign_monitor( $form_id, $entry_id ) {
		global $wpdb;

		if ( !isset( $_REQUEST['vfb-submit'] ) )
			return;

		$settings = get_option( 'vfb-notifications-' . $form_id );

		// If no notification settings for this form, exit
		if ( !$settings )
			return;

		// If Enable Campaign Monitor isn't checked, exit
		if ( !isset( $settings['campaignmonitor-enable'] ) || empty( $settings['campaignmonitor-enable'] ) )
			return;

		// If no Campaign Monitor API key, exit
		if ( !isset( $settings['campaignmonitor-api'] ) || empty( $settings['campaignmonitor-api'] ) )
			return;

		// If no Campaign Monitor client selected, exit
		if ( !isset( $settings['campaign-monitor-clientIDs'] ) || empty( $settings['campaign-monitor-clientIDs'] ) )
			return;

		// If no Campaign Monitor list selected, exit
		if ( !isset( $settings['campaign-monitor-listIDs'] ) || empty( $settings['campaign-monitor-listIDs'] ) )
			return;

		// If no email field selected, exit
		if ( !isset( $settings['campaign-monitor-merge-email'] ) || empty( $settings['campaign-monitor-merge-email'] ) )
			return;

		$api_key  = $settings['campaignmonitor-api'];
		$list     = $settings['campaign-monitor-listIDs'];
		$email    = isset( $settings['campaign-monitor-merge-email'] ) ? $settings['campaign-monitor-merge-email'] : '';
		$name     = isset( $settings['campaign-monitor-merge-name'] ) ? $settings['campaign-monitor-merge-name'] : '';

		$campaign_email = $campaign_name = '';

		// Email Address
		if ( !empty( $email ) && isset( $_POST['vfb-' . $email ] ) )
			$campaign_email = esc_html( $_POST['vfb-' . $email ] );

		// Name
		if ( !empty( $name ) && isset( $_POST['vfb-' . $name ] ) ) {
			$value = $_POST['vfb-' . $name ];
			$campaign_fname = $this->field_part( $value, 'name', 'first' );
			$campaign_lname = $this->field_part( $value, 'name', 'last' );

			$campaign_name = "$campaign_fname $campaign_lname";
		}

		$args = array(
			'EmailAddress' => $campaign_email,
			'Name'         => $campaign_name,
		);

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' )
		);

		$request = wp_remote_post( "https://api.createsend.com/api/v3/subscribers/{$list}.json", array( 'headers' => $headers, 'body' => json_encode( $args ) ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	)
			return false;

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		return;
	}

	/**
	 * notify_highrise function.
	 *
	 * @access public
	 * @param mixed $form_id
	 * @param mixed $entry_id
	 * @return void
	 */
	public function notify_highrise( $form_id, $entry_id ) {
		$settings = get_option( 'vfb-notifications-' . $form_id );

		// If no notification settings for this form, exit
		if ( !$settings )
			return;

		// If Enable Highrise isn't checked, exit
		if ( !isset( $settings['highrise-enable'] ) || empty( $settings['highrise-enable'] ) )
			return;

		// If no Highrise API key, exit
		if ( !isset( $settings['highrise-api'] ) || empty( $settings['highrise-api'] ) )
			return;

		// If no Highrise subdomain, exit
		if ( !isset( $settings['highrise-subdomain'] ) || empty( $settings['highrise-subdomain'] ) )
			return;

		// If no name field set, exit
		if ( !isset( $settings['highrise-merge-name'] ) || empty( $settings['highrise-merge-name'] ) )
			return;

		$api_key  = $settings['highrise-api'];
		$subdomain = $settings['highrise-subdomain'];
		$name     = isset( $settings['highrise-merge-name'] ) ? $settings['highrise-merge-name'] : '';
		$email    = isset( $settings['highrise-merge-email'] ) ? $settings['highrise-merge-email'] : '';
		$company  = isset( $settings['highrise-merge-company'] ) ? $settings['highrise-merge-company'] : '';
		$phone    = isset( $settings['highrise-merge-phone'] ) ? $settings['highrise-merge-phone'] : '';
		$notes	  = isset( $settings['highrise-note'] ) ? $settings['highrise-note'] : '';

		$highrise_fname = $highrise_lname = $highrise_company = $highrise_email = $highrise_phone = '';

		// Name
		if ( !empty( $name ) && isset( $_POST['vfb-' . $name ] ) ) {
			$value = $_POST['vfb-' . $name ];
			$highrise_fname = $this->field_part( $value, 'name', 'first' );
			$highrise_lname = $this->field_part( $value, 'name', 'last' );
		}

		// Email Address
		if ( !empty( $email ) && isset( $_POST['vfb-' . $email ] ) )
			$highrise_email = esc_html( $_POST['vfb-' . $email ] );

		// Company
		if ( !empty( $company ) && isset( $_POST['vfb-' . $company ] ) )
			$highrise_company = esc_html( $_POST['vfb-' . $company ] );

		// Phone
		if ( !empty( $phone ) && isset( $_POST['vfb-' . $phone ] ) )
			$highrise_phone = esc_html( $_POST['vfb-' . $phone ] );

		$contact = sprintf(
		'<person>
			<first-name>%1$s</first-name>
			<last-name>%2$s</last-name>
			<company-name>%3$s</company-name>
			<contact-data>
				<email-addresses>
					<email-address>
						<address>%4$s</address>
						<location>Work</location>
					</email-address>
				</email-addresses>
				<phone-numbers>
					<phone-number>
						<number>%5$s</number>
						<location>Work</location>
					</phone-number>
				</phone-numbers>
			</contact-data>
		</person>',
		$highrise_fname,
		$highrise_lname,
		$highrise_company,
		$highrise_email,
		$highrise_phone
		);

		$args = array(
			'apikey'	=> $api_key,
		);

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' ),
			'Content-Type' => 'application/xml',
		);

		$request = wp_remote_post( "https://{$subdomain}.highrisehq.com/people.xml", array( 'body' => $contact, 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) )
			return false;

		// If no Notes, stop
		if ( empty( $notes ) )
			return;

		// Retrieve and set response
		$response = simplexml_load_string( wp_remote_retrieve_body( $request ) );

		// Get the newly created Contact ID
		$person_id = !empty( $response->id ) ? (float) $response->id : false;

		// If no Contact ID, stop
		if ( !$person_id )
			return;

		$highrise_note = empty( $notes ) ? __( 'Inserted from Visual Form Builder Pro form.', 'vfb-pro-notifications' ) : $notes;

		$note = sprintf(
		'<note>
			<body>%1$s</body>
			<subject-id type="integer">%2$d</subject-id>
			<subject-type>Party</subject-type>
		</note>',
		$highrise_note,
		$person_id
		);

		$request = wp_remote_post( "https://{$subdomain}.highrisehq.com/notes.xml", array( 'body' => $note, 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) )
			return false;

		return;
	}

	/**
	 * notify_freshbooks function.
	 *
	 * @access public
	 * @param mixed $form_id
	 * @param mixed $entry_id
	 * @return void
	 */
	public function notify_freshbooks( $form_id, $entry_id ) {
		$settings = get_option( 'vfb-notifications-' . $form_id );

		// If no notification settings for this form, exit
		if ( !$settings )
			return;

		// If Enable Highrise isn't checked, exit
		if ( !isset( $settings['freshbooks-enable'] ) || empty( $settings['freshbooks-enable'] ) )
			return;

		// If no Highrise API key, exit
		if ( !isset( $settings['freshbooks-api'] ) || empty( $settings['freshbooks-api'] ) )
			return;

		// If no Highrise subdomain, exit
		if ( !isset( $settings['freshbooks-subdomain'] ) || empty( $settings['freshbooks-subdomain'] ) )
			return;

		// If no name field set, exit
		if ( !isset( $settings['freshbooks-merge-name'] ) || empty( $settings['freshbooks-merge-name'] ) )
			return;

		$api_key  = $settings['freshbooks-api'];
		$subdomain = $settings['freshbooks-subdomain'];
		$name     = isset( $settings['freshbooks-merge-name'] ) ? $settings['freshbooks-merge-name'] : '';
		$email    = isset( $settings['freshbooks-merge-email'] ) ? $settings['freshbooks-merge-email'] : '';
		$company  = isset( $settings['freshbooks-merge-company'] ) ? $settings['freshbooks-merge-company'] : '';
		$phone    = isset( $settings['freshbooks-merge-phone'] ) ? $settings['freshbooks-merge-phone'] : '';
		$notes	  = isset( $settings['freshbooks-note'] ) ? $settings['freshbooks-note'] : '';

		$freshbooks_fname = $freshbooks_lname = $freshbooks_company = $freshbooks_email = $freshbooks_phone = '';

		// Name
		if ( !empty( $name ) && isset( $_POST['vfb-' . $name ] ) ) {
			$value = $_POST['vfb-' . $name ];
			$freshbooks_fname = $this->field_part( $value, 'name', 'first' );
			$freshbooks_lname = $this->field_part( $value, 'name', 'last' );
		}

		// Email Address
		if ( !empty( $email ) && isset( $_POST['vfb-' . $email ] ) )
			$freshbooks_email = esc_html( $_POST['vfb-' . $email ] );

		// Company
		if ( !empty( $company ) && isset( $_POST['vfb-' . $company ] ) )
			$freshbooks_company = esc_html( $_POST['vfb-' . $company ] );

		// Phone
		if ( !empty( $phone ) && isset( $_POST['vfb-' . $phone ] ) )
			$freshbooks_phone = esc_html( $_POST['vfb-' . $phone ] );

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' ),
			'Content-Type' => 'application/xml',
		);

		$client = sprintf(
		'<?xml version="1.0" encoding="utf-8"?>
		<request method="client.create">
			<client>
				<first_name>%1$s</first_name>
			    <last_name>%2$s</last_name>
			    <organization>%3$s</organization>
			    <email>%4$s</email>
			    <work_phone>%5$s</work_phone>
			</client>
		</request>',
		$freshbooks_fname,
		$freshbooks_lname,
		$freshbooks_company,
		$freshbooks_email,
		$freshbooks_phone
		);

		$request = wp_remote_post( "https://{$subdomain}.freshbooks.com/api/2.1/xml-in", array( 'body' => $client, 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) )
			return false;

		return;
	}

	/**
	 * field_part function.
	 *
	 * @access public
	 * @param mixed $value
	 * @param string $type (default: '')
	 * @param string $part (default: '')
	 * @return void
	 */
	public function field_part( $value, $type = '', $part = '' ) {

		$output = '';

		// Basic check for type when not set
		if ( empty( $type ) ) :
			if ( is_array( $value ) && array_key_exists( 'address', $value ) )
				$type = 'address';
			elseif ( is_array( $value ) && array_key_exists( 'first', $value ) && array_key_exists( 'last', $value ) )
				$type = 'name';
			elseif ( is_array( $value ) && array_key_exists( 'hour', $value ) && array_key_exists( 'min', $value ) )
				$type = 'time';
			elseif ( is_array( $value ) )
				$type = 'checkbox';
			else
				$type = 'default';
		endif;

		// Build array'd form item output
		switch( $type ) :

			case 'time' :
				$output = ( array_key_exists( 'ampm', $value ) ) ? substr_replace( implode( ':', $value ), ' ', 5, 1 ) : implode( ':', $value );
				break;

			case 'address' :

				if ( 'address' == $part && !empty( $value['address'] ) )
					return esc_html( $value['address'] );

				if ( 'address-2' == $part && !empty( $value['address-2'] ) )
					return esc_html( $value['address-2'] );

				if ( 'city' == $part && !empty( $value['city'] ) )
					return esc_html( $value['city'] );

				if ( 'state' == $part && !empty( $value['state'] ) )
					return esc_html( $value['state'] );

				if ( 'zip' == $part && !empty( $value['zip'] ) )
					return esc_html( $value['zip'] );

				if ( 'country' == $part && !empty( $value['country'] ) )
					return esc_html( $value['country'] );

				break;

			case 'name' :

				if ( 'first' == $part && !empty( $value['first'] ) )
					return esc_html( $value['first'] );

				if ( 'last' == $part && !empty( $value['last'] ) )
					return esc_html( $value['last'] );

				if ( 'title' == $part && !empty( $value['title'] ) )
					return esc_html( $value['title'] );

				if ( 'suffix' == $part && !empty( $value['suffix'] ) )
					return esc_html( $value['suffix'] );

				break;

			case 'checkbox' :
				$output = esc_html( implode( ', ', $value ) );
				break;

			default :
				$output = wp_specialchars_decode( stripslashes( esc_html( $value ) ), ENT_QUOTES );
				break;

		endswitch;

		return $value;
	}

	/**
	 * fields_options function.
	 *
	 * @access public
	 * @param mixed $fields
	 * @param mixed $key
	 * @return void
	 */
	public function fields_options( $fields, $key ) {
	?>
		<option value=""<?php selected( $key, '' ); ?>></option>
	<?php
		if ( !$fields )
			return false;

		foreach ( $fields as $field ) :
	?>
			<option value="<?php echo esc_attr( $field->field_id ); ?>"<?php selected( $key, $field->field_id ); ?>>
				<?php echo stripslashes( $field->field_name ); ?> (<?php echo ucwords( $field->field_type ); ?>)
			</option>
	<?php
		endforeach;
	}

	/**
	 * get_fields function.
	 *
	 * @access public
	 * @param mixed $form_id
	 * @param string $type (default: '')
	 * @return void
	 */
	public function get_fields( $form_id, $type = '' ) {
		global $wpdb;

		if ( !$form_id )
			return false;

		$where = '';

		if ( !empty( $type ) ) {
			if ( is_array( $type ) ) {
				$types = implode( "','", $type );
				$where = "AND field_type IN( '{$types}' )";
			}
			else
				$where = "AND field_type = '{$type}'";
		}

		$not_in = array(
			'fieldset',
			'section',
			'instructions',
			'verification',
			'secret',
			'submit',
			'file-upload',
			'page-break',
			'checkbox',
			'address',
		);

		$not_in = implode( "','", $not_in );

		$fields = $wpdb->get_results( $wpdb->prepare(
			"SELECT field_id, field_name, field_type
			FROM {$this->field_table_name}
			WHERE form_id = %d
			$where
			AND field_type NOT IN( '{$not_in}' )
			ORDER BY field_sequence ASC",
			$form_id
		) );

		if ( !$fields )
			return false;

		return $fields;
	}

	/**
	 * Delete transients on page load
	 *
	 * FOR TESTING PURPOSES ONLY
	 *
	 * @since 1.0
	 */
	public function delete_transient() {
		delete_site_transient( 'update_plugins' );
	}

	/**
	 * Check the plugin versions to see if there's a new one
	 *
	 * @since 1.0
	 */
	public function api_check( $transient ) {

		// If no checked transiest, just return its value without hacking it
		if ( empty( $transient->checked ) )
			return $transient;

		// Append checked transient information
		$plugin_slug = plugin_basename( __FILE__ );

		// POST data to send to your API
		$args = array(
			'action' 		=> 'update-check',
			'plugin_name' 	=> $plugin_slug,
			'version' 		=> $transient->checked[ $plugin_slug ],
		);

		// Send request checking for an update
		$response = $this->api_request( $args );

		// If response is false, don't alter the transient
		if ( false !== $response )
			$transient->response[ $plugin_slug ] = $response;

		return $transient;
	}

	/**
	 * Send a request to the alternative API, return an object
	 *
	 * @since 1.0
	 */
	public function api_request( $args ) {

		// Send request
		$request = wp_remote_post( $this->api_url, array( 'body' => $args ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	)
			return false;

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		// Read server response, which should be an object
		if ( is_object( $response ) )
			return $response;
		else
			return false;
	}

	/**
	 * Return the plugin details for the plugin update screen
	 *
	 * @since 1.0
	 */
	public function api_information( $false, $action, $args ) {

		$plugin_slug = plugin_basename( __FILE__ );

		// Check if requesting info
		if ( !isset( $args->slug ) )
			return $false;

		// Check if this plugins API is about this plugin
		if ( isset( $args->slug ) && $args->slug != $plugin_slug )
			return $false;

		// POST data to send to your API
		$args = array(
			'action' 		=> 'plugin_information',
			'plugin_name' 	=> $plugin_slug,
		);

		// Send request for detailed information
		$response = $this->api_request( $args );

		// Send request checking for information
		$request = wp_remote_post( $this->api_url, array( 'body' => $args ) );

		return $response;
	}

}