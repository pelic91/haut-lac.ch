=== Visual Form Builder Pro - Notifications ===
Contributors: mmuro
Requires at least: 3.5
Tested up to: 3.6.1
Stable tag: 1.0

Connect your forms with a number of third-party services such as MailChimp and Campaign Monitor after new form submissions.

== Release Notes ==

**Version 1.0 - Oct 01, 2013**

* Plugin launch