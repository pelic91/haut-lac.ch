jQuery(document).ready(function($) {

	$( '#vfb-pro-notifications-form' ).validate();

	// !Show/Hide checkbox functionality
	$( '.vfb-enable-notify' ).change( function(){
		var id = $( this ).attr( 'id' ),
			div = '.' + id + '-wrapper';

		$( div ).toggle();
	});

	// !Remove API Connection
	$( '.vfb-notify-remove-connection' ).click( function(e){
		e.preventDefault();

		$( this ).closest( 'tr' ).nextAll( 'tr' ).fadeOut( function(){ $( this ).remove() });
		$( this ).prevAll( 'input[type=text]' ).val( '' );
	});

	// !MailChimp API check
	$( '#vfb-notify-mailchimp-api-key-check' ).click( function(e){
		e.preventDefault();

		var api_key = $( '#vfb-notify-mailchimp-api-key' ).val(),
			spinner = $( '.spinner', this );

		// Remove existing response
		$( '.vfb-mailchimp-response' ).fadeOut( function(){ $( this ).remove() });

		spinner.show();

		$.get( ajaxurl,
			{
				action: 'vfb_notifications_mailchimp_api',
				form_id: $( 'input[name="form_id"]' ).val(),
				api: api_key
			}
		).done( function( response ) {
			spinner.hide();

			$( '#vfb-mailchimp-details' ).after( response );
		});
	});

	// !Campaign Monitor API check
	$( '#vfb-notify-campaign-monitor-api-key-check' ).click( function(e){
		e.preventDefault();

		var api_key = $( '#vfb-notify-campaign-monitor-api-key' ).val(),
			spinner = $( '.spinner', this );

		// Remove existing response
		$( '.vfb-campaign-monitor-response' ).fadeOut( function(){ $( this ).remove() });

		spinner.show();

		$.get( ajaxurl,
			{
				action: 'vfb_notifications_campaign_monitor_api',
				form_id: $( 'input[name="form_id"]' ).val(),
				api: api_key
			}
		).done( function( response ) {
			spinner.hide();

			$( '#vfb-campaign-monitor-details' ).after( response );
		});
	});

	// !Campaign Monitor get lists
	$( document ).on( 'click', '#vfb-notify-campaign-monitor-select-client', function(e){
		e.preventDefault();

		var api_key = $( '#vfb-notify-campaign-monitor-api-key' ).val(),
			clientIDs = $( '#vfb-notify-campaign-monitor-clientIDs' ).val(),
			spinner = $( '.spinner', this );;

		spinner.show();

		// Remove existing response except for the Select a Client dropdown
		$( '.vfb-campaign-monitor-response:gt(0)' ).fadeOut( function(){ $( this ).remove() });

		$.get( ajaxurl,
			{
				action: 'vfb_notifications_campaign_monitor_get_lists',
				form_id: $( 'input[name="form_id"]' ).val(),
				api: api_key,
				clients: clientIDs
			}
		).done( function( response ) {
			spinner.hide();

			$( '#vfb-campaign-monitor-client-response' ).after( response );
		});
	});

	// !Highrise API check
	$( '#vfb-notify-highrise-api-key-check' ).click( function(e){
		e.preventDefault();

		var subdomain = $( '#vfb-notify-highrise-subdomain' ).val(),
			api_key = $( '#vfb-notify-highrise-api-key' ).val(),
			spinner = $( '.spinner', this );

		// Remove existing response
		$( '.vfb-highrise-response' ).fadeOut( function(){ $( this ).remove() });

		spinner.show();

		$.get( ajaxurl,
			{
				action: 'vfb_notifications_highrise_api',
				form_id: $( 'input[name="form_id"]' ).val(),
				domain: subdomain,
				api: api_key
			}
		).done( function( response ) {
			spinner.hide();

			$( '#vfb-highrise-details' ).after( response );
		});
	});

	// !FreshBooks API check
	$( '#vfb-notify-freshbooks-api-key-check' ).click( function(e){
		e.preventDefault();

		var subdomain = $( '#vfb-notify-freshbooks-subdomain' ).val(),
			api_key = $( '#vfb-notify-freshbooks-api-key' ).val(),
			spinner = $( '.spinner', this );

		// Remove existing response
		$( '.vfb-freshbooks-response' ).fadeOut( function(){ $( this ).remove() });

		spinner.show();

		$.get( ajaxurl,
			{
				action: 'vfb_notifications_freshbooks_api',
				form_id: $( 'input[name="form_id"]' ).val(),
				domain: subdomain,
				api: api_key
			}
		).done( function( response ) {
			spinner.hide();

			$( '#vfb-freshbooks-details' ).after( response );
		});
	});
});