<?php
/*
Plugin Name: Events Widget

*/

// Block direct requests
if (!defined('ABSPATH'))
    die('-1');


add_action('widgets_init', function () {
    register_widget('EventsWidget');
});

/**
 * Adds EventsWidget widget.
 */
class EventsWidget extends WP_Widget
{

    /**
     * Register widget with WordPress.
     */
    function __construct()
    {
        parent::__construct(
            'EventsWidget', // Base ID
            __('Events Widget', 'text_domain'), // Name
            array('description' => __('Events widget!', 'text_domain'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     */
    public function widget()
    {

        global $wpdb;
        $sql
            = 'SELECT DISTINCT p.*, e.post_id, i.id AS instance_id, i.start AS start, i.end AS end, e.allday AS event_allday, e.recurrence_rules, e.exception_rules, e.ticket_url, e.instant_event, e.recurrence_dates, e.exception_dates, e.venue, e.country, e.address, e.city, e.province, e.postal_code, e.show_map, e.contact_name, e.contact_phone, e.contact_email, e.cost, e.ical_feed_url, e.ical_source_url, e.ical_organizer, e.ical_contact, e.ical_uid, e.timezone_name, e.longitude, e.latitude
                FROM wp_ai1ec_events e
                INNER JOIN wp_posts p
                ON e.post_id = p.ID
                LEFT JOIN wp_icl_translations AS translation
                ON ( translation.element_type = \'post_ai1ec_event\'
                AND translation.element_id = p.ID )
                INNER JOIN wp_ai1ec_event_instances i
                ON e.post_id = i.post_id
                LEFT JOIN `wp_term_relationships` AS `term_relationships_events_categories`
                ON ( `e` . `post_id` = `term_relationships_events_categories` . `object_id` )
                LEFT JOIN `wp_term_taxonomy` AS `term_taxonomy_events_categories`
                ON (`term_relationships_events_categories` . `term_taxonomy_id` = `term_taxonomy_events_categories` . `term_taxonomy_id`
                AND `term_taxonomy_events_categories` . taxonomy = \'events_categories\' )WHERE post_type = \'ai1ec_event\'
                AND i.start >= UNIX_TIMESTAMP()
                AND ( translation.translation_id IS NULL
                OR translation.language_code = \'en\' ) AND( term_taxonomy_events_categories.term_id IN ( 83,92 ) )
                AND post_status IN ( \'publish\', \'private\' )
                ORDER BY i.start ASC, post_title ASC
                LIMIT 0, 4';
        $events = $wpdb->get_results($sql);


        foreach ($events as $event):?>
            <?php include('event.php'); ?>
        <?php endforeach;
    }


    // Widget Backend
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <?php
    }

} // class EventsWidget