<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/13/2016
 * Time: 2:36 PM
 */

//print_r($event->ID);exit;
?>
<div class="timely ai1ec-agenda-widget-view ai1ec-clearfix">
    <div>
        <div class="ai1ec-date">
            <a class="ai1ec-date-title ai1ec-load-view" href="<?= $event->guid; ?>" data-type="jsonp">
                <div class="ai1ec-month"><?= gmdate("M", $event->start) ?></div>
                <div class="ai1ec-day"><?= gmdate("d", $event->start) ?></div>
                <div class="ai1ec-weekday"><?= gmdate("D", $event->start) ?></div>
            </a>

            <div class="ai1ec-date-events">
                <div
                    class="ai1ec-event ai1ec-event-id-<?= $event->ID; ?> ai1ec-event-instance-id-<?= $event->instance_id; ?> ai1ec-allday"
                    data-end="<?= gmdate("Y-m-d H:i:s", $event->end); ?>">
                    <div class="ai1ec-event-header">
                        <div class="ai1ec-event-toggle">
                            <i class="ai1ec-fa ai1ec-fa-minus-circle ai1ec-fa-lg"></i>
                            <i class="ai1ec-fa ai1ec-fa-plus-circle ai1ec-fa-lg"></i>
                        </div>
                    <span class="ai1ec-event-title" style="color: #3d6b99; font-weight: bold; font-size: 10.5pt; margin: 0 0 0.4em; -webkit-transition: color 0.1s; transition: color 0.1s;"
                          >
                      <?= $event->post_title; ?>
                    </span>

                        <div class="ai1ec-event-time" style="    font-size: 9pt; font-weight: bold; opacity: 0.8; filter: alpha(opacity=80);">
                            <?= gmdate("M d", $event->start) ?> - <?= gmdate("M d", $event->end) ?> <span
                                class="ai1ec-allday-badge">all-day</span>
                        </div>
                    </div>

                    <div class="ai1ec-event-summary">
                        <div class="ai1ec-event-description">
                        </div>

                        <div class="ai1ec-event-summary-footer">
                            <div class="ai1ec-btn-group ai1ec-actions">
                                <a class="ai1ec-read-more ai1ec-btn ai1ec-btn-default ai1ec-load-event"
                                   href="<?= $event->guid; ?>/?instance_id=<?= $event->instance_id; ?>">
                                    Read more <i class="ai1ec-fa ai1ec-fa-arrow-right"></i>
                                </a>
                            </div>
                    <span class="ai1ec-categories">
					    <span class="ai1ec-field-label">
						    <i class="ai1ec-fa ai1ec-fa-folder-open"></i>
							    Categories:
                        </span>
						<a class=" ai1ec-category ai1ec-term-id-83 p-category"
                           href="http://testhl.haut-lac.net/fr/calendar/cat_ids~83/"> Remarques</a> <a
                            class=" ai1ec-category ai1ec-term-id-82 p-category"
                            href="http://testhl.haut-lac.net/fr/calendar/cat_ids~82/"> This_week</a>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
