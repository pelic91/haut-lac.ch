<?php
/*
 * Plugin Name: gText Widget
 * Version: 1.3
 * Plugin URI: none
 * Description: Multilingual Text Widget For Wordpress 2.8.x 
 * Author: Guram Kajaia, Kevin Filteau
 * Author URI: kevinfilteau.com
 *	Note: This plugins works only with qTranslate plugin (http://www.qianqin.de/qtranslate/)
 *	License: 
    Copyright 2009  Guram Kajaia  (email : guram.kajaia@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2, 
    as published by the Free Software Foundation. 
    
    You may NOT assume that you can use any other version of the GPL.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    The license for this software can likely be found here: 
    http://www.gnu.org/licenses/gpl-2.0.html
 */
class GTextWidget extends WP_Widget
{
	public $gtext_enabled_langs_num; # enabled languages number
	public $gtext_enabled_langs; # enabled languages @array
	function GTextWidget()
	{
		if(function_exists('qtrans_init')) {
			$widget_ops = array('classname' => 'GTextWidget', 'description' => __( "gText Widget") );
			$control_ops = array('width' => 'auto', 'height' => 'auto');
			$this->WP_Widget('gtexttext', __('gText Widget'), $widget_ops, $control_ops);
			$this->gtext_enabled_langs = qtrans_getSortedLanguages(); // get enabled languages
			$this->gtext_enabled_langs_num = count($this->gtext_enabled_langs); // get enabled languages number
		}
	}
	/**
	*	Adds qTranslate's language delimiters to text
	*/
	function gtext_lang_ini($gtext_lang,$gtext_lang_content)
	{
		return "<!--:$gtext_lang-->$gtext_lang_content<!--:-->";
	}
	function widget($args, $instance)
	{
		extract($args);
		$text = empty($instance['text']) ? '' : $instance['text'];		
		echo $before_widget;
		echo $before_title . $instance['lang_title'] . $after_title;	
		echo '<div>' . $instance['lang_text'] .  "</div>";
		echo $after_widget;
	}
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['lang_title'] = ""; # Clear Old Title
		$instance['lang_text'] = ""; # Clear Old Text
		foreach($this->gtext_enabled_langs as $lng) {
			$instance['lang_title'] .= self::gtext_lang_ini($lng,$new_instance[$lng]);
		}
		foreach($this->gtext_enabled_langs as $lng) {
			$instance['lang_text'] .= self::gtext_lang_ini($lng,$new_instance['text_'.$lng]);
		}
		return $instance;
	}
	function form($instance)
	{
		# check if qTranslate installed
		if(!defined("QT_SUPPORTED_WP_VERSION")) {
			echo "You Must Enable/Install Qtranslate To Use This Plugin";
		}
		else
		{
			$instance = wp_parse_args( (array) $instance, array('title'=>'', 'text'=>'') );	
			$title = $instance['title'];
	        $text = $instance['text'];
			$gtext_parsed_title = qtrans_split($instance['lang_title']); # parse qTranslate's lang delimiters from title
			$gtext_parsed_text = qtrans_split($instance['lang_text']); # parse qTranslate's lang delimiters from text
			foreach($this->gtext_enabled_langs as $gtext_lang)
			{
				echo '<p><label for="' . $this->get_field_name($gtext_lang) . '">' . __('Title['.$gtext_lang .']') . '</label><br /><input style="width:400px;margin-left:10px;" id="' . $this->get_field_id($gtext_lang) . '" name="' . $this->get_field_name($gtext_lang) . '" type="text" value="' . $gtext_parsed_title[$gtext_lang] . '" /></p>';
				echo '<p><label for="' . $this->get_field_name("text_".$gtext_lang) . '">' . __('Text['.$gtext_lang.']') . '</label><br /><textarea style="width:400px;height:300px;margin-left:10px;" id="' . $this->get_field_id("text_".$gtext_lang) . '" name="' . $this->get_field_name("text_".$gtext_lang) . '">' . $gtext_parsed_text[$gtext_lang] . '</textarea></p>';
			}
		}
	}
}
function GTextInit() {
	register_widget('GTextWidget');
}
add_action('widgets_init', 'GTextInit');
?>
