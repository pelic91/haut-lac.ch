=== gText Widget ===
Contributors: Guram Kajaia, Kevin Filteau
tags: multilingual, language, text, widget, admin, translation
Requires at least: 2.8.x
Tested up to: 3.9.0
Stable tag: 1.3

This is multilingual text widget, which works with qTranslate plugin.

== Description ==

This is multilingual text widget, which works with qTranslate plugin.
You have to install qTranslate plugin before you use gText Widget.
This plugins requires at least qTranslate 2.0 version.
After installing gText Widget, you will have different input boxes for languages, which you have enabled with qTranslate.

== Installation ==

Installation instructions:

1. Download the plugin from [http://wordpress.org/extend/plugins/gtext-widget/]
2. Extract *.php file to your plugins directory
3. Activate plugin from the 'Plugins' menu in Wordpress.
4. Enjoy :-)

== Frequently Asked Questions ==

Not at this time :-)
