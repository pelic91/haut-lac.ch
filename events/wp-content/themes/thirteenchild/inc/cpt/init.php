<?php
/******************************************************
 *  
 *  @Include CPT Core
 *  @This fle contains class to register new Custom Post Type.
 *  
 ******************************************************/
include( 'cpt-core/cpt-core.php' );
include( 'cpt-sortable/cpt-sortable.php' );

/******************************************************
 *  
 *  @Include CPTs containing CPT registration & other settings
 *  @Each files contains CPT registration code using CPT Core and other settings for the CPTs.
 *  
 ******************************************************/
// include( 'post-types/portfolio.php' );
// include( 'post-types/slider.php' );

$modules_dir =  PG_INC_CHILD . DS . 'cpt' . DS . 'post_type_modules';
$modules_uri =  PG_INC_CHILD_URL . 'cpt' . '/' . 'post_type_modules';

// $module_type = 'post_type';
$modules     = get_module_folder($modules_dir, $modules_uri, $module_type);

foreach( $modules as $module ){
	
	if( $module['auto'] != 'yes' )  continue;
	
	include_once( $module['module_dir'] . DS . 'module.php' );
	// echo '<pre>';
	// print_r($module);
	// echo '</pre>';
	
}

// echo '<pre>';
// print_r($modules);
// echo '</pre>';

/*
$redux_opt_name = 'aaaaa';
// Replace {$redux_opt_name} with your opt_name.
// Also be sure to change this function name!
if(!function_exists('redux_register_custom_extension_loader')) :
// function redux_register_custom_extension_loader($ReduxFramework) {
function redux_register_custom_extension_loader() {
	$path = dirname( __FILE__ ) . '/modules/';
	$folders = scandir( $path, 1 );
	
	// echo '<pre>';
	// print_r($folders);
	// echo '</pre>';
	
	foreach($folders as $folder) {
		if ($folder === '.' or $folder === '..' or !is_dir($path . $folder) ) {
			continue;
		}
		$extension_class = 'ReduxFramework_Extension_' . $folder;
		if( !class_exists( $extension_class ) ) {
			// In case you wanted override your override, hah.
			$class_file = $path . $folder . '/extension_' . $folder . '.php';
			$class_file = apply_filters( 'redux/extension/'.$ReduxFramework->args['opt_name'].'/'.$folder, $class_file );
			if ( file_exists( $class_file ) ) {
				require_once( $class_file );
				$extension = new $extension_class( $ReduxFramework );
			}
		}
	}
}
// Modify {$redux_opt_name} to match your opt_name
add_action("redux/cpts/{$redux_opt_name}/before", 'redux_register_custom_extension_loader', 0);
redux_register_custom_extension_loader();
endif;
*/
?>