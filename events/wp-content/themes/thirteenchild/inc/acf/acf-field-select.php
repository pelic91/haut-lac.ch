<?php
// $mych = my_acf_load_field();
function my_acf_load_field( $field )
{
	$method = 2;
	
	if( $method == 1 ){
		// reset choices
		$field['choices'] = array();
		
		// get the textarea value from options page without any formatting
		$choices = get_field('animation_effects', 'option', false);
		
		// explode the value so that each line is a new array piece
		$choices = explode("\n", $choices);
		
		// echo "<pre>all options :\n";
		// print_r($choices);
		// echo '</pre>';
		
		// remove any unwanted white space
		$choices = array_map('trim', $choices);
		
		// echo "<pre>space trimmed :\n";
		// print_r($choices);
		// echo '</pre>';
		
		$new_choices  = array();
		$has_optgrp   = false;
		$optsr        = 0;
		
		// loop through array and add to field 'choices'
		if( is_array($choices) )
		{
			$optgr_stat = false;
			$optgrp = '';
			foreach( $choices as $choice )
			{
				if( !empty($choice) ){
					if (strpos($choice, ':') !== FALSE){
						
						// break option with key and value
						$choice = explode(":", $choice);
						
						// remove any unwanted white space
						$choice = array_map('trim', $choice);
						
						if( strtolower($choice[0]) == 'option' ){
							$has_optgrp = true;
							$optsr++;
							$new_choices[ 'opgr_'.$optsr ] = $choice[1];
						}else{
							$new_choices[$choice[0]] = $choice[1];
						}
					}else{
						if( strtolower($choice) == 'option' ){
							$has_optgrp = true;
							$optsr++;
							$new_choices[ 'opgr_'.$optsr ] = $choice;
						}else{
							$new_choices[ $choice ] = $choice;
						}
					}
				}
			}
		}
		
		// echo "<pre>Fresh Choices :\n";
		// print_r($new_choices);
		// echo '</pre>';
		
		if( is_array($new_choices) ){
			$optgrp_stat  = false;
			$optgrp       = 'Option';
			$optgrp_count = 1;
			
			if( $has_optgrp ) {
				foreach( $new_choices as $new_choice_key => $new_choice_val )
				{
					$rest = substr($new_choice_key, 0, 4); // returns "d"
					
					if( substr($new_choice_key, 0, 4) == 'opgr' ){
						$optgrp = $new_choice_val;
					}else{
						$field['choices'][$optgrp][$new_choice_key] = $new_choice_val;
					}
					// if( !$optgrp_stat && $new_choice_key != 'option' ){
						// $field['choices']['Option ' . $optgrp_count][$new_choice_key] = $new_choice_val;
					// }elseif( !$optgrp_stat && $new_choice_key == 'option' ){
						// $optgrp_stat = true;
						// $optgrp       = $new_choice_val;
						// $optgrp_count++;
					// }else{
						// $field['choices'][$optgrp][$new_choice_key] = $new_choice_val;
					// }
				}
			}else{
				$field['choices'] = $new_choices;
			}
		}
		
		
		/*
		if( is_array($choices) )
		{
			$optgr_stat = false;
			$optgrp = '';
			foreach( $choices as $choice )
			{
				if( !empty($choice) ){
					if (strpos($choice, ':') !== FALSE){
						$choice = explode(":", $choice);
						
						// remove any unwanted white space
						$choice = array_map('trim', $choice);
						
						if( strtolower($choice[0]) == 'option' ){
							$optgr_stat = true;
							$optgrp = $choice[1];
							// $field['choices'][$optgrp] => array();
						}
						echo '<pre>';
						print_r($optgr_stat);
						echo '</pre>';
						
						if( $optgr_stat && strtolower($choice[0]) != 'option'){
							$field['choices'][$optgrp][$choice[0]] = $choice[1];
						}else{
						}
						echo '<pre>';
						print_r($choice);
						echo '</pre>';
					}else{
						$field['choices'][ $choice ] = $choice;
					}
				}
			}
		}
		*/
	}
	
	if( $method == 2 ){
		$field['choices'] = array(
			'Attention Seekers' => array(
				'bounce'              =>'bounce',
				'flash'               =>'flash',
				'pulse'               =>'pulse',
				'rubberBand'          =>'rubberBand',
				'shake'               =>'shake',
				'swing'               =>'swing',
				'tada'                =>'tada',
				'wobble'              =>'wobble',
			),
			'Bouncing Entrances'=> array(
					'bounceIn'            =>'bounceIn',
					'bounceInDown'        =>'bounceInDown',
					'bounceInLeft'        =>'bounceInLeft',
					'bounceInRight'       =>'bounceInRight',
					'bounceInUp'          =>'bounceInUp',
			),
			'Bouncing Exits'    => array(
					'bounceOut'           =>'bounceOut',
					'bounceOutDown'       =>'bounceOutDown',
					'bounceOutLeft'       =>'bounceOutLeft',
					'bounceOutRight'      =>'bounceOutRight',
					'bounceOutUp'         =>'bounceOutUp',
			),
			'Fading Entrances'  => array(
				'fadeIn'              =>'fadeIn',
				'fadeInDown'          =>'fadeInDown',
				'fadeInDownBig'       =>'fadeInDownBig',
				'fadeInLeft'          =>'fadeInLeft',
				'fadeInLeftBig'       =>'fadeInLeftBig',
				'fadeInRight'         =>'fadeInRight',
				'fadeInRightBig'      =>'fadeInRightBig',
				'fadeInUp'            =>'fadeInUp',
				'fadeInUpBig'         =>'fadeInUpBig',
			),
			'Fading Exits'      => array(
				'fadeOut'             =>'fadeOut',
				'fadeOutDown'         =>'fadeOutDown',
				'fadeOutDownBig'      =>'fadeOutDownBig',
				'fadeOutLeft'         =>'fadeOutLeft',
				'fadeOutLeftBig'      =>'fadeOutLeftBig',
				'fadeOutRight'        =>'fadeOutRight',
				'fadeOutRightBig'     =>'fadeOutRightBig',
				'fadeOutUp'           =>'fadeOutUp',
				'fadeOutUpBig'        =>'fadeOutUpBig',
			),
			'Flippers'          => array(
				'flip'                =>'flip',
				'flipInX'             =>'flipInX',
				'flipInY'             =>'flipInY',
				'flipOutX'            =>'flipOutX',
				'flipOutY'            =>'flipOutY',
			),
			'Lightspeed'        => array(
				'lightSpeedIn'        =>'lightSpeedIn',
				'lightSpeedOut'       =>'lightSpeedOut',
			),
			'Rotating Entrances'=> array(
				'rotateIn'            =>'rotateIn',
				'rotateInDownLeft'    =>'rotateInDownLeft',
				'rotateInDownRight'   =>'rotateInDownRight',
				'rotateInUpLeft'      =>'rotateInUpLeft',
				'rotateInUpRight'     =>'rotateInUpRight',
			),
			'Rotating Exits'    => array(
				'rotateOut'           =>'rotateOut',
				'rotateOutDownLeft'   =>'rotateOutDownLeft',
				'rotateOutDownRight'  =>'rotateOutDownRight',
				'rotateOutUpLeft'     =>'rotateOutUpLeft',
				'rotateOutUpRight'    =>'rotateOutUpRight',
			),
			'Specials'          => array(
				'hinge'               =>'hinge',
				'rollIn'              =>'rollIn',
				'rollOut'             =>'rollOut',
			),
			'Zoom Entrances'    => array(
				'zoomIn'              =>'zoomIn',
				'zoomInDown'          =>'zoomInDown',
				'zoomInLeft'          =>'zoomInLeft',
				'zoomInRight'         =>'zoomInRight',
				'zoomInUp'            =>'zoomInUp',
			),
			'Zoom Exits'        => array(
				'zoomOut'             =>'zoomOut',
				'zoomOutDown'         =>'zoomOutDown',
				'zoomOutLeft'         =>'zoomOutLeft',
				'zoomOutRight'        =>'zoomOutRight',
				'zoomOutUp'           =>'zoomOutUp',
			),
		);
	}
	
    // Important: return the field
    return $field;
}

// v3.5.8.2 and below
add_filter('acf_load_field-tb_effect', 'my_acf_load_field');

// v4.0.0 and above
add_filter('acf/load_field/name=tb_effect', 'my_acf_load_field');
?>