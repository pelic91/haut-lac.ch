<?php
function my_blog_categories( $field ){
	$field['choices'] = array(
		'Attention Seekers' => 'sssssss',
		'Bouncing Entrances'=> 'eeeeee'
	);
	
    // Important: return the field
    return $field;
}

// v3.5.8.2 and below
add_filter('acf_load_field-tmpl_news_category', 'my_blog_categories');
add_filter('acf_load_field-blog_category_hide', 'my_blog_categories');

// v4.0.0 and above
add_filter('acf/load_field/name=tmpl_news_category', 'my_blog_categories');
add_filter('acf/load_field/name=blog_category_hide', 'my_blog_categories');
?>