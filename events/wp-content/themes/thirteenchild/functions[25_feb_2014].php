<?php
function twentythirteen_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'twentythirteen_logo_section' , array(
    'title'       => __( 'Logo', 'twentythirteen' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
) );
$wp_customize->add_setting( 'twentythirteen_logo' );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'twentythirteen_logo', array(
    'label'    => __( 'Logo', 'twentythirteen' ),
    'section'  => 'twentythirteen_logo_section',
    'settings' => 'twentythirteen_logo',
) ) );
}
add_action('customize_register', 'twentythirteen_theme_customizer');

function twentythirteen_theme_footer_customizer( $wp_customize ) {
    $wp_customize->add_section( 'twentythirteen_footerlogo_section' , array(
    'title'       => __( 'Logo', 'twentythirteen' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
) );
$wp_customize->add_setting( 'twentythirteen_footerlogo' );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'twentythirteen_footerlogo', array(
    'label'    => __( 'Logo', 'twentythirteen' ),
    'section'  => 'twentythirteen_footerlogo_section',
    'settings' => 'twentythirteen_footerlogo',
) ) );
}
add_action('customize_register', 'twentythirteen_theme_footer_customizer');

class Social_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Social_Widget', // Base ID
			__('social media', 'text_domain'), // Name
			array( 'description' => __( 'A social media Widget ', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
        $facebook = apply_filters( 'facebook', $instance['facebook'] );
        $twitter = apply_filters( 'twitter', $instance['twitter'] );
        $pinterest = apply_filters( 'pinterest', $instance['pinterest'] );
        $youtube = apply_filters( 'youtube', $instance['youtube'] );
        $google = apply_filters( 'google', $instance['google'] );
        $instagram = apply_filters( 'instagram', $instance['instagram'] );
        $linkedin = apply_filters( 'linkedin', $instance['linkedin'] );
        $vimeo = apply_filters( 'vimeo', $instance['vimeo'] );
        
        
		echo $args['before_widget'];
        if ( ! empty( $title ) ){
			echo $args['before_title'] . $title . $args['after_title'];
            }
        echo '<ul class="social_icon">';
		if ( ! empty( $facebook ) ){
            
			echo  '<li>'.'<a href='.$facebook.' class="facebook" target="_blank">Facebook</a>'.'</li>';
            }
            
         if ( ! empty( $twitter ) ){
			echo  '<li>'.'<a href='.$twitter.' class="twitter" target="_blank" >Twitter</a>'.'</li>';
            }
            
         if ( ! empty( $pinterest ) ){
			echo  '<li>'.'<a href='.$pinterest.' class="pinterest" target="_blank" >Pinterest</a>'.'</li>';
            }
         
         if ( ! empty( $youtube ) ){
			echo  '<li>'.'<a href='.$youtube.' class="youtube" target="_blank" >Youtube</a>'.'</li>';
            }
         
         if ( ! empty( $google ) ){
			echo  '<li>'.'<a href='.$google.' class="google_plus" target="_blank" >Google+</a>'.'</li>';
            }
            
         if ( ! empty( $instagram ) ){
			echo  '<li>'.'<a href='.$instagram.' class="instagram" target="_blank" >Instagram </a>'.'</li>';
            }
         if ( ! empty( $linkedin ) ){
			echo '<li>'.'<a href='.$linkedin.' class="linkedin"  target="_blank" >Linkedin</a>'.'</li>';
            } 
          if ( ! empty( $vimeo ) ){
			echo '<li>'.'<a href='.$vimeo.' class="vimeo"  target="_blank" >vimeo</a>'.'</li>';
            }    
         
		 echo '</ul>';
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance ) ) {
			$title = $instance[ 'title' ];
            $facebook = $instance[ 'facebook' ];
            $twitter = $instance[ 'twitter' ];
            $pinterest = $instance[ 'pinterest' ];
            $youtube = $instance[ 'youtube' ];
            $google = $instance[ 'google' ];
            $instagram = $instance[ 'instagram' ];
            $linkedin = $linkedin[ 'linkedin' ];
            $vimeo = $vimeo[ 'vimeo' ];
            
		}
		else {
			$title = __( 'New title', 'text_domain' );
            $facebook = __( 'New facebook', 'text_domain' );
            $twitter = __( 'New Twitter ', 'text_domain' );
            $pinterest = __( 'New pinterest', 'text_domain' );
            $youtube = __( 'New youtube', 'text_domain' );
            $google = __( 'New google', 'text_domain' );
            $instagram = __( 'New instance', 'text_domain' );
            $linkedin = __( 'New linkedin', 'text_domain' );
            $vimeo = __( 'New vimeo', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		
        
        
		<label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e( 'facebook:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" type="text" value="<?php echo esc_attr( $facebook ); ?>" />
		
        
        
		<label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e( 'twitter:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" type="text" value="<?php echo esc_attr( $twitter ); ?>" />
        
        <label for="<?php echo $this->get_field_id( 'pinterest' ); ?>"><?php _e( 'pinterest:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'pinterest' ); ?>" name="<?php echo $this->get_field_name( 'pinterest' ); ?>" type="text" value="<?php echo esc_attr( $pinterest ); ?>" />
        
        <label for="<?php echo $this->get_field_id( 'youtube' ); ?>"><?php _e( 'youtube:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'youtube' ); ?>" name="<?php echo $this->get_field_name( 'youtube' ); ?>" type="text" value="<?php echo esc_attr( $youtube ); ?>" />

        <label for="<?php echo $this->get_field_id( 'google' ); ?>"><?php _e( 'google:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'google' ); ?>" name="<?php echo $this->get_field_name( 'google' ); ?>" type="text" value="<?php echo esc_attr( $google ); ?>" />

        <label for="<?php echo $this->get_field_id( 'instagram' ); ?>"><?php _e( 'instagram:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'instagram' ); ?>" name="<?php echo $this->get_field_name( 'instagram' ); ?>" type="text" value="<?php echo esc_attr( $instagram ); ?>" />

        <label for="<?php echo $this->get_field_id( 'linkedin' ); ?>"><?php _e( 'linkedin:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'linkedin' ); ?>" name="<?php echo $this->get_field_name( 'linkedin' ); ?>" type="text" value="<?php echo esc_attr( $linkedin ); ?>" />
        
        <label for="<?php echo $this->get_field_id( 'vimeo' ); ?>"><?php _e( 'vimeo:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'vimeo' ); ?>" name="<?php echo $this->get_field_name( 'vimeo' ); ?>" type="text" value="<?php echo esc_attr( $vimeo ); ?>" />

		
        </p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['facebook'] = ( ! empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : '';
        $instance['twitter'] = ( ! empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : '';
        $instance['pinterest'] = ( ! empty( $new_instance['pinterest'] ) ) ? strip_tags( $new_instance['pinterest'] ) : '';
        $instance['youtube'] = ( ! empty( $new_instance['youtube'] ) ) ? strip_tags( $new_instance['youtube'] ) : '';
        $instance['google'] = ( ! empty( $new_instance['google'] ) ) ? strip_tags( $new_instance['google'] ) : '';
        $instance['instagram'] = ( ! empty( $new_instance['instagram'] ) ) ? strip_tags( $new_instance['instagram'] ) : '';
        $instance['linkedin'] = ( ! empty( $new_instance['linkedin'] ) ) ? strip_tags( $new_instance['linkedin'] ) : '';
        $instance['vimeo'] = ( ! empty( $new_instance['vimeo'] ) ) ? strip_tags( $new_instance['vimeo'] ) : '';
		return $instance;
	}

} 


    register_widget( 'Social_Widget' );   
    
function twentythieteen_widget_init()
{
register_sidebar( array(
'name' => 'Footer Sidebar 1',
'id' => 'footer-sidebar-1',
'description' => 'Appears in the footer area',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

}

add_action('widgets_init','twentythieteen_widget_init');  