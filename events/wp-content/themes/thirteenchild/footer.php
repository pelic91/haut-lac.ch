<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
        
			<?php get_sidebar( 'main' ); ?>
        
			<div class="site-info">
            <?php if ( get_theme_mod( 'twentythirteen_footerlogo' ) ) { ?>
    
        
    
<?php } ?>
				<?php do_action( 'twentythirteen_credits' ); ?>
				
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
<script type='text/javascript' src="http://testhl.haut-lac.net/wp-content/themes/thirteenchild/js/custom.js"></script>
</body>
</html>