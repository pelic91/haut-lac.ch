<?php
$error = NULL;
$filename = NULL;
$num_id = NULL;
	
$id = array_pop(array_keys($_FILES));
$Form = explode('_', $id);
$file = explode('.', $_FILES[$id]['name']);
$extension = $file[count($file)-1];
$extension_ok = array('gif', 'jpg', 'png', 'txt', 'doc', 'docx', 'bmp');
//$upload_path = $_SERVER["DOCUMENT_ROOT"].'/gestion/upload/';
$upload_path = ABSPATH.'gestion/upload/';

session_id($Form[2]);
session_start();

//require_once 'gestion/inc/function/config.php';
//require_once 'gestion/inc/class/user.class.php';

require_once(ABSPATH.'gestion/inc/function/config.php');
require_once(ABSPATH.'gestion/inc/class/user.class.php');

$user = new User($db);

if (!$user->is_loaded()) exit('403');
			
if ($Form[0]!= 'Dyn' && $Form[1]!= 'Form') $error = 'Form error';
is_numeric($Form[count($Form)-1]) ? $num_id = intval($Form[count($Form)-1]) : $error = 'ID Form error';
if (!array_search($extension, $extension_ok)) $error = 'Incorrect extension';


if(!$error && isset($_FILES[$id]) && $_FILES[$id]['error'] == 0) {
	do {$generated_name = strtolower($user->randomPass(20)).'.'.$extension;} while (file_exists($upload_path.$generated_name));
	move_uploaded_file($_FILES[$id]['tmp_name'], $upload_path.$generated_name) ? $error = 'OK' : $error = 'Upload failed !';
	if (!$user->set_response($_POST['folder'], $Form[3], $generated_name)) $error = 'Upload failed !';
} else {
	$error = 'Input error';
}

?>
 
<script type="text/javascript">
<!-- 
        window.top.window.uploadEnd("<?php echo $error; ?>", "<?php echo $num_id?>", "<?php echo $generated_name;?>");
//-->
</script>