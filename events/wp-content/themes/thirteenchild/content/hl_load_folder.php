<div id="contentlong">
<?php
/*
			echo "<pre>";
			print_r($_SESSION);
			echo "</pre>";
*/
//			$login_hl_page = get_permalink(704);
			$login_hl_page = get_permalink(2037);

//			$logout_page = get_permalink(859);
			$logout_page = get_permalink(2042);
			
//			$back_to_folder = get_permalink(838);
			$back_to_folder = get_permalink(2039);
			
//			$file_upload = get_permalink(863);
			$file_upload = get_permalink(2057);


			require_once(ABSPATH.'gestion/inc/function/config.php');
			require_once(ABSPATH.'gestion/inc/class/user.class.php');

			$user = new User($db);

			$tmp = explode('/',$_SERVER['REQUEST_URI']);
			
			if(qtrans_getLanguage()=='en') {
				$tmp[2] = "eng";
			}elseif(qtrans_getLanguage()=='fr') {
				$tmp[2] = "fre";
			}else{
				$tmp[2] = "eng";
			}
			
			$lang = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? '/eng' : '/'.$tmp[2];
			$langraw = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? 'eng' : $tmp[2];


			if ($user->is_loaded()) {

				$current = is_numeric($_GET['folder']) ? $_GET['folder'] : 0 ;
				$user->current_Folder = $current;
				$folder = $user->get_folder();
				
				if ($_GET['folder'] && (count($folder)==0 || $current==0 || !array_key_exists($current, $folder))) {
					echo $lang=='/eng' ?
						'<p>You do not have access to this folder.</p>':
						'<p>vous n\'avez pas acc&egrave;s &agrave; ce dossier.</p>';
				} else {
					$accept_cg = $lang=='/eng' ? 'You must accept and check the general conditions to send this folder.' : 'Vous devez accepter et cocher les conditions g\351n\351rales\net pour envoyer ce dossier.';
					$a_sender = $lang=='/eng' ? 'Sending...' : 'Envoi en cours...';
				
					if (!$_GET['folder']) $current = $user->add_folder();

					$sql = $db->query("SELECT * FROM `hautlac_formulaire` WHERE `Valide` = '1' AND `Type`=7 ORDER BY `Position` ASC;");
					$nb_sep = $sql->num_rows;
					
					$sql = $db->query("SELECT * FROM `hautlac_formulaire` WHERE `Valide` = '1' ORDER BY `Position` ASC;");
					$form_view = $sql->num_rows;
					
					
					$counter = 0;
					$tag_ul = '';
					//$upload_path = $_SERVER["DOCUMENT_ROOT"].'/gestion/upload/';
					$upload_path = ABSPATH.'gestion/upload/';
					$upload_path_www = '/gestion/upload/';
					
					$requiredErrorMsg = $lang=='/eng' ? 'This is a required field' : 'Ceci est un champ obligatoire';
					$ValidationErrorMsg = $lang=='/eng' ? 'One of required fields is not filled in.' : 'Un des champs obligatoires n\'est pas remplie.';
					
					echo '<div id="ValidationMsg" style="display:none;" >'.$ValidationErrorMsg.'</div>';
					
					$js = 'function set_response(obj){id=obj.getAttribute("id").split("_");data_send=encodeURI("SSID='.session_id().'&Form_ID="+id[2]+"&folder='.$current.'&response="+obj.value);$.ajax({type: "POST", url: "/gestion/inc/ajax/insert_item.php", data: data_send}); }';
					
					/* 
					Functions to check required fields and show error message in hidden div (change display css)
					and checkallfields on next / prev btn to hide or show send folder button at the end + error msg general for fields
					*/
				
					$js .= 'function checkallfields(){ $(".required").each(function() { if($(this).val() == "") { $(this).addClass("failed"); } }); $(".requiredcheckbox").each(function() { if($(this).val() == 0) { $(this).addClass("failed"); } });   }';
				
					/*
					//old variant showing msg under field per field
					$js .= 'function checkfield(obj, isRequired){ if(isRequired == "y") { if(obj.value == "") { id="requiredfor_"+obj.getAttribute("id"); $("#"+id).fadeIn("slow"); $("#"+obj.getAttribute("id")).addClass("failed"); } else { id="requiredfor_"+obj.getAttribute("id"); $("#"+id).css("display", "none"); $("#"+obj.getAttribute("id")).removeClass("failed"); } } }';
					$js .= 'function setcheckboxerror(obj, isRequired){ if(isRequired == "y") { id="requiredfor_"+obj.getAttribute("id"); $("#"+id).fadeIn("slow"); $("#"+obj.getAttribute("id")).addClass("failed"); } }';
					$js .= 'function unsetcheckboxerror(obj){ id="requiredfor_"+obj.getAttribute("id"); $("#"+id).css("display", "none"); $("#"+obj.getAttribute("id")).removeClass("failed"); }';
					$js .= 'function checkselect(obj, isRequired){ if(isRequired == "y") { if(obj.value == 0) { id="requiredfor_"+obj.getAttribute("id"); $("#"+id).fadeIn("slow"); $("#"+obj.getAttribute("id")).addClass("failed"); } else { id="requiredfor_"+obj.getAttribute("id"); $("#"+id).css("display", "none"); $("#"+obj.getAttribute("id")).removeClass("failed"); } } }';*/
					
					$js .= 'function checkfield(obj, isRequired){ if(isRequired == "y") { if(obj.value == "") { $("#"+obj.getAttribute("id")).addClass("failed"); } else { $("#"+obj.getAttribute("id")).removeClass("failed"); } } }';
					$js .= 'function setcheckboxerror(obj, isRequired){ if(isRequired == "y") { $("#"+obj.getAttribute("id")).addClass("failed"); } }';
					$js .= 'function unsetcheckboxerror(obj){ $("#"+obj.getAttribute("id")).removeClass("failed"); }';
					$js .= 'function checkselect(obj, isRequired){ if(isRequired == "y") { if(obj.value == 0) { $("#"+obj.getAttribute("id")).addClass("failed"); } else { $("#"+obj.getAttribute("id")).removeClass("failed"); } } }';
					

					$js .= '
					$(document).ready(function(e) {
						$("#a_sender").click(function(){
							if($("#user_agrement").prop("checked") == true){
								$("#a_sender").html("'.$a_sender.'");
								$.ajax({type: "POST",url: "/gestion/inc/ajax/finish_folder.php",data: encodeURI("SSID='.session_id().'&folder='.$current.'"), success: function() {location.replace("'.$back_to_folder.'");}});
							}else{
								alert("'.$accept_cg.'");
								return false;
							}
						});
					});
					function close_it(){
						/*
						if($("#user_agrement").attr("checked") == false) {
							alert("'.$accept_cg.'");
							return false;
						}
						$("#a_sender").html("'.$a_sender.'");
						$.ajax({type: "POST",url: "/gestion/inc/ajax/finish_folder.php",data: encodeURI("SSID='.session_id().'&folder='.$current.'"), success: function() {location.replace("index.php/'.$lang.'/folder-'.$langraw.'");}});
						*/
					}';
					$js .= 'function uploadEnd(sError, Id, Name) {if(sError == \'OK\') {$("#Return_"+Id).html(\'Upload done !<a href="'.$upload_path_www.'\'+Name+\'" target="_new">'.$viewdownloadfile.'</a>\');} else {$("#Return_"+Id).html(sError);}}';
					if ($nb_sep>0) $js .= 'function show_page(from, to) {$("#ask_"+from).fadeOut(200, function() {$("#ask_"+to).fadeIn(400);}); checkallfields(); numFailed = $(".failed").length; if(numFailed > 0) {  $("#ValidationMsg").fadeIn("slow"); $("#a_sender").attr("style","display: none !important"); $("#a_sender_not").attr("style","display: inline-block !important"); } else { $("#ValidationMsg").css("display", "none"); $("#a_sender").attr("style","display: inline-block !important"); $("#a_sender_not").attr("style","display: none !important"); } }';
					echo '<script type="text/javascript">'.$js.'</script>';
																									
					echo '<ul id="ask_0">';
					$previous = $lang=='/eng' ? 'Previous' : 'Pr&eacute;c&eacute;dent';
					$next = $lang=='/eng' ? 'Next' : 'Suivant';
					$send = $lang=='/eng' ? 'Send this folder' : 'Envoyer ce dossier';
					$nosend = $lang=='/eng' ? 'Folder incomplete' : 'Dossier incomplet';
					$uploadsubmit = $lang=='/eng' ? 'Upload the file' : 'Upload du fichier';
					$viewdownloadfile = $lang=='/eng' ? 'View / Download my file' : 'Voir / T&eacute;l&eacute;charger le fichier';	
					$selectoption = $lang=='/eng' ? 'Select an option please' : 'Choisir une option svp';					
					
					
					while($res = $sql->fetch_object()) {
					

						$Question = $lang=='/eng' ? utf8_encode($res->Question_EN) : utf8_encode($res->Question_FR);
						$Choix  = $lang=='/eng' ? utf8_encode($res->Choix_EN) : utf8_encode($res->Choix_FR);
						$DefaultChoix  = $lang=='/eng' ? utf8_encode($res->Default_EN) : utf8_encode($res->Default_FR);
						$Must = $res->Obligatoire=='1' ? '&nbsp;<b>(*)</b>' : '';
						
						$JSRequiredClass = $res->Obligatoire=='1' ? 'required' : '';
						$JSRequiredClassCheckbox = $res->Obligatoire=='1' ? 'requiredcheckbox' : '';
						$JSRequiredBoolean = $res->Obligatoire=='1' ? 'y' : 'n';
						$JSRequiredErrorMsgHolder = $res->Obligatoire=='1' ? '<br/><div id="requiredfor_Dyn_Form_'.$res->FormID.'" class="frequired" style="display:none;">'.$requiredErrorMsg.'</div>' : '';
						
						switch ($res->Type) {
							case 1:
								echo '<li><label>'.$Question.$Must.'</label><input type="text" onkeyup="set_response(this);" onblur="checkfield(this, \''.$JSRequiredBoolean.'\');" value="'.$user->get_response($current, $res->FormID).'" type="textbox" id="Dyn_Form_'.$res->FormID.'" autocomplete="off" maxlength="'.$res->LenMax.'" class="'.$JSRequiredClass.'" />'.$JSRequiredErrorMsgHolder.'</li>';
								break;
							case 2:
								$checked = $user->get_response($current, $res->FormID) ? ' checked="yes"' : '';
								echo '<li><label>'.$Question.$Must.'</label><input value="" onclick="if(this.checked){this.value=\''.$res->CSV_Value.'\'; unsetcheckboxerror(this); }else{this.value=\'0\'; setcheckboxerror(this, \''.$JSRequiredBoolean.'\'); }set_response(this);" '.$checked.'type="checkbox" id="Dyn_Form_'.$res->FormID.'" autocomplete="off" class="'.$JSRequiredClassCheckbox.'" />'.$JSRequiredErrorMsgHolder.'</li>';
								break;
							case 3:
								$attribute = array(1 => array ('<b>', '</b>'), 2 => array ('<u>', '</u>'), 3 => array ('<i>', '</i>'), 4 => array ('<h1>', '</h1>'));
								echo '<li><label>'.$attribute[$res->Choix_FR][0].$Question.$attribute[$res->Choix_FR][1].'</label></li>';
								break;
							case 4:
								if(empty($DefaultChoix)) {
									$ret = '<option value="0">'.$selectoption.'</option>';
								}
								else { $ret = ''; }
								$choice = explode(';', $Choix);
								$value = explode(';', $Choix);
								for ($i = 0; $i <= count($choice)-1; $i++) {								
									$selected = (trim($value[$i]) == $user->get_response($current, $res->FormID) && $user->get_response($current, $res->FormID)) ? 'selected' : '';
									if($selected == 'selected') { $selected_final = $selected; }
									else {
										//get default value
										if(trim($value[$i]) == $DefaultChoix) { $selected_final = 'selected'; }
										else { $selected_final = ''; }
									}
																	
									$ret .= '<option value="'.trim($value[$i]).'"'.$selected_final.'>'.trim($choice[$i]).'</option>';
								}
								echo '<li><label>'.$Question.$Must.'</label><select id="Dyn_Form_'.$res->FormID.'" onchange="if(this.value != 0){set_response(this);}" onblur="checkselect(this, \''.$JSRequiredBoolean.'\');" class="'.$JSRequiredClass.'" >'.$ret.'</select>'.$JSRequiredErrorMsgHolder.'</li>';
								break;
							case 5:
								echo '<form enctype="multipart/form-data" action="'.$file_upload.'" target="uploadFrame" method="post"><li><label>'.$Question.$Must.'</label><input type="hidden" name="folder" value="'.$current.'" /><input type="file" name="Dyn_Form_'.session_id().'_'.$res->FormID.'" onblur="checkfield(this, \''.$JSRequiredBoolean.'\');" class="'.$JSRequiredClass.'" /><input type="submit" value="'.$uploadsubmit.'"><span class="upload_status" id="Return_'.$res->FormID.'">';
								if ($user->get_response($current, $res->FormID)) {echo '<a href="'.$upload_path_www.$user->get_response($current, $res->FormID).'" target="_new">'.$viewdownloadfile.'</a>';}
								echo '</span>'.$JSRequiredErrorMsgHolder.'</li></form>';
								break;
							case 6:
								$text_limit = $res->LenMax>0 ? ' onkeypress="this.value = this.value.slice(0, '.$res->LenMax.')"' : '';
								echo '<li><label>'.$Question.$Must.'</label><textarea'.$text_limit.' onkeyup="set_response(this);" onblur="checkfield(this, \''.$JSRequiredBoolean.'\');" id="Dyn_Form_'.$res->FormID.'" class="'.$JSRequiredClass.'" >'.$user->get_response($current, $res->FormID).'</textarea>'.$JSRequiredErrorMsgHolder.'</li>';
								break;
							case 7:
								$counter++;

								echo '<div id="step_'.$counter.'" class="by_step">';
							
								if ($nb_sep>0) {
									echo $lang=='/eng' ? 
										'<span class="step">Step '.$counter.' of '.($nb_sep+1).'</span>':
										'<span class="step">&Eacute;tape '.$counter.' sur '.($nb_sep+1).'</span>';
								}
								
		
								if ($counter > 1) {
									echo '<a href="javascript:show_page('.($counter-1).', '.($counter-2).')" class="frmbtn">&laquo;&nbsp;'.$previous.'</a>';
								} else {
									echo '<span class="step_empty">&nbsp;</span>';
								}
								if ($counter < $nb_sep+1) {
									echo '<a href="javascript:show_page('.($counter-1).', '.$counter.')" class="frmbtn">'.$next.'&nbsp;&raquo;</a>';
								} else {
									echo '<span class="step_empty">&nbsp;</span>';
								}
								echo '</div>';
																
								echo '</ul><ul id="ask_'.$counter.'" style="display:none;">';
								
								break;	
								
						}
												
					}
										
				if ($lang=='/eng') {
					echo '<li style="line-height:32px;color:#8A0808;text-align:center;vertical-align:middle;">I acknowledge that I have read the <a href="http://hl.haut-lac.ch/uploads/condition-general/condition-general.pdf" target="_new" title="Click here to download the general conditions">general conditions</a> and accept them by ticking the following box&nbsp;&raquo;&nbsp;<input type="checkbox" id="user_agrement" value="0" /></li>';					
				} else {
					echo '<li style="line-height:32px;color:#8A0808;text-align:center;vertical-align:middle;">Je reconnais avoir pris connaissance des <a href="http://hl.haut-lac.ch/uploads/condition-general/condition-general.pdf" target="_new" title="Cliquez ici pour télécharger les conditions générales">conditions générales</a> et les accepte en cochant la case suivante&nbsp;&raquo;&nbsp;<input type="checkbox" id="user_agrement" value="0" /></li>';
				}

				echo '<div id="step_'.$counter.'" class="by_step">';

				if ($nb_sep>0) {
					echo $lang=='/eng' ? 
						'<span class="step">Step '.($nb_sep+1).' of '.($nb_sep+1).'</span>':
						'<span class="step">&Eacute;tape '.($nb_sep+1).' sur '.($nb_sep+1).'</span>';
				}
				echo '<a href="javascript:show_page('.$counter.', '.($counter-1).')" class="frmbtn">&laquo;&nbsp;'.$previous.'</a>';
				echo '<a href="javascript:void(0)" onclick="close_it()" id="a_sender" class="frmbtnlast" style="">'.$send.'</a>';
				echo '<a href="javascript:void(0)" id="a_sender_not" class="frmbtnlasterror" style="display:none; !important">'.$nosend.'</a></div>';
								
				echo '</ul>';
								
				echo '<iframe id="uploadFrame" name="uploadFrame" src="" style="width:0;height:0;display:none;"></iframe>';
				
				echo $lang=='/eng' ? 
					'<p>(*) This information is required.</p><a href="'.$back_to_folder.'">Back to my folders</a>':
					'<p>(*) Ces informations sont obligatoires.</p><a href="'.$back_to_folder.'">Retourner &agrave; mes dossiers</a>';
				
				echo $lang=='/eng' ?
						'<p><a href="'.$logout_page.'">Login out</a></p>':
						'<p><a href="'.$logout_page.'">D&eacute;connexion</a></p>';

				}
			} else {
				echo $lang=='/eng' ? 
					'<p>You must be <a href="'.$login_hl_page.'">registered</a> to access this folder</a>':
					'<p>Vous devez &ecirc;tre <a href="'.$login_hl_page.'">identifi&eacute;</a> pour acc&eacute;der &agrave; vos dossiers.</p>';
			}

?>
</div>