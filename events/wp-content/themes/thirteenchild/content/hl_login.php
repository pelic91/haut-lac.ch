<div id="contentlong">
<?php
/*
			echo "<pre>";
			print_r($_SESSION);
			echo "</pre>";
*/
//			$admintion_folder = get_permalink(838);
			$admintion_folder = get_permalink(2039);
			
//			$create_hl_account = get_permalink(844);
			$create_hl_account = get_permalink(2043);

//			$lost_hl_login = get_permalink(847);
			$lost_hl_login = get_permalink(2046);
					
			require_once(ABSPATH.'gestion/inc/function/config.php');
			require_once(ABSPATH.'gestion/inc/class/user.class.php');
		
			$user = new User($db);
			
			$tmp = explode('/',$_SERVER['REQUEST_URI']);

			if(qtrans_getLanguage()=='en') {
				$tmp[2] = "eng";
			}elseif(qtrans_getLanguage()=='fr') {
				$tmp[2] = "fre";
			}else{
				$tmp[2] = "eng";
			}

			$lang = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? '/eng' : '/'.$tmp[2];
			$langraw = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? 'eng' : $tmp[2];
			
			if ( isset($_POST['login']) && isset($_POST['pass'])){
				if ( !$user->login($_POST['login'],$_POST['pass'] )){
					echo $lang=='/eng' ?
						'<span class="login_error">Identification failed. <a href="javascript:void(0);" onclick="history.back()">Repeat please.</span>':
						'<span class="login_error">Erreur d\'indentification. <a href="javascript:void(0);" onclick="history.back()">Recommencez s.v.p.</span>';
				} else {

					echo '<script language="JavaScript">setTimeout(\'move()\',5000);function move() {window.location  = "'.$admintion_folder.'"}</script>';
			
					echo $lang=='/eng' ?
					'<span class="login_info">Identifying successful, you will be redirected to your records, thank you wait 5 seconds or <a href="'.$admintion_folder.'">click  here </a>.</span>':
					'<span class="login_info">Identification r&eacute;ussi, vous allez &ecirc;tre redirig&eacute; vers vos dossiers, merci de patienter 5 secondes ou cliquez <a href="'.$admintion_folder.'">sur ce lien</a>.</span>';
				}
			} else {
			
			$mail = $lang=='/eng' ? 'Email address' : 'Adresse email';
			$password = $lang=='/eng' ? 'Password' : 'Mot de passe';
			$create = $lang=='/eng' ? 'Create an account' : 'Cr&eacute;er un compte';
			$lost_login = $lang=='/eng' ? 'Lost login ?' : 'Mot de passe perdu ?';
			$create_account = $lang=='/eng' ? 'To create a folder, you must ' : 'Pour constituer un dossier, vous devez ';
			
			$submit_label = $lang=='/eng' ? 'Send ' : 'Envoyer ';
						
			$form_html  = '<p>'.$create_account;

//			$form_html .= '<a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/registration-'.$langraw.'">'.$create.'</a>.';
//			$form_html .= '<a href="index.php'.$lang.'/admissions-'.$langraw.'/inscription-'.$langraw.'/login_lost-'.$langraw.'">'.$lost_login.'</a>';
			
			// for dev server
			
			$form_html .= '<a href="'.$create_hl_account.'">'.$create.'</a>.'; 
			$form_html .= '<a href="'.$lost_hl_login.'">'.$lost_login.'</a>';

			// for local server
//			$form_html .= '<a href="'.get_permalink(839).'">'.$create.'</a>.';
//			$form_html .= '<a href="'.get_permalink(847).'">'.$lost_login.'</a>';

			$form_html .= '<br /><br /></p>';
			$form_html .= '<form name="login" method="post">
							<label class="login">'.$mail.'</label><input type="text" name="login" /><br />
							<label class="login">'.$password.'</label><input type="password" name="pass" /><br />
							<input type="submit" value="'.$submit_label.'" /><br />
						   </form>';
						   
			echo $form_html;
			}
?>
</div>