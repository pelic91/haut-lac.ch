<div id="contentlong">
<?php
/*
			echo "<pre>";
			print_r($_SESSION);
			echo "</pre>";
*/
			require_once(ABSPATH.'gestion/inc/function/config.php');
			require_once(ABSPATH.'gestion/inc/class/user.class.php');
			
			$user = new User($db);
			$err = false;
			
			$tmp = explode('/',$_SERVER['REQUEST_URI']);
			
			if(qtrans_getLanguage()=='en') {
				$tmp[2] = "eng";
			}elseif(qtrans_getLanguage()=='fr') {
				$tmp[2] = "fre";
			}else{
				$tmp[2] = "eng";
			}
			
			$lang = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? '/eng' : '/'.$tmp[2];
			$langraw = ($tmp[2]!= 'eng' && $tmp[2]!= 'fre') ? 'eng' : $tmp[2];
			
			$submit_label = $lang=='/eng' ? 'Send ' : 'Envoyer ';
			
			if (!empty($_POST)) {
				if (!preg_match('/^[a-zA-Z0-9._-]{1,64}@[a-z0-9._-]{2,64}\.[a-z]{2,4}$/', $_POST['login'])) {$erreur[0] = true; $err = true;}
				if (!$err) {
					$sql = $user->query("SELECT `{$user->tbFields['pass']}` FROM `{$user->dbTable}` WHERE `{$user->tbFields['login']}` = '".$_POST['login']."' LIMIT 1",__LINE__);
					if ( $sql->num_rows==1 ){
						$res = $sql->fetch_array();
						if ($lang=='/eng') {
							$email = 'Your login is : '.$_POST['login'].' , your password is : '.$res[0];
							mail($_POST['login'], 'Haut Lac - Reminder of your login and your password', $email);
							echo 'An email containing your login has been sent.';
						} else {
							$email = 'Votre identifiant est : '.$_POST['login'].' , votre mot de passe : '.$res[0];
							mail($_POST['login'], 'Haut Lac - Rappel de votre mot de passe', $email);
							echo 'Un email contenant vos identifiants vient d\'&ecirc;tre envoy&eacute;.';				
						}
					} else {
						echo $lang=='/eng' ? 'This account don\'t exist.' : 'Ce compte n\'existe pas';
						 
					}
				}
			} else {

				$indicate = $lang=='/eng' ? 'Enter your email address, you will receive your new password.' : 'Indiquez votre adresse email, vous recevrez de nouveau votre mot de passe.';
				$mail = $lang=='/eng' ? 'Email address' : 'Adresse email';
				
				echo '
				<p>'.$indicate.'<br /><br /></p>
				<form name="login" method="post">
					<label class="login">'.$mail.'</label><input type="text" name="login" value="'.$_POST['login'].'" />';
					if ($erreur[0])
						{echo '<span class="login_error">'.$mail.' incorrect</span>' ;}
				echo '
					<br />
					<input type="submit" value="'.$submit_label.'" /><br />
				</form>';
			}
?>
</div>