<?php
$commnunity_image= get_sub_field('commnunity_image');
$commnunity_image_title           = $commnunity_image['title'];
$commnunity_image_alt             = $commnunity_image['alt'];

$full_image_size     = 'large';
$gal_img             = $gal_data['sizes'][$full_image_size];
$gal_img_width       = $gal_data['sizes'][$full_image_size.'-width'];
$gal_img_height      = $gal_data['sizes'][$full_image_size.'-height'];

$thumb_image_size    = 'large';
$commnunity_image_thumb       = $commnunity_image['sizes'][$thumb_image_size];
$commnunity_image_thumb_width = $commnunity_image['sizes'][$thumb_image_size.'-width'];
$commnunity_image_height= $commnunity_image['sizes'][$thumb_image_size.'-height'];
?>
<section class="<?php echo get_row_layout();?> <?php echo $third_class; ?> third_sections" style="margin: 0 -5000px;" >

	<div class="wrapper">
		<div class="commnity_content">
			<div class="commnity_content_id">
	 <h1><?php the_sub_field('community_title');?></h1>
	<p>
		<?php the_sub_field('community_description');?>
	 </p>
	 <a href="<?php  the_sub_field('community_link'); ?>" class="grey_but">
			<span><?php the_sub_field('community_link_title'); ?></span>
			<span class="grey_but_shadow"></span>
			<b><?php the_sub_field('know_more'); ?></b>
	</a>

		</div>
		<img src="<?php  echo $commnunity_image_thumb;?>" title="<?php echo $commnunity_image_title; ?>" alt="<?php echo $commnunity_image_alt; ?>"/>
	   </div>

	</div>
</section>