<section class="school_section <?php echo get_row_layout();?>">
	<div class="widget rpwe_widget recent-posts-extended">
		<div class="patten_tital">
			<span class="patten_blog_left"></span>
				<h2><?php echo __('Latest News', 'twentythirteen')?></h2>
			<span class="patten_blog_right"></span>
		</div>
		<?php
		$args = array(
			'posts_per_page'   => 3,
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_type'        => 'post',
			'post_status'      => 'publish',
			'post__not_in'     => get_option( 'sticky_posts' ),
			'suppress_filters' => true
		);
		$posts_array = get_posts( $args );
		if( $posts_array ){
			?>
			<div class="rpwe-block ">
				<ul class="rpwe-ul">
					<?php
					foreach ( $posts_array as $post ) {
						setup_postdata( $post );
						?>
						<li class="rpwe-li rpwe-clearfix">
							<?php
							if ( has_post_thumbnail() ){
								?>
								<a class="rpwe-img" href="<?php echo esc_url( get_permalink() );?>" rel="bookmark">
									<img class="rpwe-alignleft rpwe-thumb" alt="" src="">
									<?php
									echo get_the_post_thumbnail( get_the_ID(),
										'recent_post',
										array( 
											'class' => $args['thumb_align'] . ' rpwe-thumb the-post-thumbnail',
											'alt'   => esc_attr( get_the_title() )
										)
									);
									?>
								</a>
								<?php
							}
							?>
							<h3 class="rpwe-title">
								<a rel="bookmark" title="<?php echo sprintf( esc_attr__( 'Permalink to %s', 'rpwe' ), the_title_attribute( 'echo=0' ) );?>" href="<?php echo esc_url( get_permalink() );?>">
									<?php echo esc_attr( get_the_title() );?>
								</a>
							</h3>
							<?php
							$date = get_the_date();
							?>
							<time datetime="<?php echo esc_html( get_the_date( 'c' ) );?>" class="rpwe-time published"><?php echo esc_html( $date );?></time>
							<div class="rpwe-summary">
								<?php
								echo wp_trim_words( get_the_excerpt(), 25, ' &hellip;' );
								?>
								<a href="<?php echo esc_url( get_permalink() );?>" class="more-link"><?php echo __('read more');?></a>
							</div>
						</li>
						<?php
					}
					?>
				</ul>
			</div>
			<?php
		}
		wp_reset_postdata();
		?>
	</div>
</section>