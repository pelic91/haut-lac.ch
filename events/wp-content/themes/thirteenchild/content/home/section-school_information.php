<?php
$school_information_image           = get_sub_field('school_information_image');
//print_r($school_information_image);

$school_information_image_title     = $school_information_image['title'];
$school_information_image_alt       = $school_information_image['alt'];

$thumb_image_size                   = 'medium';
$school_information_img_thumb       = $school_information_image['sizes'][$thumb_image_size];
$school_information_img_thumb_width = $school_information_image['sizes'][$thumb_image_size.'-width'];
$school_information_img_thumb_height= $school_information_image['sizes'][$thumb_image_size.'-height'];
?>
<section class="<?php echo get_row_layout();?> <?php echo $third_class; ?>">
	<div class="school_box">
		<div class="school_img">
			<img src="<?php  echo $school_information_img_thumb;?>" title="<?php echo $school_information_image_title; ?>" alt="<?php echo $school_information_image_alt; ?>"/>
		</div>
		<span class="school_shadow"></span>
		<span class="school_tital"><?php the_sub_field('school_information_title');?></span>
		<p class="school_details"><?php the_sub_field('school_information_description');?></p>
		<a class="white_but" href="<?php  the_sub_field('school_information_read_more'); ?>">
			<span><?php the_sub_field('school_information_read_more_title'); ?></span>
			<span class="white_but_shadow"></span>
		</a>
		<span class="fold"></span>
	</div>
	<span class="school_box_shadow"></span>
</section>						