<section class="<?php echo get_row_layout();?> <?php echo $third_class; ?>">
	<?php
	$school_info_image           = get_sub_field('school_info_image');
	$school_info_image_title     = $school_info_image['title'];
	$school_info_image_alt       = $school_info_image['alt'];
	
	$full_image_size             = 'large';
	$gal_img                     = $gal_data['sizes'][$full_image_size];
	$gal_img_width               = $gal_data['sizes'][$full_image_size.'-width'];
	$gal_img_height              = $gal_data['sizes'][$full_image_size.'-height'];
	
	$thumb_image_size            = 'medium';
	$school_info_img_thumb       = $school_info_image['sizes'][$thumb_image_size];
	$school_info_img_thumb_width = $school_info_image['sizes'][$thumb_image_size.'-width'];
	$school_info_img_thumb_height= $school_info_image['sizes'][$thumb_image_size.'-height'];
	$school_info_read_more       = get_sub_field('school_info_read_more');
	?>
	<div class="bilingualism_practice">
		<img src="<?php  echo $school_info_img_thumb;?>" title="<?php echo $school_info_image_title; ?>" alt="<?php echo $school_info_image_alt; ?>"/>
		<span class="bilin_tital"><?php the_sub_field('school_info_title');?></span>
		<p><?php the_sub_field('school_info_description');?></p>
		<a class="white_but" href="<?php  the_sub_field('school_info_read_more'); ?>">
			<span><?php the_sub_field('school_info_read_more_title'); ?></span>
			<span class="white_but_shadow"></span>
		</a>
		<span class="fold"></span>
	</div>
	<span class="bilin_prac_box_shadow"></span>
</section>