<section class="school_section <?php echo get_row_layout();?>">
	<?php
	$full_width   =  get_sub_field('full_width');
	$school_infos =  get_sub_field('school_infos');
	$count        = count($school_infos);
	$columns      = 2;
	
	if( have_rows('school_infos') ){
		?>
		<div class="bilingualism_practice">
			<div class="bilingualism_practice_items">
				<?php
				while( have_rows('school_infos') ){
					the_row();
					++$school_infos_sr;
					
					$class = array();
					if( $full_width && ( $count == $school_infos_sr && ($school_infos_sr % $columns) == 1 )){
						// $class[] = 'col-sm-12';
						$class[] = 'full';
					}else{
						// $class[] = 'col-sm-'.(12/$columns);
					}
					$class = implode(' ', $class);
					
					$sinf_title           = get_sub_field('school_info_title');
					
					$sinf_img           = get_sub_field('school_info_image');
					$sinf_img_title     = $sinf_img['title'];
					$sinf_img_alt       = $sinf_img['alt'];
					$sinf_img_size      = 'medium';
					$sinf_img_src       = $sinf_img['sizes'][$sinf_img_size];
					$sinf_img_width     = $sinf_img['sizes'][$sinf_img_size.'-width'];
					$sinf_img_height    = $sinf_img['sizes'][$sinf_img_size.'-height'];
					
					$sinf_desc          = get_sub_field('school_info_description');
					$sinf_readmore_link = get_sub_field('school_info_read_more');
					$sinf_readmore_title= get_sub_field('school_info_read_more_title');
					?>
					<div class="bilingualism_practice_item <?php echo $class;?>">
						<div class="bilingualism_practice_item_thumb">
							<img src="<?php echo $sinf_img_src;?>" title="<?php echo $sinf_img_title;?>" alt="<?php echo $sinf_img_alt;?>">
						</div>
						<div class="bilingualism_practice_item_data">
							
								<span class="bilin_tital"><?php echo $sinf_title;?></span>
							
							<p><?php echo $sinf_desc;?></p>
							<?php
							if( $sinf_readmore_link ){
								?>
								<a href="<?php echo $sinf_readmore_link;?>" class="white_but"><span>read more</span></a>
								<span class="white_but_shadow"></span>
								<?php
							}
							?>
						</div>
						<div class="clearfix"></div>
					</div>
					<?php
				}
				?>
				<div class="clearfix"></div>
			</div>
			<span class="fold"></span>
		</div>
		<span class="bilin_prac_box_shadow"></span>
		<?php
	}
	?>
</section>