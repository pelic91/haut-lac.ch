<?php /* ?>
<ul class="flexiselsummer_two logo_slider">
	<?php
	if( have_rows('content_blocks') ){
		
		// loop through the rows of data
		while ( have_rows('content_blocks') ) {
			the_row();
			
			if( get_row_layout() == 'haut_schoolinfo_logo' ){
				?>
				<li>
					<section class="<?php echo get_row_layout();?> <?php echo $third_class; ?>">
						<?php
						$school_logo_title= get_sub_field('schoolinfo_logo_image_title');
						$school_logo_image= get_sub_field('schoolinfo_logo_image');
						$school_logo_link= get_sub_field('schoolinfo_logo_image_link');
						$school_logo_image = $school_logo_image['url'];
						?>
						<a href="<?php echo $school_logo_link; ?>" target="_blank" title="<?php echo $school_logo_title; ?>"><img src="<?php echo $school_logo_image; ?>" title="<?php echo $school_logo_title; ?>"  alt="<?php echo $school_logo_title; ?>" /></a>
					</section>
				</li>
				<?php
			}
		}
	}else{
		// no layouts found
	}
	?>
</ul>
<?php */ ?>