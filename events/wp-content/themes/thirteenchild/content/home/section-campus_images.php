<section class="school_section <?php echo get_row_layout(); ?>">
    <div class="new_campus">

        <div class="rpwe-block1">
            <div class="patten_tital">
                <h3><?php the_sub_field('capmus_title'); ?></h3>
            </div>
           <?php echo do_shortcode( '[wpml_translate lang=\'en\']<img class="holdiays_img" src="http://www.haut-lac.ch/wp-content/uploads/2015/12/Screenshot-from-2015-12-19-15-31-42.jpg" />[/wpml_translate][wpml_translate lang=\'fr\'][ai1ec cat_id="74"][/wpml_translate]' );?>
        </div>
        <div class="rpwe-block2">
            <div class="patten_tital">
                <h3><?php echo do_shortcode('[wpml_translate lang=\'en\']Pictures of the Week[/wpml_translate][wpml_translate lang=\'fr\']Photos de la semaine[/wpml_translate]');?></h3>
            </div>
            <?php echo do_shortcode( '[instagram-feed  showheader=false]' );?>
        </div>
    </div>
</section>
