<section class="<?php echo get_row_layout();?> <?php echo $third_class; ?>" style="margin: 0 -5000px;">
	<div class="wrapper">
		<div class="enrol_no">
			<h1><?php the_sub_field('entrol_title');?></h1>
			<?php the_sub_field('enrol_content');?>
		</div>
	</div>
</section>
<?php
/*
if(is_active_sidebar('content-sidebar-1')){
	dynamic_sidebar('content-sidebar-1');
}
*/
?>