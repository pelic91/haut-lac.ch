<aside class="widget qtranxs_widget" id="">
	<?php
	$languages = icl_get_languages('skip_missing=0&orderby=id&order=desc');
	if(!empty($languages)){
		?>
		<ul id="qtranslate-2-chooser" class="qtranxs_language_chooser">
			<?php
			foreach($languages as $l){
				// echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
				
				$native_name = $l['native_name'];
				
				$class = array();
				$class[] = 'lang-'.$l['language_code'];
				if( $l['active'] ){
					$class[] = 'active';
				}
				$class = implode(' ', $class);
				?>
				<li class="<?php echo $class;?>">
					<a class="qtranxs_text qtranxs_text_<?php echo $l['language_code'];?>" title="<?php echo $native_name;?>" hreflang="<?php echo $l['language_code'];?>" href="<?php echo $l['url'];?>">
						<span><?php echo $native_name;?></span>
					</a>
				</li>
				<?php
			}
			?>
		</ul>
		<?php
	}
	?>
</aside>