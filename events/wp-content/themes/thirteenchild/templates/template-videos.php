<?php
/**
 * Template Name: Videos
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area inner_blog our blog">
		<div id="content" class="site-content wrapper" role="main">
			<header class="entry-header">
				<?php
				$blog_page_id = $post->ID;
				if( is_home() ){
					$blog_page_id = get_option('page_for_posts');
				}
				$page_title = get_the_title();
				$blog_title =  get_field('blog_title', $blog_page_id);
				if( $blog_title ){
					$page_title = $blog_title;
				}
				?>
				<h1 class="entry-title"><?php echo $page_title; ?></h1>
				<div class="header_desc">
					<?php
					$blog_description =  get_field('blog_description', $blog_page_id);
					
					if($blog_description){
						?>
						 <p><?php echo $blog_description;?></p>
						<?php
					}
					?>
				</div>
			</header>
			
			<?php get_sidebar(); ?>
			
			<div class="main_area">
				<div class="videos-blog">
					<?php
					$video_query = new WP_Query( array(
						'post_type'      => 'video',
						'posts_per_page' => 6,
						'paged'          => get_query_var('paged')
					) );
					?>
					<div class="recent-articles-wrapper">
						<?php
						// Posts are found
						if ( $video_query->have_posts() ) {
							?>
							<div class="recent-articles">
								<?php
								$i = 1;
								while ( $video_query->have_posts() ) {
									$video_query->the_post();
									global $post;
									
									$video = get_field('video');
									$video_url = get_post_meta( get_the_ID(), 'video', true );
									
									if( $video ){
										?>
										<div id="post-<?php the_ID(); ?>" class="recent-article recent-video<?php echo ( $i == 3 ? ' last' : '' );?>">
											<div class="article-thumb">
												<?php /* ?><a href="#ipad-video-<?php the_ID(); ?>" rel="prettyPhoto"><?php */ ?>
												<?php
												// https://www.youtube.com/watch?v=w9OhG7Wx1CY
												?>
												<a href="<?php echo $video_url;?>" rel="prettyPhoto" title="<?php echo $video->title;?>">
													<?php
													/*
													if ( has_post_thumbnail() ) {
														the_post_thumbnail('recent_article_thumb');
													}else{
														?>
														<img src="http://dummyimage.com/199x149/e1e1e1/ffffff.png&text=<?php the_title(); ?>" alt="<?php the_title(); ?>"/>
														<?php
													}
													*/
													?>
                                                    <?php the_post_thumbnail(); ?>
													<span class="player-btn"></span>
												</a>
											</div>
											<h3 class="article-title">
												<?php the_title(); ?>
											</h3>
											<?php
											$description = get_field('description');
											if( !$description ){
												// $description = 'Lorem Ipsum is simply dummy text.';
											}
											if( $description ){
												?>
												<div class="video_desc">
													<?php echo $description;?>
												</div>
												<?php
											}
											?>
										</div>
										<?php
										$i++;
									}
								}
								if( function_exists('wp_pagenavi') ){
									wp_pagenavi( array( 'query' => $video_query ) );
								}
								?>
							</div>
							<?php
							wp_reset_postdata(); // Reset Query
							
						}else{ // Posts not found
							echo '<h4>' . __( 'Posts not found', 'su' ) . '</h4>';
						}
						?>
						<div class="clearfix"></div>
					</div><!-- .recent-articles-wrapper -->
				</div><!-- .videos-blog -->
			</div><!-- .main_area -->
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>