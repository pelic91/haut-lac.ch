<?php
/**
 * Template Name: News
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area inner_blog our blog">
		<div id="content" class="site-content wrapper" role="main">
			<header class="entry-header">
				<?php
				$blog_page_id = $post->ID;
				if( is_home() ){
					$blog_page_id = get_option('page_for_posts');
				}
				$page_title = get_the_title();
				$blog_title =  get_field('blog_title', $blog_page_id);
				if( $blog_title ){
					$page_title = $blog_title;
				}
				?>
				<h1 class="entry-title"><?php echo $page_title; ?></h1>
				<div class="header_desc">
					<?php
					$blog_description =  get_field('blog_description', $blog_page_id);
					
					if($blog_description){
						?>
						 <p><?php echo $blog_description;?></p>
						<?php
					}
					?>
				</div>
			</header>
			
			<?php get_sidebar(); ?>
			
			<div class="main_area">
				<?php
				if( function_exists('get_field') ){
					$tmpl_news_category = get_field('tmpl_news_category');
				}
				global $wp_query;
				query_posts( 'cat='.$tmpl_news_category );
				?>
				<?php if ( have_posts() ) : ?>

					<?php /* The loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						
						<?php // get_template_part( 'content/news-template' ); ?>
						
						<div class="consectetur_section">
							<div class="consectetur_details">
								<div class="incididunt_blog">
									<?php
									//$gtdate = the_date(); 
									//$month = get_the_time('M', $post->ID);
									//echo $month = date('M',get_the_time());
									//echo get_the_time('M', $post->ID); 
									?>
									<span class="entry-date">
										<?php echo get_the_time('M',$post->ID); ?>
										<span><?php echo get_the_time('j',$post->ID); ?></span>
										<?php echo get_the_time('Y',$post->ID); ?>
									</span>
									<span class="blog_tital"><a href="<?php echo get_permalink( $post_id ); ?>"> <?php the_title(); ?></a></span>
									<div class="consectetur_content">
										<?php
										$the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $type);
										if(!empty($the_url)) {
											?>
											<img src="<?php echo  $the_url[0]; ?>" alt="<?php echo  $the_url[0];  ?>"/>
											<?php
										}
										?>
										<div class="blog_details">
											<?php the_excerpt(); ?>
											<a href="<?php echo get_permalink( $post_id ); ?>"><?php echo __(utf8_encode("<!--:fr-->LIRE LA SUITE<!--:--><!--:en-->VIEW MORE  <!--:-->")); ?></a>
											<?php
											$my_var = get_comments_number( $post_id );
											?>
											<span class="comment"><?php echo $my_var; ?></span>
											<span class="date_and_time_time"><?php  the_time('jS F , Y g:i a') ; ?></span>
										</div>
										<!--<span class="date_and_time_time">--><?php //echo get_the_date('g:i a'); ?><!--</span>-->
									</div> 
								</div>
							</div>
							<div class="consectetur_shadow"></div>
						</div>
					<?php endwhile; ?>
					
					<?php
					// Reset Query
					wp_reset_query();
					?>
					
					<?php twentythirteen_paging_nav(); ?>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
			</div><!-- .main_area -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>