<form action="<?php echo home_url( '/' ); ?>" class="search-form" method="get" role="search">
	
	<label><span class="screen-reader-text"><?php echo utf8_encode( __("Search...")); ?></span>
		<input type="search" title="<?php echo utf8_encode( __("Search...")); ?>" name="s" value="" placeholder="<?php echo utf8_encode( __("Search...")); ?>" class="search-field"/>
    </label>
    <input type="submit" value="Search" class="search-submit"/>
    
</form>