<section class="school_section <?php echo get_row_layout(); ?>">
    <div class="new_campus">

        <div class="rpwe-block1">
            <div class="patten_tital">
                <h3><?php the_sub_field('capmus_title'); ?></h3>
            </div>
            <?php echo do_shortcode( '[ai1ec cat_name="this66_week"]' );?>
        </div>
        <div class="rpwe-block2">
            <div class="patten_tital">
                <h3><?php echo do_shortcode('[wpml_translate lang=\'en\']Pictures of the Week[/wpml_translate][wpml_translate lang=\'fr\']Photos de la semaine[/wpml_translate]');?></h3>
            </div>
            <?php echo do_shortcode( '[instagram-feed  showheader=false]' );?>
        </div>
    </div>
</section>
