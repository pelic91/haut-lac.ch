<?php
if ($API_Key != "64faf5d0b1dc311fd0f94af64f6c296a03045571") {header("Location: /gestion/");}

// Login access

		if ($_POST['i'] || $_POST['p']) {
			$err = null;
			
			if (strlen($_POST['i'])>32 || strlen($_POST['p'])>16) {$err = 'Identifiant ou mot de passe incorrect.';}
			
			if (!$err && $admin->login($_POST['i'], $_POST['p'])) {
				header("Location: /gestion/");
			}
			else {
				$err = '&Eacute;chec d\'identification.';$site->Set_Title($err);
			}
			
		}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $site->Get_Title();?></title>
	<link href="media/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="media/css/login.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<div id="login">

		<div id="login-box">
		
			<form name="login" method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
		
				<div id="login-box-title"><?php echo $err ?  '<img src="media/image/icons/exclamation.png" width="16" height="16" alt="Erreur" />'.$err : 'Veuillez indiquer votre identifiant ainsi que votre mot de passe ci-dessous.';?></div>
				
				<div class="login-box-field">
					<label>Identifiant :</label>
					<input name="i" class="form-login" title="Identifiant" value="" maxlength="32" />
				</div>
					
				<div class="login-box-field">
					<label>Mot de passe :</label>
					<input name="p" type="password" class="form-login" title="Mot de passe" value="" maxlength="16" onKeyPress="if (event.keyCode == 13) document.login.submit();" />
				</div>

				<a href="javascript:void(0)" onclick="document.login.submit();" class="button">Identification</a>
			
			</form>

		</div>
		
	</div>

</body>
</html>
