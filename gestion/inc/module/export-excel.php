<?php
if (!isset($API_Key)) die('400');

foreach (glob('../class/*.php') as $file) {include_once($file);}
foreach (glob('../function/*.php') as $file) {include_once($file);}


$folder = intval($_GET['folder']);
if ($folder == 0) die('400');

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator('AGARTHA');
$objPHPExcel->getProperties()->setLastModifiedBy('AGARTHA');
$objPHPExcel->getProperties()->setTitle('Haut Lac - Dossier #'.$folder);
$objPHPExcel->getProperties()->setSubject('Haut Lac - Dossier #'.$folder);
$objPHPExcel->getProperties()->setDescription('Haut Lac - Dossier #'.$folder);
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(11);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(20);



$sheet = $objPHPExcel->getActiveSheet();
$sheet->setTitle('Haut Lac - Dossier #'.$folder);
$sheet->setCellValueByColumnAndRow(0, 1, 'Consultation du dossier # '.$folder);
$sheet->setCellValueByColumnAndRow(0, 2, 'Question');
$sheet->setCellValueByColumnAndRow(1, 2, 'Réponse');
$sheet->getStyle('A2:B2')->applyFromArray(
        array(
            'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('rgb'=>'ffff00'))
        )
    );
$title = $sheet->getStyle('A1');
$styleFont = $title->getFont();
$styleFont->setSize(13.5);

$sql = $db->query("SELECT * FROM `hautlac_formulaire` WHERE `Valide`='1' ORDER BY ExportPosition ASC;");
if ($sql && $sql->num_rows > 0) {
	$line = 3;
	while($res = $sql->fetch_object()) {
		$sql2 = $db->query("SELECT * FROM `hautlac_form_user` WHERE `FolderID` = '".$folder."' AND `FormID` = '".$res->FormID."' ;");
		$res2 = $sql2->fetch_object();
		$response = utf8_encode($res2->Response);
		$question_FR = utf8_encode($res->Question_FR);

		switch ($res->Type) {
			case 1: case 4: case 6:
				$sheet->setCellValueByColumnAndRow(0, $line, $question_FR);
				$sheet->setCellValueExplicit('B'.$line, (string) $response, PHPExcel_Cell_DataType::TYPE_STRING);
				$line++;
 				break;
			case 2: // Case à cocher
				$response = $response == '1' ? 'Oui' : 'Non';
				$sheet->setCellValueByColumnAndRow(0, $line, $question_FR);
				$sheet->setCellValueByColumnAndRow(1, $line, $response);
				$line++;
				break;
		}
		

	}
}


$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->setOffice2003Compatibility(true);

ob_clean();
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=haut-lac-dossier-".$folder.".xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);

$objWriter->save('php://output');

?>