<script language="javascript">
<!-- //
var page_num = 1

$(document).ready(function(){
			
		$("#tab ul").sortable({ axis: 'y', opacity: 0.6, cursor: 'move', update: function() {
			var order = 'order=' + $(this).sortable('serialize').split('&Form[]=').join('|').substring(7);
			$.ajax({type: 'POST', url: 'inc/ajax/update_form.php', data: order});
			}
		});
});

function insert_form() {
	$('#facebox img[id="ajax_loader"]').show();

	var ret = parseInt($('#facebox select[id="Form_Type"] option:selected').val());
	var err;
	var data_send;
	
	// Filtres & Contr�les
	if ((!$('#facebox input[id="Form_'+ret+'_Question_FR"]').val() || !$('#facebox input[id="Form_'+ret+'_Question_EN"]').val()) && ret!=7) {err = "Veuillez indiquez la question.";}
	if (!err) {
		switch (ret) {
			case 0:
				err = "Veuillez s&eacute;lectionner un type de champ.";
				break;
			case 1:
			case 2:
			case 4:
			case 6:
			case 3:
			case 5:
			case 7:
				break;
			default:
				alert('Erreur.');
				return false;
		}
	}
				
	data_send = encodeURI(
				'Form_Type='+ret+
				'&Form_Question_FR='+$('#facebox input[id="Form_'+ret+'_Question_FR"]').val()+
				'&Form_Question_EN='+$('#facebox input[id="Form_'+ret+'_Question_EN"]').val()+
				'&Form_Choix_FR='+$('#facebox input[id="Form_'+ret+'_Choix_FR"]').val()+
				$('#facebox select[id="Form_'+ret+'_Choix_FR"]').val()+
				'&Form_Choix_EN='+$('#facebox input[id="Form_'+ret+'_Choix_EN"]').val()+
				$('#facebox select[id="Form_'+ret+'_Choix_EN"]').val()+
				'&Form_LenMax='+$('#facebox input[id="Form_'+ret+'_LenMax"]').val()+
				'&Form_Regex='+$('#facebox select[id="Form_'+ret+'_Regex"]').val()+
				'&Form_Obligatoire='+$('#facebox input[name="Form_'+ret+'_Obligatoire"]').val()+
				'&Form_Default_FR='+$('#facebox input[id="Form_'+ret+'_Default_FR"]').val()+
				'&Form_Default_EN='+$('#facebox input[id="Form_'+ret+'_Default_EN"]').val()
				);				
	
	if (err) {
		$('#facebox div[id="error_message"]').html(err);
		$('#facebox div[id="success"]').slideUp();$('#facebox div[id="error"]').slideDown();
		return false;
	}
	
	$('#facebox div[id="success"]').slideUp();
	$('#facebox div[id="error"]').slideUp();
	
	$.ajax({
		type: 'POST',
		url: 'inc/ajax/insert_form.php',
		data: data_send,
		success:function(data) {
			if (data) {
				$('#facebox div[id="error_message"]').html(data);
				$('#facebox div[id="error"]').slideDown();
			} else {
				$('#facebox input[id="submit_form"]').hide();
				$('#facebox input[id="submit_form_close"]').show();
				$('#facebox div[id="error"]').slideUp();
				$('#facebox div[id="success"]').slideDown();
				setTimeout("location.reload()",1000);
			}
		},
		error:function(data) {
			$('#facebox div[id="error_message"]').html('Op&eacute;ration &eacute;chou&eacute;. Contact&eacute; l\'administrateur.');
			$('#facebox div[id="success"]').slideUp();
			$('#facebox div[id="error"]').slideDown();
		}
	});
	
	$('#facebox img[id="ajax_loader"]').hide();
}

//Modifier certaines valeurs pour un champ sans avoir a supprimer puis recreer le champ
//added 15.06.2013
function edit_form(id) {
	$('#facebox img[id="ajax_loader"]').show();
	
	var ret = parseInt($('#facebox input[id="FormEdit_'+id+'_Type"]').val());
	var err;
	var data_send;
	
	// Filtres & Contr�les
	if ((!$('#facebox input[id="FormEdit_'+id+'_Question_FR"]').val() || !$('#facebox input[id="FormEdit_'+id+'_Question_EN"]').val()) && ret!=7) {err = "Veuillez indiquez la question.";}
	if (!err) {
		switch (ret) {
			case 0:
				err = "Veuillez s&eacute;lectionner un type de champ.";
				break;
			case 1:
			case 2:
			case 4:
			case 6:
			case 3:
			case 5:
			case 7:
				break;
			default:
				alert('Erreur.');
				return false;
		}
	}
		
	data_send = encodeURI(
				'Form_Type='+ret+
				'&FormID='+id+
				'&Form_Question_FR='+$('#facebox input[id="FormEdit_'+id+'_Question_FR"]').val()+
				'&Form_Question_EN='+$('#facebox input[id="FormEdit_'+id+'_Question_EN"]').val()+
				'&Form_LenMax='+$('#facebox input[id="FormEdit_'+id+'_LenMax"]').val()+
				'&Form_Obligatoire='+$('input:radio[name="FormEdit_'+id+'_Obligatoire"]:checked').val()+
				'&Form_Default_FR='+$('#facebox input[id="FormEdit_'+id+'_Default_FR"]').val()+
				'&Form_Default_EN='+$('#facebox input[id="FormEdit_'+id+'_Default_EN"]').val()
				);				
		
	if (err) {
		$('#facebox div[id="error_message'+id+'"]').html(err);
		$('#facebox div[id="success'+id+'"]').slideUp();$('#facebox div[id="error'+id+'"]').slideDown();
		return false;
	}
	
	$('#facebox div[id="success'+id+'"]').slideUp();
	$('#facebox div[id="error'+id+'"]').slideUp();
	
	$.ajax({
		type: 'POST',
		url: 'inc/ajax/edit_form.php',
		data: data_send,
		success:function(data) {
			if (data) {
				$('#facebox div[id="error_message'+id+'"]').html(data);
				$('#facebox div[id="error'+id+'"]').slideDown();
			} else {
				$('#facebox input[id="submit_form'+id+'"]').hide();
				$('#facebox input[id="submit_form_close'+id+'"]').show();
				$('#facebox div[id="error'+id+'"]').slideUp();
				$('#facebox div[id="success'+id+'"]').slideDown();
				setTimeout("location.reload()",1000);
			}
		},
		error:function(data) {
			$('#facebox div[id="error_message'+id+'"]').html('Op&eacute;ration &eacute;chou&eacute;. Contact&eacute; l\'administrateur.');
			$('#facebox div[id="success'+id+'"]').slideUp();
			$('#facebox div[id="error'+id+'"]').slideDown();
		}
	});
	
	$('#facebox img[id="ajax_loader"]').hide();
}

function update_user(user_id) {
	$('#get_info_'+user_id).hide();
	$('#set_info_'+user_id).show();
	$('#edit_info_'+user_id).hide();
	$('#valide_info_'+user_id).show();
}

function valide_user(user_id) {
	$('#get_info_'+user_id).html($('#set_info_'+user_id).val());
	$('#set_info_'+user_id).hide();
	$('#get_info_'+user_id).show();
	$('#valide_info_'+user_id).hide();
	$('#edit_info_'+user_id).show();
	data_send = encodeURI('type=gestionnaire&User_ID='+user_id+'&info=' + $('#set_info_'+user_id).val());	
	$.ajax({type: 'POST', url: 'inc/ajax/update_user.php', data: data_send});
}

function del_form(form_id) {
	if (confirm("Souhaitez-vous vraiement supprimer ce champ ?")) {
		data_send = encodeURI('Form_ID='+form_id);
		$.ajax({type: 'POST', url: 'inc/ajax/delete_form.php', data: data_send});
		location.reload();
	}
}

function Form_Type_Select() {
	$('#facebox fieldset[class="Form_Set"]').hide();
	$('#facebox fieldset[id="Form_Type_'+$('#facebox select[id="Form_Type"] option:selected').val()+'"]').fadeIn();
}
-->
</script>

		<div id="main-content">
				
			<noscript>
				<div class="notification error png_bg">
					<div>
						Le javascript de votre navigateur ne fonctionne pas. Merci de <a href="http://www.01net.com/telecharger/windows/Internet/navigateur/" title="Upgrade to a better browser">mettre &agrave; jour</a> votre navigateur upgrade ou <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Activer le javascript de votre navigateur">activer</a> le javacript.
					</div>
				</div>
			</noscript>

			<div id="new" style="display: none" class="modal">

				<h3>Ajout d'un champ</h3>
			 
				<div class="notification success png_bg" style="display: none;" id="success">
					<a href="javascript:void(0);" onclick="$('#facebox div[id=\'success\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
					<div>
						L'ajout du champ s'est d&eacute;roul&eacute; avec succ&egrave;s.
					</div>
				</div>
				
				<div class="notification error png_bg" style="display: none;" id="error">
					<a href="javascript:void(0);" onclick="$('#facebox div[id=\'error\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
					<div id="error_message"></div>
				</div>
				
				<form action="#" method="post">

					<fieldset>
						<label>Type</label>
							<span class="r">
							<select id="Form_Type" onchange="Form_Type_Select();">
								<option value="0">Choisissez un type</option>
								<option value="1">Texte</option>
								<option value="6">Zone de texte</option>
								<option value="2">Bo&icirc;te &agrave; cocher</option>
								<option value="3">Indication</option>
								<option value="4">Liste de choix</option>
								<option value="5">Fichier</option>
								<option value="7">S&eacute;parateur</option>
							</select>
							</span>
					</fieldset>
					
					<!-- Texte -->
					<fieldset id="Form_Type_1" class="Form_Set">
						<label>Question en Fran&ccedil;ais</label>
							<input id="Form_1_Question_FR" maxlength="512" class="max" />
						<label>Question en Anglais</label>
							<input id="Form_1_Question_EN" maxlength="512" class="max" />
						<label>Longueur maximale</label>
							<span class="r"><input id="Form_1_LenMax" value="120" class="comp" /></span>
						<label>V&eacute;rification</label>
							<span class="r">
							<select id="Form_1_Regex" class="comp">
								<option value="0">Aucune</option>
								<option value="1">Num&eacute;rique</option>
								<option value="2">Alphanum&eacute;rique</option>
								<option value="3">Date</option>
								<option value="4">E-Mail</option>
							</select>
							</span>
						<label>Obligatoire</label>
							<span class="r">					
							<input type="radio" value="1" name="Form_1_Obligatoire" >Oui</input>
							<input type="radio" value="0" name="Form_1_Obligatoire" checked="checked">Non</input>
							</span>
						<!--<label>Ent&ecirc;te CSV</label>
							<span class="r"><input id="Form_1_CSV" class="comp" /></span>-->
					</fieldset>
					<!-- !.Texte -->
					
					<!-- CheckBox -->
					<fieldset id="Form_Type_2" class="Form_Set">
						<label>Question en Fran&ccedil;ais</label>
							<input id="Form_2_Question_FR" maxlength="512" class="max" />
						<label>Question en Anglais</label>
							<input id="Form_2_Question_EN" maxlength="512" class="max" />
						<label>Obligatoire</label>
							<span class="r">					
							<input type="radio" value="1" name="Form_2_Obligatoire" >Oui</input>
							<input type="radio" value="0" name="Form_2_Obligatoire" checked="checked">Non</input>
							</span>	
						<!--<label>Valeur CSV si coch&eacute;</label>
							<input id="Form_2_CSV_Value" maxlength="1024" class="comp" />
						<label>Ent&ecirc;te CSV</label>
							<span class="r"><input id="Form_2_CSV" class="comp" /></span>-->
					</fieldset>
					<!-- !CheckBox -->
					
					<!-- Indication -->
					<fieldset id="Form_Type_3" class="Form_Set">
						<label>Indication en Fran&ccedil;ais</label>
							<input id="Form_3_Question_FR" maxlength="512" class="max" />
						<label>Indication en Anglais</label>
							<input id="Form_3_Question_EN" maxlength="512" class="max" />
						<label>Option visuelle</label>
							<select id="Form_3_Choix_FR" class="comp">
								<option value="0">Aucune</option>
								<option value="1">Gras</option>
								<option value="2">Soulign&eacute;</option>
								<option value="3">Italique</option>
								<option value="4">Agrandi</option>
							</select>

					</fieldset>
					<!-- !Indication -->
				
					<!-- Liste de choix -->
					<fieldset id="Form_Type_4" class="Form_Set">
						<label>Indication en Fran&ccedil;ais</label>
							<input id="Form_4_Question_FR" class="max" />
						<label>Indication en Anglais</label>
							<input id="Form_4_Question_EN" class="max" />
						<p>Rappel : Chaque choix doit &ecirc;tre s&eacute;par&eacute; par <b>un point-virgule</b> (ex : petit; moyen; grand)</p>
						<label>Choix en Fran&ccedil;ais</label>
							<input id="Form_4_Choix_FR" class="max" />
						<label>Choix en Anglais</label>
							<input id="Form_4_Choix_EN" class="max" />
						<label>La Valeur par défaut (en début de liste) en Fran&ccedil;ais</label>
							<input id="Form_4_Default_FR" class="max" />
						<label>La Valeur par défaut (en début de liste) en Anglais</label>
							<input id="Form_4_Default_EN" class="max" />	
						<label>Obligatoire</label>
							<span class="r">					
							<input type="radio" value="1" name="Form_4_Obligatoire" >Oui</input>
							<input type="radio" value="0" name="Form_4_Obligatoire" checked="checked">Non</input>
							</span>		
						<!--<label>Valeur CSV</label>
							<input id="Form_4_CSV_Value" class="comp" />							
						<label>Ent&ecirc;te CSV</label>
							<span class="r"><input id="Form_4_CSV" class="comp" /></span>-->
					</fieldset>
					<!-- !Liste de choix -->
					
					<!-- Fichier -->
					<fieldset id="Form_Type_5" class="Form_Set">
						<label>Indication en Fran&ccedil;ais</label>
							<input id="Form_5_Question_FR" maxlength="512" class="max" />
						<label>Indication en Anglais</label>
							<input id="Form_5_Question_EN" maxlength="512" class="max" />
						<label>Obligatoire</label>
							<span class="r">					
							<input type="radio" value="1" name="Form_5_Obligatoire" >Oui</input>
							<input type="radio" value="0" name="Form_5_Obligatoire" checked="checked">Non</input>
							</span>			
					</fieldset>
					<!-- !Fichier -->
					
					<!-- Zone de Texte -->
					<fieldset id="Form_Type_6" class="Form_Set">
						<label>Question en Fran&ccedil;ais</label>
							<input id="Form_6_Question_FR" maxlength="512" class="max" />
						<label>Question en Anglais</label>
							<input id="Form_6_Question_EN" maxlength="512" class="max" />
						<label>Longueur maximale</label>
							<span class="r"><input id="Form_6_LenMax" value="10000" class="comp" /></span>
						<label>Obligatoire</label>
							<span class="r">					
							<input type="radio" value="1" name="Form_6_Obligatoire" >Oui</input>
							<input type="radio" value="0" name="Form_6_Obligatoire" checked="checked">Non</input>
							</span>
						<!--<label>Ent&ecirc;te CSV</label>
							<span class="r"><input id="Form_6_CSV" class="comp" /></span>-->
					</fieldset>
					<!-- !.Texte -->

					<!-- S�parateur -->
					<fieldset id="Form_Type_7" class="Form_Set">N.B. : Sp&eacute;cifie une nouvelle &eacute;tape dans le processus du formulaire parent.</fieldset>
					<!-- !S�parateur -->
					
					<fieldset>
						<center>
							<input class="button" type="button" value="Annuler" onclick="$(document).trigger('close.facebox');" />&nbsp;
							<input class="button" type="button" value="Ajouter" onclick="insert_form();" id="submit_form" />
							<input class="button" type="button" value="Fermer" onclick="$(document).trigger('close.facebox');" id="submit_form_close" style="display:none;" />
						</center>
					</fieldset>
					
				</form>
				
			</div>
				
				<div class="clear"></div>

				<div class="content-box">
					
					<div class="content-box-header">
						
						<h3>Formulaire<span id="Ul_Order"></span></h3>
						<div class="clear"></div>
						
					</div>
					

					<div class="content-box-content">

						<div class="tab-content default-tab" id="tab">

<?php

$form = array(
			'Type' => array (1 => 'Bo&icirc;te de texte', 2 => 'Case &agrave; cocher', 3 => 'Indication', 4 => 'Liste de choix', 5=> 'Fichier'),
			'ReGex' => array (0 => 'Aucun', 1 => 'Num&eacute;rique', 2 => 'Alphanum&eacute;rique', 3 => 'Date', 4 => 'Email'),
			'Obligatoire' => array (0 => 'Non', 1 => 'Oui')
		);
		
								
			$request = "SELECT * FROM `hautlac_formulaire` WHERE `Valide` = '1' ORDER BY `Position` ASC";
			
			$sql = $db->query($request);
					
			if ( $sql->num_rows == 0) 
				{echo '<center>Il n\'y a pas encore de champs dans le formulaire. Merci de cliquer sur &lsaquo;&nbsp;Nouveau champ&nbsp;&rsaquo;</center>';}
			else
				{echo '
										<ul>';
					$i=0;
					while($res = $sql->fetch_object()) {
						$i++;
						$Type_of = $res->Type != 7 ? utf8_encode($res->Question_FR.' / '.$res->Question_EN) : '<i>S&eacute;parateur</i>';
						?>
											<li id="Form_<?php echo $res->FormID;?>">
												<div class="resume">
													<span class="title"><?php echo $i;?>) <?php echo $Type_of;?></span>
													<span class="what">											
														
														<?php if($res->Type != 7) {	?>
														<a href="#edit<?php echo $res->FormID; ?>" title="Editer le champ de <<?php echo utf8_encode($res->Question_FR);?>>" id="edit_form_<?php echo $res->FormID;?>" rel="modal">
															<img src="media/image/icons/pencil.png" alt="Editer" />
														</a>
														<?php }	?>
														<a href="javascript:void(0);" onclick="del_form(<?php echo $res->FormID;?>);" title="Supprimer le champ de <<?php echo utf8_encode($res->Question_FR);?>>" id="del_form_<?php echo $res->FormID;?>">
															<img src="media/image/icons/06.png" alt="Supprimer" />
														</a>													
													</span>
												</div>
												<div class="clear"></div>
											</li>
						<?php
						}
						
				echo '
										</ul>';		
						
				//Print each Field edit form that is hidden, will be shown when "edit" is clicked !
				//contains prefilled with DB data !		
					
					$sql2 = $db->query($request);
					
					$i=0;					
					while($res = $sql2->fetch_object()) {
						$i++;
													
							switch ($res->Type) {
								case "1":
								
									?>
											<div id="edit<?php echo $res->FormID; ?>" style="display: none" class="modal">

											<h3>Modifier le champ</h3>
										 
											<div class="notification success png_bg" style="display: none;" id="success<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'success\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div>
													Modification du champ s'est d&eacute;roul&eacute; avec succ&egrave;s.
												</div>
											</div>
											
											<div class="notification error png_bg" style="display: none;" id="error<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'error\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div id="error_message<?php echo $res->FormID; ?>"></div>
											</div>
											
											<form action="#" method="post">
											
													<input type="hidden" id="FormEdit_<?php echo $res->FormID; ?>_Type" value="<?php echo $res->Type; ?>" />
											
													<!-- Texte -->
													<fieldset id="Form_Type_<?php echo $res->FormID; ?>" class="Form_Set2">
														<label>Question en Fran&ccedil;ais</label>
															<input id="FormEdit_<?php echo $res->FormID; ?>_Question_FR" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_FR); ?>" />
														<label>Question en Anglais</label>
															<input id="FormEdit_<?php echo $res->FormID; ?>_Question_EN" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_EN); ?>" />
														<label>Longueur maximale</label>
															<span class="r"><input id="FormEdit_<?php echo $res->FormID; ?>_LenMax" class="comp" value="<?php echo $res->LenMax; ?>" /></span>
														<label>Obligatoire</label>
															<span class="r">	
															Valeur actuelle: <?php echo ''.($res->Obligatoire == 1 ? 'Oui' : 'Non').''; ?><br/>
															<input type="radio" value="1" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Oui</input>
															<input type="radio" value="0" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Non</input>
															</span>
													</fieldset>
													<!-- !.Texte -->
											
											<fieldset>
												<center>
													<input class="button" type="button" value="Annuler" onclick="$(document).trigger('close.facebox');" />&nbsp;
													<input class="button" type="button" value="Sauvegarder" onclick="edit_form('<?php echo $res->FormID; ?>');" id="submit_form<?php echo $res->FormID; ?>" />
													<input class="button" type="button" value="Fermer" onclick="$(document).trigger('close.facebox');" id="submit_form_close<?php echo $res->FormID; ?>" style="display:none;" />
												</center>
											</fieldset>
											
											</form>
											
											</div>
											
									<?php
								
									break;
								case "2":
								
									?>
											<div id="edit<?php echo $res->FormID; ?>" style="display: none" class="modal">

											<h3>Modifier le champ</h3>
										 
											<div class="notification success png_bg" style="display: none;" id="success<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'success\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div>
													Modification du champ s'est d&eacute;roul&eacute; avec succ&egrave;s.
												</div>
											</div>
											
											<div class="notification error png_bg" style="display: none;" id="error<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'error\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div id="error_message<?php echo $res->FormID; ?>"></div>
											</div>
											
											<form action="#" method="post">
											
												<input type="hidden" id="FormEdit_<?php echo $res->FormID; ?>_Type" value="<?php echo $res->Type; ?>" />
											
												<!-- CheckBox -->
												<fieldset id="FormEdit_<?php echo $res->FormID; ?>" class="Form_Set2">
													<label>Question en Fran&ccedil;ais</label>
														<input id="FormEdit_<?php echo $res->FormID; ?>_Question_FR" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_FR); ?>" />
													<label>Question en Anglais</label>
														<input id="FormEdit_<?php echo $res->FormID; ?>_Question_EN" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_EN); ?>" />
													<label>Obligatoire</label>
														<span class="r">					
														Valeur actuelle: <?php echo ''.($res->Obligatoire == 1 ? 'Oui' : 'Non').''; ?><br/>
														<input type="radio" value="1" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Oui</input>
														<input type="radio" value="0" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Non</input>
														</span>	
												</fieldset>
												<!-- !CheckBox -->

											<fieldset>
												<center>
													<input class="button" type="button" value="Annuler" onclick="$(document).trigger('close.facebox');" />&nbsp;
													<input class="button" type="button" value="Sauvegarder" onclick="edit_form('<?php echo $res->FormID; ?>');" id="submit_form<?php echo $res->FormID; ?>" />
													<input class="button" type="button" value="Fermer" onclick="$(document).trigger('close.facebox');" id="submit_form_close<?php echo $res->FormID; ?>" style="display:none;" />
												</center>
											</fieldset>
												
											</form>
											
											</div>
									<?php
								
									break;	
								case "3":
								
									?>
											<div id="edit<?php echo $res->FormID; ?>" style="display: none" class="modal">

											<h3>Modifier le champ</h3>
										 
											<div class="notification success png_bg" style="display: none;" id="success<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'success\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div>
													Modification du champ s'est d&eacute;roul&eacute; avec succ&egrave;s.
												</div>
											</div>
											
											<div class="notification error png_bg" style="display: none;" id="error<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'error\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div id="error_message<?php echo $res->FormID; ?>"></div>
											</div>
											
											<form action="#" method="post">
											
												<input type="hidden" id="FormEdit_<?php echo $res->FormID; ?>_Type" value="<?php echo $res->Type; ?>" />
											
													<!-- Indication -->
													<fieldset id="FormEdit_<?php echo $res->FormID; ?>" class="Form_Set2">
														<label>Indication en Fran&ccedil;ais</label>
															<input id="FormEdit_<?php echo $res->FormID; ?>_Question_FR" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_FR); ?>" />
														<label>Indication en Anglais</label>
															<input id="FormEdit_<?php echo $res->FormID; ?>_Question_EN" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_EN); ?>" />
													</fieldset>
													<!-- !Indication -->

											<fieldset>
												<center>
													<input class="button" type="button" value="Annuler" onclick="$(document).trigger('close.facebox');" />&nbsp;
													<input class="button" type="button" value="Sauvegarder" onclick="edit_form('<?php echo $res->FormID; ?>');" id="submit_form<?php echo $res->FormID; ?>" />
													<input class="button" type="button" value="Fermer" onclick="$(document).trigger('close.facebox');" id="submit_form_close<?php echo $res->FormID; ?>" style="display:none;" />
												</center>
											</fieldset>
												
											</form>
											
											</div>
									<?php
								
									break;									
								case "4":

									?>
											<div id="edit<?php echo $res->FormID; ?>" style="display: none" class="modal">

											<h3>Modifier le champ</h3>
										 
											<div class="notification success png_bg" style="display: none;" id="success<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'success\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div>
													Modification du champ s'est d&eacute;roul&eacute; avec succ&egrave;s.
												</div>
											</div>
											
											<div class="notification error png_bg" style="display: none;" id="error<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'error\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div id="error_message<?php echo $res->FormID; ?>"></div>
											</div>
											
											<form action="#" method="post">
											
													<input type="hidden" id="FormEdit_<?php echo $res->FormID; ?>_Type" value="<?php echo $res->Type; ?>" />
											
													<!-- Liste de choix -->
													<fieldset id="FormEdit_<?php echo $res->FormID; ?>" class="Form_Set2">
														<label>Indication en Fran&ccedil;ais</label>
															<input id="FormEdit_<?php echo $res->FormID; ?>_Question_FR" class="max" value="<?php echo utf8_encode($res->Question_FR); ?>" />
														<label>Indication en Anglais</label>
															<input id="FormEdit_<?php echo $res->FormID; ?>_Question_EN" class="max" value="<?php echo utf8_encode($res->Question_EN); ?>" />
														<label>Obligatoire</label>
															<span class="r">					
															Valeur actuelle: <?php echo ''.($res->Obligatoire == 1 ? 'Oui' : 'Non').''; ?><br/>
															<input type="radio" value="1" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Oui</input>
															<input type="radio" value="0" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Non</input>
															</span>
														<label>La Valeur par défaut (en début de liste) en Fran&ccedil;ais</label>
														<input id="FormEdit_<?php echo $res->FormID; ?>_Default_FR" class="max" value="<?php echo utf8_encode($res->Default_FR); ?>" />
													<label>La Valeur par défaut (en début de liste) en Anglais</label>
														<input id="FormEdit_<?php echo $res->FormID; ?>_Default_EN" class="max" value="<?php echo utf8_encode($res->Default_EN); ?>" />	
													</fieldset>
													<!-- !Liste de choix -->

											<fieldset>
												<center>
													<input class="button" type="button" value="Annuler" onclick="$(document).trigger('close.facebox');" />&nbsp;
													<input class="button" type="button" value="Sauvegarder" onclick="edit_form('<?php echo $res->FormID; ?>');" id="submit_form<?php echo $res->FormID; ?>" />
													<input class="button" type="button" value="Fermer" onclick="$(document).trigger('close.facebox');" id="submit_form_close<?php echo $res->FormID; ?>" style="display:none;" />
												</center>
											</fieldset>
													
											</form>
											
											</div>
									<?php								

								break;							
								case "6":

									?>
											<div id="edit<?php echo $res->FormID; ?>" style="display: none" class="modal">

											<h3>Modifier le champ</h3>
										 
											<div class="notification success png_bg" style="display: none;" id="success<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'success\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div>
													Modification du champ s'est d&eacute;roul&eacute; avec succ&egrave;s.
												</div>
											</div>
											
											<div class="notification error png_bg" style="display: none;" id="error<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'error\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div id="error_message<?php echo $res->FormID; ?>"></div>
											</div>
											
											<form action="#" method="post">
												
												<input type="hidden" id="FormEdit_<?php echo $res->FormID; ?>_Type" value="<?php echo $res->Type; ?>" />
												
												<!-- Zone de Texte -->
												<fieldset id="FormEdit_<?php echo $res->FormID; ?>" class="Form_Set2">
													<label>Question en Fran&ccedil;ais</label>
														<input id="FormEdit_<?php echo $res->FormID; ?>_Question_FR" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_FR); ?>" />
													<label>Question en Anglais</label>
														<input id="FormEdit_<?php echo $res->FormID; ?>_Question_EN" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_EN); ?>" />
													<label>Longueur maximale</label>
														<span class="r"><input id="FormEdit_<?php echo $res->FormID; ?>_LenMax" value="<?php echo $res->LenMax; ?>" class="comp" /></span>
													<label>Obligatoire</label>
														<span class="r">					
														Valeur actuelle: <?php echo ''.($res->Obligatoire == 1 ? 'Oui' : 'Non').''; ?><br/>
														<input type="radio" value="1" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Oui</input>
														<input type="radio" value="0" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Non</input>
														</span>
												</fieldset>
												<!-- !.Texte -->
											
											<fieldset>
												<center>
													<input class="button" type="button" value="Annuler" onclick="$(document).trigger('close.facebox');" />&nbsp;
													<input class="button" type="button" value="Sauvegarder" onclick="edit_form('<?php echo $res->FormID; ?>');" id="submit_form<?php echo $res->FormID; ?>" />
													<input class="button" type="button" value="Fermer" onclick="$(document).trigger('close.facebox');" id="submit_form_close<?php echo $res->FormID; ?>" style="display:none;" />
												</center>
											</fieldset>
											
											</form>
											
											</div>
									<?php
								
									break;														
								case "5":

									?>
											<div id="edit<?php echo $res->FormID; ?>" style="display: none" class="modal">

											<h3>Modifier le champ</h3>
										 
											<div class="notification success png_bg" style="display: none;" id="success<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'success\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div>
													Modification du champ s'est d&eacute;roul&eacute; avec succ&egrave;s.
												</div>
											</div>
											
											<div class="notification error png_bg" style="display: none;" id="error<?php echo $res->FormID; ?>">
												<a href="javascript:void(0);" onclick="$('#facebox div[id=\'error\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
												<div id="error_message<?php echo $res->FormID; ?>"></div>
											</div>
											
											<form action="#" method="post">
											
												<input type="hidden" id="FormEdit_<?php echo $res->FormID; ?>_Type" value="<?php echo $res->Type; ?>" />
											
												<!-- Fichier -->
												<fieldset id="FormEdit_<?php echo $res->FormID; ?>" class="Form_Set2">
													<label>Indication en Fran&ccedil;ais</label>
														<input id="FormEdit_<?php echo $res->FormID; ?>_Question_FR" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_FR); ?>" />
													<label>Indication en Anglais</label>
														<input id="FormEdit_<?php echo $res->FormID; ?>_Question_EN" maxlength="512" class="max" value="<?php echo utf8_encode($res->Question_EN); ?>" />  
													<label>Obligatoire</label>
														<span class="r">					
														Valeur actuelle: <?php echo ''.($res->Obligatoire == 1 ? 'Oui' : 'Non').''; ?><br/>
														<input type="radio" value="1" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Oui</input>
														<input type="radio" value="0" name="FormEdit_<?php echo $res->FormID; ?>_Obligatoire" >Non</input>
														</span>			
												</fieldset>
												<!-- !Fichier -->
											
											<fieldset>
												<center>
													<input class="button" type="button" value="Annuler" onclick="$(document).trigger('close.facebox');" />&nbsp;
													<input class="button" type="button" value="Sauvegarder" onclick="edit_form('<?php echo $res->FormID; ?>');" id="submit_form<?php echo $res->FormID; ?>" />
													<input class="button" type="button" value="Fermer" onclick="$(document).trigger('close.facebox');" id="submit_form_close<?php echo $res->FormID; ?>" style="display:none;" />
												</center>
											</fieldset>
											
											</form>
											
											</div>
									<?php
								
									break;							
								default:
									echo '';
							} //end switch
						
						
						}	//end while loop	
					
				}
						?>

						</div>
						
					</div>

					
				</div>
				
				<ul class="shortcut-buttons-set">
					<li>
						<a class="shortcut-button" href="#new" rel="modal">
							<span>
								<img src="media/image/icons/04.png" alt="Nouveau champ" /><br />
								Nouveau champ
							</span>
						</a>
					</li>
				</ul>
				
				<div class="clear"></div>
				

				<div id="footer">
					<small>
							<?php echo $copy;?>
					</small>
				</div>
				
		</div>