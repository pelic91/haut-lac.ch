<script language="javascript">
<!-- //
-->
</script>

		<div id="main-content">
				
			<noscript>
				<div class="notification error png_bg">
					<div>
						Le javascript de votre navigateur ne fonctionne pas. Merci de <a href="http://www.01net.com/telecharger/windows/Internet/navigateur/" title="Upgrade to a better browser">mettre &agrave; jour</a> votre navigateur upgrade ou <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Activer le javascript de votre navigateur">activer</a> le javacript.
					</div>
				</div>
			</noscript>
				
				<div class="clear"></div>

				<div class="content-box">
					
					<div class="content-box-header">
						
						<h3>Fonctionnement</h3>
						<div class="clear"></div>
						
					</div>
					
					<div class="content-box-content">
						<p><b>Modifier l'ordre d'apparition dans le formulaire</b> : Cliquez sur le champ dans la zone formulaire <u>sans relacher</u> le bouton de la souris puis montez ou descendez-le dans son nouvel emplacement entre 2 autres champs.</p>
						<p><b>Ajout d'un champ</b> : Pour ajouter des champs, vous trouverez un bouton &lsaquo; Nouveau champ &rsaquo; tout en bas de cette page.</p>
						<p><b>Modifier un champ</b> : Cette action est impossible par s&eacute;curit&eacute;. En effet, une modification pourrait entra&icirc;ner des erreurs de filtrage. Il est donc vivement conseill&eacute; de supprimer un champ et d'en cr&eacute;er un nouveau.</p>
						<p><b>Supprimer un champ</b> : Cliquez sur l'ic&ocirc;ne de croix pour supprimer <u>d&eacute;finitivement</u> le champ.
						<p><b>N.B.</b> : Attention, les modifications sont trait&eacute;es en temps r&eacute;el dans le formulaire c&ocirc;t&eacute; utilisateur. Vous pouvez fermer ces explications en cliquant sur le titre.
					</div>

				</div>
				
				<div class="clear"></div>

				<div class="content-box">
					
					<div class="content-box-header">
						
						<h3>Comment cr&eacute;er les champs adequat</h3>
						<div class="clear"></div>
						
					</div>
					
					<div class="content-box-content">
						<p>
							<h5>Ajouter un champ de type "Texte"</h5>
							<blockquote>
								<p><i>Utilisation</i> : Permet &agrave; l'utilisateur d'indiquer du texte court (en g&eacute;n&eacute;ral 1 &agrave; 3 mots) tel que son nom, pr&eacute;nom, adresse...</p>
								<p>
									<i>Cr&eacute;ation</i> : Cliquez sur "Nouveau champ" et s&eacute;lectionnez le type "Texte".<br />
									Remplissez la question &agrave; poser en FR/EN, d&eacute;finissez la longueur de la r&eacute;ponse maximale autoris&eacute;e, le type de v&eacute;rification a effectu&eacute;, si le champ exige d'&ecirc;tre compl&eacute;t&eacute; puis le nom de la colonne CSV (pour l'export).
								</p>
							</blockquote>
						</p>
						<p>
							<h5>Ajouter un champ de type "Bo&icirc;te &agrave; cocher"</h5>
							<blockquote>
								<p><i>Utilisation</i> : Permet &agrave; l'utilisateur de valider une question par une case &agrave; cocher tel que gar&ccedil;on, fille...</p>
								<p>
									<i>Cr&eacute;ation</i> : Cliquez sur "Nouveau champ" et s&eacute;lectionnez le type "Bo&icirc;te &agrave; cocher".<br />
									Remplissez la question &agrave; poser en FR/EN, indiquez la valeur que prendra le champs CSV correspondant si la case est coch&eacute; par l'utilisateur puis le nom de la colonne CSV (pour l'export).
								</p>
							</blockquote>
						</p>						
						<p>
							<h5>Ajouter un champ de type "Indication"</h5>
							<blockquote>
								<p><i>Utilisation</i> : Indique un texte &agrave; l'utilisateur tel que Informations des parents</p>
								<p>
									<i>Cr&eacute;ation</i> : Cliquez sur "Nouveau champ" et s&eacute;lectionnez le type "Indication".<br />
									Remplissez l'indication en FR/EN puis l'option visuel.
								</p>
							</blockquote>
						</p>						
						<p>
							<h5>Ajouter un champ de type "Liste de choix"</h5>
							<blockquote>
								<p><i>Utilisation</i> : Permet &agrave; l'utilisateur de s&eacute;lectionner une option parmi un liste de choix</p>
								<p>
									<i>Cr&eacute;ation</i> : Cliquez sur "Nouveau champ" et s&eacute;lectionnez le type "Liste de choix".<br />
									Remplissez l'indication en FR/EN, indiquez les diff&eacute;rents choix <b>s&eacute;par&eacute;s par une virgule</b> ainsi que leur valeur CSV correpndante puis indiquez l'ent&ecirc;te CSV.<br />Par exemple, si l'indication en Fran&ccedil;ais est "Quel est le moment de la journ&eacute;e ?", le choix en Fran&ccedil;ais peut &ecirc;tre "matin, midi, soir" correspondant aux valeurs CSV "mat, mid, soir".
								</p>
							</blockquote>
						</p>						
						<p>
							<h5>Ajouter un champ de type "Fichier"</h5>
							<blockquote>
								<p><i>Utilisation</i> : Permet &agrave; l'utilisateur de t&eacute;l&eacute;charger un fichier.</p>
								<p>
									<i>Cr&eacute;ation</i> : Cliquez sur "Nouveau champ" et s&eacute;lectionnez le type "Fichier".<br />
									Remplissez l'indication en FR/EN.
								</p>
							</blockquote>
						</p>						
						<p>
							<h5>Ajouter un champ de type "Zone de texte"</h5>
							<blockquote>
								<p><i>Utilisation</i> : Permet &agrave; l'utilisateur d'indiquer des phrases compl&egrave;tes tel que les &eacute;coles fr&eacute;quent&eacute;es, les informations concernant sa sant&eacute;.</p>
								<p>
									<i>Cr&eacute;ation</i> : Cliquez sur "Nouveau champ" et s&eacute;lectionnez le type "Texte".<br />
									Remplissez la question &agrave; poser en FR/EN, d&eacute;finissez la longueur de la r&eacute;ponse maximale autoris&eacute;e, si le champ exige d'&ecirc;tre compl&eacute;t&eacute; puis le nom de la colonne CSV (pour l'export).
								</p>
							</blockquote>
						</p>
						<p>
							<h5>Ajouter un "S&eacute;parateur"</h5>
							<blockquote>
								<p><i>Utilisation</i> : Indique au formulaire d'afficher une nouvelle &eacute;tape.</p>
								<p>
									<i>Cr&eacute;ation</i> : Cliquez sur "Nouveau champ" et s&eacute;lectionnez le type "S&eacute;parateur".<br />
								</p>
							</blockquote>
						</p>							
					</div>
				</div>
				
				<div class="clear"></div>
				
				<div id="footer">
					<small>
							<?php echo $copy;?>
					</small>
				</div>
				
		</div>