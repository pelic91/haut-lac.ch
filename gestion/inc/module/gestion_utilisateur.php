<script language="javascript">
<!-- //
var page_num = 1

$(document).ready(function(){
	pager();
});

function pager(page) {
	if (!page) {page = page_num;}
	$('#tab').html('<center><p>Chargement en cours...</p><img src="/media/image/icons/ajax-loader2.gif" style="margin:20px 0;" /></center>');
	data_send = encodeURI(
		'subject=utilisateur&page='+page
		+'&filter_id='+$('#filter_by_login').val()
		+'&filter_info='+$('#filter_by_info').val()
		);
	$.ajax({
		type: 'POST',
		url: 'inc/ajax/pager.php',
		data: data_send,
		success:function(data) {$('#tab').html(data);},
		error:function(data) {$('#tab').html('<center><p>Impossible de r&eacute;cup&eacute;rer les donn&eacute;es.</p><p>Merci de contacter le support.</p></center>');}
	});
	page_num = page;
}

function del_user(user_id) {
	if (confirm("Souhaitez-vous vraiement supprimer ce membre ?")) {
		data_send = encodeURI('type=utilisateur&User_ID='+user_id);	
		$.ajax({type: 'POST', url: 'inc/ajax/delete_user.php', data: data_send});
		pager();
	}
}

-->
</script>

		<div id="main-content">
				
			<noscript>
				<div class="notification error png_bg">
					<div>
						Le javascript de votre navigateur ne fonctionne pas. Merci de <a href="http://www.01net.com/telecharger/windows/Internet/navigateur/" title="Upgrade to a better browser">mettre &agrave; jour</a> votre navigateur upgrade ou <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Activer le javascript de votre navigateur">activer</a> le javacript.
					</div>
				</div>
			</noscript>
				
				<div class="clear"></div>
				
				<div class="content-box">
					
					<div class="content-box-header">
						
						<h3>Liste et gestion des utilisateurs</h3>
						<div class="clear"></div>
						
					</div>
					

					<div class="content-box-content">
						
						<div class="content-box-filter">
							<fieldset>
								<label class="content-box-filter-title">Filtre&nbsp;&raquo;</label>
								<label>Par adresse e-mail</label>
								<input type="textbox" id="filter_by_login" onkeyup="pager();" />
							</fieldset>
						</div>
						
						<div class="tab-content default-tab" id="tab"></div>
						
					</div>

					
				</div>
				
				<div class="clear"></div>
				

				<div id="footer">
					<small>
							<?php echo $copy;?>
					</small>
				</div>
				
		</div>