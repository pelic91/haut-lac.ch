		<div id="sidebar">
		
			<div id="sidebar-wrapper">
				
				<h1 id="sidebar-title"><a href="#">Tools corp</a></h1>
			  
				<a href="#"><img id="logo" src="media/image/logo.png" alt="Simpla Admin logo" /></a>
			  
				<div id="profile-links">
					<i><?php echo ucfirst($admin->userData['email']);?></i>&nbsp;&rsaquo;&nbsp;<a href="/gestion/index.php?cload=logout" title="D&eacute;connexion">D&eacute;connexion</a>
				</div>        
				
				<ul id="main-nav">

					<li>
						<a href="/gestion/index.php?cload=tableau-de-bord" class="nav-top-item no-submenu current" id="nav_1">
							Tableau de bord
						</a>       
					</li>
					
					<?php if ($admin->userData['Level']>=2) { ?>
					<li><a href="javascript:void(0);" class="nav-top-item" id="nav_2">Membres</a>
						<ul id="sub_2">
							<li><a href="/gestion/index.php?cload=gestion_utilisateur">Utilisateur</a></li>
							<li><a href="/gestion/index.php?cload=gestion_gestionnaire">Gestionnaire</a></li>
						</ul>
					</li>
					<?php } ?>
					
					<?php if ($admin->userData['Level']>=2) { ?>					
					<li>
						<a href="javascript:void(0);" class="nav-top-item" id="nav_3">Formulaire</a>
							<ul id="sub_3">
								<li><a href="/gestion/index.php?cload=formulaire">Gestion du formulaire</a></li>
								<li><a href="/gestion/index.php?cload=how-to-formulaire">Utilisation du formulaire</a></li>
							</ul>
					</li>
					<?php } ?>
					
					<?php if ($admin->userData['Level']>=1) { ?>					
					<li>
						<a href="javascript:void(0);" class="nav-top-item" id="nav_4">Dossier</a>
							<ul id="sub_4">
								<li><a href="/gestion/index.php?cload=view-folder">Consultation des dossiers</a></li>
							</ul>
					</li>
					<?php } ?>
										
				</ul>
				
			</div>
			
		</div>