<script language="javascript">
<!-- //
var page_num = 1

$(document).ready(function(){
	pager();
});

function pager(page) {
	if (!page) {page = page_num;}
	$('#tab').html('<center><p>Chargement en cours...</p><img src="/media/image/icons/ajax-loader2.gif" style="margin:20px 0;" /></center>');
	data_send = encodeURI(
		'subject=gestionnaire&page='+page
		+'&filter_id='+$('#filter_by_login').val()
		+'&filter_info='+$('#filter_by_info').val()
		);
	$.ajax({
		type: 'POST',
		url: 'inc/ajax/pager.php',
		data: data_send,
		success:function(data) {$('#tab').html(data);},
		error:function(data) {$('#tab').html('<center><p>Impossible de r&eacute;cup&eacute;rer les donn&eacute;es.</p><p>Merci de contacter le support.</p></center>');}
	});
	page_num = page;
}

function insert_user() {

	if (!$('#facebox input[id="new_id"]').val() || !$('#facebox input[id="new_pass"]').val()) {
		$('#facebox div[id="error_message"]').html('Merci d\'indiquer un identifiant et un mot de passe.');
		$('#facebox div[id="success"]').slideUp();
		$('#facebox div[id="error"]').slideDown();
		return false;
	}

	$('#facebox img[id="ajax_loader"]').show();
	
	data_send = encodeURI('type=gestionnaire'+
				'&user_id='+$('#facebox input[id="new_id"]').val()+
				'&user_pass='+$('#facebox input[id="new_pass"]').val()+
				'&user_info='+$('#facebox textarea[id="new_info"]').val());

	$.ajax({
		type: 'POST',
		url: 'inc/ajax/insert_user.php',
		data: data_send,
		success:function(data) {
			if (data) {
				$('#facebox div[id="error_message"]').html(data);
				$('#facebox div[id="error"]').slideDown();
			} else {
				$('#facebox input[id="submit_user"]').hide();
				$('#facebox input[id="submit_user_close"]').show();
				$('#facebox div[id="error"]').slideUp();
				$('#facebox div[id="success"]').slideDown();
				pager();
			}
		},
		error:function(data) {
			$('#facebox div[id="error_message"]').html('Op&eacute;ration &eacute;chou&eacute;. Contact&eacute; l\'administrateur.');
			$('#facebox div[id="success"]').slideUp();
			$('#facebox div[id="error"]').slideDown();
		}
	});

	$('#facebox img[id="ajax_loader"]').hide();
}

function update_user(user_id) {
	$('#get_info_'+user_id).hide();
	$('#set_info_'+user_id).show();
	$('#edit_info_'+user_id).hide();
	$('#valide_info_'+user_id).show();
}

function valide_user(user_id) {
	$('#get_info_'+user_id).html($('#set_info_'+user_id).val());
	$('#set_info_'+user_id).hide();
	$('#get_info_'+user_id).show();
	$('#valide_info_'+user_id).hide();
	$('#edit_info_'+user_id).show();
	data_send = encodeURI('type=gestionnaire&User_ID='+user_id+'&info=' + $('#set_info_'+user_id).val());	
	$.ajax({type: 'POST', url: 'inc/ajax/update_user.php', data: data_send});
}

function del_user(user_id) {
	if (confirm("Souhaitez-vous vraiement supprimer ce membre ?")) {
		data_send = encodeURI('type=gestionnaire&User_ID='+user_id);	
		$.ajax({type: 'POST', url: 'inc/ajax/delete_user.php', data: data_send});
		pager();
	}
}

function area_count() {
	ret = $('#facebox textarea[id="new_info"]').val()
	$('#facebox span[id="area_counter"]').html(Math.abs(ret.length-128) + ' caract&egrave;res restants');
	if (ret.length >= 128) {
		$('#facebox textarea[id="new_info"]').val(ret.substr(0,128));
	}
}
-->
</script>

		<div id="main-content">
				
			<noscript>
				<div class="notification error png_bg">
					<div>
						Le javascript de votre navigateur ne fonctionne pas. Merci de <a href="http://www.01net.com/telecharger/windows/Internet/navigateur/" title="Upgrade to a better browser">mettre &agrave; jour</a> votre navigateur upgrade ou <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Activer le javascript de votre navigateur">activer</a> le javacript.
					</div>
				</div>
			</noscript>

			<div id="new" style="display: none" class="modal">

				<h3>Ajout d'un gestionnaire<img src="/media/image/icons/ajax-loader.gif" id="ajax_loader" /></h3>
			 
				<p>
					Pensez &agrave; v&eacute;rifier les informations avant de valider.
				</p>
				
				<div class="notification success png_bg" style="display: none;" id="success">
					<a href="javascript:void(0);" onclick="$('#facebox div[id=\'success\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
					<div>
						L'ajout du gestionnaire s'est d&eacute;roul&eacute; avec succ&egrave;s.
					</div>
				</div>

				<div class="notification error png_bg" style="display: none;" id="error">
					<a href="javascript:void(0);" onclick="$('#facebox div[id=\'error\']').slideUp();" class="close"><img src="media/image/icons/cross_grey_small.png" title="Fermer cette notification" alt="Fermer" /></a>
					<div id="error_message"></div>
				</div>
				
				<form action="#" method="post">

					<fieldset>
						<label>Identifiant</label><input class="textbox" type="textbox" id="new_id" maxlength="32"  />
						<label>Mot de passe</label><input class="textbox" type="textbox" id="new_pass" maxlength="16" />
					</fieldset>
					
					<fieldset>
						<label class="more_info">Informations compl&eacute;mentaires</label><span id="area_counter">128 caract&egrave;res restants</span>
						<textarea id="new_info" onkeyup="area_count();"></textarea>
					</fieldset>
					
					<fieldset>
						<center>
							<input class="button" type="button" value="Annuler" onclick="$(document).trigger('close.facebox');" />&nbsp;
							<input class="button" type="button" value="Ajouter" onclick="insert_user();" id="submit_user" />
							<input class="button" type="button" value="Fermer" onclick="$(document).trigger('close.facebox');" id="submit_user_close" style="display:none;" />
						</center>
					</fieldset>
					
				</form>
				
			</div>
				
				<div class="clear"></div>
				
				<div class="content-box">
					
					<div class="content-box-header">
						
						<h3>Liste et gestion des gestionnaires</h3>
						<div class="clear"></div>
						
					</div>
					

					<div class="content-box-content">
						
						<div class="content-box-filter">
							<fieldset>
								<label class="content-box-filter-title">Filtre&nbsp;&raquo;</label>
								<label>Par identifant</label>
								<input type="textbox" id="filter_by_login" onkeyup="pager();" />
								<label>Par information</label>
								<input type="textbox" id="filter_by_info" onkeyup="pager();" />
							</fieldset>
						</div>
						
						<div class="tab-content default-tab" id="tab"></div>
						
					</div>

					
				</div>
				
				<ul class="shortcut-buttons-set">
					<li>
						<a class="shortcut-button" href="#new" rel="modal">
							<span>
								<img src="media/image/icons/01.png" alt="Nouveau gestionnaire" /><br />
								Nouveau gestionnaire
							</span>
						</a>
					</li>
				</ul>
				
				<div class="clear"></div>
				

				<div id="footer">
					<small>
							<?php echo $copy;?>
					</small>
				</div>
				
		</div>