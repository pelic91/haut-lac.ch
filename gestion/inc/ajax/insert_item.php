<?php
//if (!$_POST['SSID'] || !$_POST['folder'] || !$_POST['Form_ID'] || !$_POST['response'])   {exit('403');}
//some fields need to pass 0 which is empty and causesd issues with above code ! 14.04.2013
if (!$_POST['SSID'] || !$_POST['folder'] || !$_POST['Form_ID'])   {exit('403');}

session_id($_POST['SSID']);
session_start();

foreach (glob('../class/*.php') as $file) {include_once($file);}
foreach (glob('../function/*.php') as $file) {include_once($file);}

$user = new User($db);
if (!$user->is_loaded()) exit('403 Forbidden');

foreach ($_POST as $key => $value) {
	$_POST[$key] = utf8_decode(trim(urldecode($value)));
}

$user->set_response($_POST['folder'], $_POST['Form_ID'], $_POST['response']);
?>