<?php
class User{
	var $dbTable;
	var $sessionVariable = 'User_Session';
	var $tbFields = array(
		'userID' => 'userID', 
		'login'  => 'email',
		'pass'   => 'password',
		'active' => 'active'
	);
	var $remTime = 2592000;
	var $remCookieName = '';
	var $remCookieDomain = '';
	var $passMethod = 'nothing';
	var $displaySQL = false;
	var $displayErrors = false;
	var $userID;
	var $db;
	var $userData = array();
	var $current_Folder;

	function User($db = '', $settings = '', $Table = 'hautlac_user') {
		if ( is_array($settings) ) {
			foreach ( $settings as $k => $v ){
				if ( !isset( $this->{$k} ) ) die('La propri&eacute;t&eacute; '.$k.' n\'existe pas. V&eacute;rifier la configuration');
				$this->{$k} = $v;
			}
		}	
		$this->remCookieDomain = $this->remCookieDomain == '' ? $_SERVER['HTTP_HOST'] : $this->remCookieDomain;
		$this->dbTable = $Table;
		$this->db = $db;

		if ( !$this->db ) die(mysqli_error($db));

		if( !empty($_SESSION[$this->sessionVariable]) ) {
			$this->loadUser( $_SESSION[$this->sessionVariable] );
		}
		if ( isset($_COOKIE[$this->remCookieName]) && !$this->is_loaded()) {
			$u = unserialize(base64_decode($_COOKIE[$this->remCookieName]));
			$this->login($u['uname'], $u['password']);
		}

	}
  
	function login($uname, $password, $remember = true, $loadUser = true) {
		$uname    = $this->escape($uname);
		$password = $originalPassword = $this->escape($password);
		switch(strtolower($this->passMethod)){
			case 'sha1':
				$password = "SHA1('$password')"; break;
			case 'md5' :
				$password = "MD5('$password')";break;
			case 'nothing':
				$password = "'$password'";
		}
		$res = $this->query("SELECT * FROM `{$this->dbTable}` WHERE `{$this->tbFields['login']}` = '$uname' AND `{$this->tbFields['pass']}` = $password LIMIT 1");
		if ( $res->num_rows == 0) {return false;}
		
		if ( $loadUser ) {
			$this->userData = $res->fetch_array();
			$this->userID = $this->userData[$this->tbFields['userID']];
			$_SESSION[$this->sessionVariable] = $this->userID;
		}
		
		return true;
	}

	function logout($redirectTo = '') {
		setcookie($this->remCookieName, '', time()-3600);
		$_SESSION[$this->sessionVariable] = '';
		$this->userData = '';
		if ( $redirectTo != '' && !headers_sent()){
			header('Location: '.$redirectTo );
			exit;
		}
	}

	function is($prop){
		return $this->get_property($prop)==1?true:false;
	}
  
	function get_property($property) {
		if (empty($this->userID)) $this->error('Utilisateur non charg&eacute;', __LINE__);
		if (!isset($this->userData[$property])) $this->error('Propri&eacute;t&eacute; inconnue <b>'.$property.'</b>', __LINE__);
		return $this->userData[$property];
	}

	function is_active() {
		return $this->userData[$this->tbFields['active']];
	}
  
	function is_loaded() {
		return empty($this->userID) ? false : true;
	}

	function activate() {
		if (empty($this->userID)) $this->error('Utilisateur non charg&eacute;', __LINE__);
		if ( $this->is_active()) $this->error('Compte d&eacute;j&agrave; activ&eacute;', __LINE__);
		$res = $this->query("UPDATE `{$this->dbTable}` SET {$this->tbFields['active']} = 1 WHERE `{$this->tbFields['userID']}` = '".$this->escape($this->userID)."' LIMIT 1");
		if ($res->num_rows == 1) {
			$this->userData[$this->tbFields['active']] = true;
			return true;
		}
		return false;
	}

	function insertUser($data) {
		if (!is_array($data)) $this->error('La variable $data n\'est pas un tableau', __LINE__);
		switch(strtolower($this->passMethod)){
			case 'sha1':
				$password = "SHA1('".$data[$this->tbFields['pass']]."')"; break;
			case 'md5' :
				$password = "MD5('".$data[$this->tbFields['pass']]."')";break;
			case 'nothing':
				$password = "'".$data[$this->tbFields['pass']]."'";
		}
		foreach ($data as $k => $v ) $data[$k] = "'".$this->escape($v)."'";
		$data[$this->tbFields['pass']] = $password;
		$query = "INSERT INTO `{$this->dbTable}` (`".implode('`, `', array_keys($data))."`) VALUES (".implode(", ", $data).");";
		$this->db->query($query);
		return ($this->db->insert_id);
	}

	function randomPass($length=40, $chrs = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ'){
		for($i = 0; $i < $length; $i++) {
			$pwd .= $chrs{mt_rand(0, strlen($chrs)-1)};
		}
		return $pwd;
	}
	
	function get_folder() {
		$folder = array();
		if (empty($this->userID)) return false;
		$sql = $this->query("SELECT * FROM `hautlac_dossier` WHERE `UserID` = '".$this->userID."';");
		while($res = $sql->fetch_object()) {
			$folder[$res->FolderID] = array( 'State' => $res->State);
			$sql2 = $this->query("SELECT * FROM hautlac_form_user WHERE `FolderID`='".$res->FolderID."';");
			while($res2 = $sql2->fetch_object()) {
				$sql3 = $this->query("SELECT * FROM hautlac_formulaire WHERE `FormID`='".$res2->FormID."';");
				$res3 = $sql3->fetch_object();
				$folder[$res->FolderID][$res2->FormID] = array('response' => utf8_encode($res2->Response), 'Question_FR' => utf8_encode($res3->Question_FR), 'Question_EN' => utf8_encode($res3->Question_EN));
			}
		}
		return $folder;
	}

	function add_folder() {
		if (empty($this->userID)) return false;
		$this->query("INSERT INTO `hautlac_dossier` (`UserID` ,`State`) VALUES ('".$this->userID."', '0');");
		return ($this->db->insert_id);
	}
	
	function get_response($folder, $formID) {
		if (empty($this->userID)) return false;
		$sql = $this->query("SELECT * FROM `hautlac_dossier` WHERE `UserID` = '".$this->userID."' AND `FolderID` = '".$folder."' ;");
		if ($sql->num_rows == 0) return false;
		$sql = $this->query("SELECT * FROM `hautlac_form_user` WHERE `FolderID` = '".$folder."' AND `FormID` = '".$formID."' ;");
		if ($sql->num_rows == 0) return false;
		$res = $sql->fetch_object();
		return utf8_encode($res->Response);
	}
	
	function set_response($folder, $formID, $response) {
		if (empty($this->userID) || !is_numeric($folder) || !is_numeric($formID)) return false;
		$sql = $this->query("SELECT * FROM `hautlac_dossier` WHERE `UserID` = '".$this->userID."' AND `FolderID` = '".$folder."' ;");
		if ($sql->num_rows == 0) return false;
		$sql = $this->query("SELECT * FROM `hautlac_form_user` WHERE `FolderID` = '".$folder."' AND `FormID` = '".$formID."' ;");
		if ($sql->num_rows == 0) {
			$sql = $this->query("INSERT INTO `hautlac_form_user` (`FolderID` , `FormID` , `Response`) VALUES ('".$folder."', '".$formID."', '".$response."');");
		} else {
			$sql = $this->query("UPDATE `hautlac_form_user` SET `Response` = '".$response."' WHERE `FolderID` = '".$folder."' AND `FormID` = '".$formID."' ;");
		}
		return true;
	}
	
	function close_folder($folder) {
		if (empty($this->userID)) return false;
		$sql = $this->query("SELECT * FROM `hautlac_dossier` WHERE `UserID` = '".$this->userID."' AND `FolderID` = '".$folder."' ;");
		if ($sql->num_rows == 0) return false;
		$sql = $this->query("UPDATE `hautlac_dossier` SET `State` = '1' WHERE `FolderID` = '".$folder."' LIMIT 1;");
		return true;
	}
  ////////////////////////////////////////////
  // PRIVATE FUNCTIONS
  ////////////////////////////////////////////
  
function query($sql)
{
$res = $this->db->query($sql);
if ( $this->displaySQL ) { echo '<b>Requ&ecirc;te : </b>'.$sql.' [Modif : '.$res->num_rows.']<br />'; }
if ( !res )
$this->error(mysqli_error($this->db), $line);
return $res;
}

function loadUser($userID)
{
$res = $this->query("SELECT * FROM `{$this->dbTable}` WHERE `{$this->tbFields['userID']}` = '".$this->escape($userID)."' LIMIT 1");
if ( $res->num_rows == 0 )
return false;
$this->userData = $res->fetch_array();
$this->userID = $userID;
$_SESSION[$this->sessionVariable] = $this->userID;
return true;
}

function escape($str) {
$str = htmlentities($str);
return $str;
}
  
function error($error, $line = '', $die = false) {
if ( $this->displayErrors )
echo '<b>Error: </b>'.$error.' - <b>Line: </b>'.($line==''?'Unknown':$line);
if ($die) exit;
return false;
}
  
function getIp() {
$ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $ip = $_SERVER['REMOTE_ADDR'];
return $ip;
}
}
?>