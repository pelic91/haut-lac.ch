<?php
$API_Key = "64faf5d0b1dc311fd0f94af64f6c296a03045571";

/* Load Session & Cookie */
	if(!isset($_SESSION)) {
		session_set_cookie_params(86400, '', '', FALSE);
		session_cache_limiter('nocache');
		//session_save_path($_SERVER['DOCUMENT_ROOT'].'/session_user');
		session_start();
	}

	
/* Load Class & Function */
	foreach (glob('inc/class/*.php') as $file) {include_once($file);}
	foreach (glob('inc/function/*.php') as $file) {include_once($file);}
	
/* Init */
	// See inc/function/config.php to get other variable
	$user = new User($db);
	$admin = new User($db, null, 'hautlac_admin');
	$site = new Website($db);

/* Start */


/* Switch Page */
	switch ($site->Get_Page()) {
	
		case 'logout':
			$admin->logout('/gestion/');
			break;
		
		default:

			if ($admin->is_loaded()) {
				require_once('tools.php');
			} else {
				$site->Set_Title('Veuillez vous identifier.');
				require_once('login.php');
			}
	}	

?>