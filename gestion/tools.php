<?php

	switch ($site->Get_Page()) {

		case 'formulaire':
			$site->Set_Title('Administration Haut Lac > Gestion du formulaire');
			$module = 'formulaire.php'; 
			break;
	
		case 'gestion_gestionnaire':
			$site->Set_Title('Administration Haut Lac > Gestion des gestionnaires');
			$module = 'gestion_gestionnaire.php';
			break;

		case 'gestion_utilisateur':
			$site->Set_Title('Administration Haut Lac > Gestion des utilisateurs');
			$module = 'gestion_utilisateur.php';
			break;
			
		case 'how-to-formulaire':
			$site->Set_Title('Administration Haut Lac > Comment cr&eacute;er le formulaire');
			$module = 'how_to_formulaire.php';
			break;

		case 'view-folder':
			if (!$_GET['folder']) {
				$site->Set_Title('Administration Haut Lac > Consultation des dossiers');
				$module = 'view_folder.php';
			} else {
				$site->Set_Title('Administration Haut Lac > Consultation du dossier #'.$_GET['folder']);
				$module = 'view_folder_details.php';			
			}
			break;
		case 'export-excel':
			if (isset($_GET['folder']) && is_numeric($_GET['folder'])) {
				require_once('inc/module/export-excel.php');
				die();
			}
			break;
			
		case 'tableau-de-bord':
		default:
			$site->Set_Title('Administration Haut Lac > Tableau de bord');
			$module = 'main_board.php'; 
	}	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $site->Get_Title();?></title>
	<link type="text/css" rel="stylesheet" href="media/css/reset.css" />
	<link type="text/css" rel="stylesheet" href="media/css/style.css" />
	<link type="text/css" rel="stylesheet" href="media/css/invalid.css" />	

	<script type="text/javascript" src="media/js/jquery.js"></script>
	<script type='text/javascript' src="media/js/facebox.js"></script>
	<script type="text/javascript" src="media/js/configuration.js"></script>
</head>

<body>
	
	<div id="body-wrapper">
			
		<?php 
		require_once('inc/module/menu.php');
		require_once('inc/module/'.$module);
		?>

	</div>
	
</body>
	
</html>
