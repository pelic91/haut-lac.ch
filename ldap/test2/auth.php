<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>LDAP Test: Authentication Results</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        body { padding-top: 50px; }
	</style>
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<h2>LDAP Test: Authentication Results</h2>
	<p>Return to <a href="index.php">top page</a>.</p><hr />
	<?php
	include 'conf.php';

	$user = htmlspecialchars($_POST['user']);
	$filter ='';
	//$filter = "((uid=" . $user . ")" . "(mail=" . $user ."@*))";
	$pass = $_POST['password'];
	echo "<p>Equivalent command line:<br /><tt>ldapsearch -h " . $server . " -p " . $port . " -b " . $basedn . " \"" .$filter . "\"</tt></p>";
	echo "<hr />";

	// Connect to the LDAP server.
	$ldapconn = ldap_connect($server, $port) or
	die("Could not connect to " . $server . ":" . $port . ".");
	
	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');
	ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0); // We need this for doing an LDAP search.
	echo $binddn = 'uid='.$user.','.$basedn;
	// Bind anonymously to the LDAP server to search and retrieve DN.
	$ldapbind = ldap_bind($ldapconn,$user,$pass) or die("Could not bind anonymously.");
	$result   = ldap_search($ldapconn,$basedn,$filter) or die ("Search error.");
	$entries  = ldap_get_entries($ldapconn, $result);
	$binddn   = $entries[0]["dn"];
	echo "<p>Bind DN found: ". $binddn . "</p>";
	echo "<hr />";

	// Bind again using the DN retrieved. If this bind is successful,
	// then the user has managed to authenticate.
	$ldapbind = ldap_bind($ldapconn, $binddn, $_POST['password']);
	if ($ldapbind) {
		echo "Successful authentication for " . $user . ".";
	} else {
		echo "Failed authentication for " . $user . ".";
	}

	ldap_close($ldapconn);
	?>
	<hr /><p>Return to <a href="index.php">top page</a>.</p>
</div>
</body>
</html>