<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>LDAP Test: Search Result</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        body { padding-top: 50px; }
	</style>
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<h2>LDAP Test: Search Result</h2>
	<p>Return to <a href="index.php">top page</a>.</p><hr />
	<?php
	include 'conf.php';

	// Thanks to http://www.devshed.com/c/a/PHP/Using-PHP-With-LDAP-part-1
	// for inspiration.
	$name = htmlspecialchars($_POST['name']);
	$filter = "(|(cn=*" . $name . "*)" . "(sn=*" . $name ."*))";

	echo "<p>Equivalent command line:<br /><tt>ldapsearch -h " .
	$server . " -p " . $port . " -b " . $basedn . " \"" .
	$filter . "\"</tt></p>";
	echo "<hr />";

	// Connect to the LDAP server.
	$ldapconn = ldap_connect($server, $port) or
	die("Could not connect " . $server . ":" . $port . ".");
	
	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');
	ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0); // We need this for doing an LDAP search.

	// Bind anonymously to the LDAP server to search.
	$ldapbind = ldap_bind($ldapconn) or die("Could not bind anonymously.");
	$result   = ldap_search($ldapconn,$basedn,$filter) or die ("Search error.");
	$entries  = ldap_get_entries($ldapconn, $result);

	// Display key data for each entry.
	for ($i=0; $i<$entries["count"]; $i++) {
		echo "<p>DN: " . $entries[$i]["dn"] . "<br />";
		echo "Uid: " . $entries[$i]["uid"][0] . "<br />";
		echo "Email: " . $entries[$i]["mail"][0] . "</p>";
	}

	ldap_close($ldapconn);
	?>
	<hr />
	<p>Return to <a href="index.php">top page</a>.</p>
</div>
</body>
</html>