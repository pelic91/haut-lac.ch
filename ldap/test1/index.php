<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>LDAP Test</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        body { padding-top: 50px; }
	</style>
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<h2>LDAP Test</h2>
	<hr />
	<h3>Authentication</h3>
	<form action="auth.php" method="POST">
		<div class="col-md-5">
			<tt>Uid/email:</tt>
			<input class="form-control" type="text" name="user" size="30" /><br />
			<tt>Password :</tt>
			<input class="form-control" type="password" name="password" size="30" />
			<div class="clearfix"></div>
			<br>
			<input class="btn btn-default" type="submit" value="Authenticate" name="submit" />
		</div>
		<div class="clearfix"></div>
	</form>
	<hr />
	<h3>User Lookup</h3>
	<form action="lookup.php" method="POST">
		<div class="col-md-5">
			<tt>User name:</tt>
			<input class="form-control" type="text" name="name" size="30" />
			<br>
			<input class="btn btn-default" type="submit" value="Search" name="submit" />
		</div>
		<div class="clearfix"></div>
	</form>
	<hr />
</div>
</body>
</html>